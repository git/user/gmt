# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"
GCONF_DEBUG="no"
GNOME2_LA_PUNT="yes"

inherit eutils gnome2-multilib virtualx

DESCRIPTION="Gtk module for bridging AT-SPI to Atk"
HOMEPAGE="http://live.gnome.org/Accessibility"

LICENSE="LGPL-2+"
SLOT="2"
KEYWORDS="~amd64"
IUSE=""

COMMON_DEPEND="
	>=app-accessibility/at-spi2-core-2.11.2[${MULTILIB_USEDEP}]
	>=dev-libs/atk-2.11.90[${MULTILIB_USEDEP}]
	>=dev-libs/glib-2.32:2[${MULTILIB_USEDEP}]
	>=sys-apps/dbus-1.5[${MULTILIB_USEDEP}]
"
RDEPEND="${COMMON_DEPEND}
	!<gnome-extra/at-spi-1.32.0-r1
"
DEPEND="${COMMON_DEPEND}
	virtual/pkgconfig
"

src_configure() {
	gnome2-multilib_src_configure --enable-p2p
}

ehook gnome2-multilib-per-abi-pre_src_test my_abi_pre_test
my_abi_pre_test() {
	Xemake check
	return 1
}
