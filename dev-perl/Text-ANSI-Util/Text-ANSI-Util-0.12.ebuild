# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

MODULE_AUTHOR="SHARYANTO"
MODULE_VERSION=${PV}
inherit perl-module

DESCRIPTION="Terminal control using ANSI escape sequences"

LICENSE="GPL-1"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

SRC_TEST="do"

RDEPEND="dev-perl/Data-Dump
		dev-perl/Text-WideChar-Util"
