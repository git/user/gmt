# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

MODULE_AUTHOR="SHARYANTO"
MODULE_VERSION=${PV}
inherit perl-module

DESCRIPTION="Routines for text containing wide characters"

LICENSE="GPL-1"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

SRC_TEST="do"

RDEPEND="dev-perl/Unicode-LineBreak"
