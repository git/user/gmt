# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# @ECLASS: eutils-multilib.eclass
# @MAINTAINER:
# Gregory M. Turner <gmt@be-evil.net>
# @AUTHOR:
# Gregory M. Turner <gmt@be-evil.net>
# @BLURB: multilib-build-based extensions for standard eutils functions
# @DESCRIPTION:
# This eclass contains multilib-build-based extensions for standard eutils functions.

inherit multilib multilib-build eutils

# @FUNCTION: multilib_preserve_old_lib
# @USAGE: [list-of-library-templates]
# @DESCRIPTION:
# Operates just like preserve_old_lib, but invokes preserve_old_lib once for each
# ABI that would be returned by multilib_get_enabled_abis; before invocation,
# performs @GET_LIBDIR@ substitution on each argument (see: multilib.eclass:
# get_libdir_subst; eutils.eclass: preserve_old_lib; multilib-build.eclass:
# multilib_get_enabled_abis)
multilib_preserve_old_lib() {
	debug-print-function "${FUNCNAME}" "$@"
	multilib_foreach_abi do_with_get_libdir_subst \
		preserve_old_lib "$@"
}

# @FUNCTION: multilib_preserve_old_lib_notify
# @USAGE: [list-of-library-templates]
# @DESCRIPTION:
# Operates just like preserve_old_lib_notify, but invokes preserve_old_lib once
# for each ABI that would be returned by multilib_get_enabled_abis; before
# invocation, performs @GET_LIBDIR@ substitution on each argument (see:
# multilib.eclass: get_libdir_subst; eutils.eclass: preserve_old_lib_notify;
# multilib-build.eclass: multilib_get_enabled_abis)
multilib_preserve_old_lib_notify() {
	debug-print-function "${FUNCNAME}" "$@"
	multilib_foreach_abi do_with_get_libdir_subst \
		preserve_old_lib_notify "$@"
}

# @FUNCTION: abi_arch_usex
# @USAGE: <pseudo-USE flag> [true output] [false output] [true suffix] [false suffix]
# @DESCRIPTION:
# If abi_arch_use pseudo-flag is true, echo [true output][true suffix] (defaults to "yes"),
# otherwise echo [false output][false suffix] (defaults to "no").  See abi_arch_use in
# multilib-build from the gmt overlay for more information on abi_arch_use
abi_arch_usex() {
	abi_arch_use "$1" && echo "${2-yes}$4" || echo "${3-no}$5"
}

# @FUNCTION: use_with_best_abi
# @USAGE: <USEFLAG> [<flagname> [value]]
# @DESCRIPTION:
# Deprecated clone of multilib_native_use_with
use_with_best_abi() {
	eqawarn "QA warning: use_with_best_abi is deprecated" >&2
	eqawarn "use multilib_native_use_with instead" >&2

	multilib_native_use_with "$@"
}

# @FUNCTION: use_with_non_best_abi
# @USAGE: <USEFLAG> [<flagname> [value]]
# @DESCRIPTION:
# Deprecated clone of multilib_non_native_use_with
use_with_non_best_abi() {
	eqawarn "QA warning: use_with_non_best_abi is deprecated" >&2
	eqawarn "use multilib_non_native_use_with instead" >&2

	multilib_non_native_use_with "$@"
}

# @FUNCTION: use_enable_best_abi
# @USAGE: <USEFLAG> [<flagname> [value]]
# @DESCRIPTION:
# Deprecated clone of multilib_native_use_enable
use_enable_best_abi() {
	eqawarn "QA warning: use_enable_best_abi is deprecated" >&2
	eqawarn "use multilib_native_use_enable instead" >&2

	multilib_native_use_enable "$@"
}

# @FUNCTION: use_enable_non_best_abi
# @USAGE: <USEFLAG> [<flagname> [value]]
# @DESCRIPTION:
# Deprecated clone of multilib_non_native_use_Enable
use_enable_non_best_abi() {
	eqawarn "QA warning: use_enable_non_best_abi is deprecated" >&2
	eqawarn "use multilib_non_native_use_enable instead" >&2

	multilib_non_native_use_enable "$@"
}

# @FUNCTION: best_abi_enable
# @USAGE: <flagname> [<value>]
# @DESCRIPTION:
# Deprecated clone of multilib_native_enable
best_abi_enable() {
	eqawarn "QA warning: best_abi_enable is deprecated" >&2
	eqawarn "use multilib_native_enable instead" >&2

	multilib_native_enable "${@}"
}

# @FUNCTION: best_abi_with
# @USAGE: <flagname> [<value>]
# @DESCRIPTION:
# Deprecated clone of multilib_native_with
best_abi_with() {
	eqawarn "QA warning: best_abi_with is deprecated" >&2
	eqawarn "use multilib_native_with instead" >&2

	multilib_native_with "${@}"
}

# @FUNCTION: non_best_abi_enable
# @USAGE: <flagname> [<value>]
# @DESCRIPTION:
non_best_abi_enable() {
	eqawarn "QA warning: best_abi_enable is deprecated" >&2
	eqawarn "use multilib_native_enable instead" >&2

	multilib_non_native_enable "${@}"
}

# @FUNCTION: non_best_abi_with
# @USAGE: <flagname> [<value>]
# @DESCRIPTION:
non_best_abi_with() {
	eqawarn "QA warning: best_abi_with is deprecated" >&2
	eqawarn "use multilib_native_with instead" >&2

	multilib_non_native_with "${@}"
}
