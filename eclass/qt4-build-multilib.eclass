# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# @ECLASS: qt4-build-multilib.eclass
# @MAINTAINER:
# Greg Turner <gmt@be-evil.net>
# @BLURB: provides a multi-abi-compatible framework similar to qt4-build.eclass
# @DESCRIPTION:
# This eclass wraps the qt4-build.eclass phase functions, providing
# a quick(-ish) migration path for ebuilds based on qt4-build.eclass to
# support multi-ABI profiles.  Compared to other foo-multilib
# framework adapters, it is more or less a gigantic steaming pile of
# horse manure, as it does not do out-of-tree builds and contains
# agressive, ugly hacks.

case ${EAPI} in
	4|5|4-*|5-*)	: ;;
	*)	die "qt4-build.eclass: unsupported EAPI=${EAPI:-0}" ;;
esac

inherit qt4-build multilib-build ehooker

# @FUNCTION: qt4-build-multilib_pkg_setup
# @DESCRIPTION:
# Sets up PATH and LD_LIBRARY_PATH.
qt4-build-multilib_pkg_setup() {
	debug-print-function ${FUNCNAME} "$@"
	ehook_fire qt4-build-multilib-global-pre_pkg_setup && \
		qt4-build_pkg_setup "$@"
	ehook_fire qt4-build-multilib-global-post_pkg_setup -u
}

# @ECLASS-VARIABLE: QT4_EXTRACT_DIRECTORIES
# @DEFAULT_UNSET
# @DESCRIPTION:
# Space-separated list including the directories that will be extracted from
# Qt tarball.

# @ECLASS-VARIABLE: QT4_TARGET_DIRECTORIES
# @DEFAULT_UNSET
# @DESCRIPTION:
# Arguments for build_target_directories. Takes the directories in which the
# code should be compiled. This is a space-separated list.

# @FUNCTION: qt4-build-multilib_src_unpack
# @DESCRIPTION:
# Unpacks the sources.
qt4-build-multilib_src_unpack() {
	debug-print-function "${FUNCNAME}" "$@"
	ehook_fire qt4-build-multilib-global-pre_src_unpack && \
		qt4-build_src_unpack "$@"
	ehook_fire qt4-build-multilib-global-post_src_unpack -u
}

# implement qt4-build-multilib-best-abi-{pre,post}_src_* ehooks
# by implementing a hook-adapter for the per-abi ehooks
_qt4-build-multilib_best_abi_hook_demux() {
	local hookname="${1/-per-abi-/-best-abi-}"
	multilib_is_native_abi && { ehook-fire "${hookname}" ; return $? ; }
	return 0
}

if [[ ${_QT4_BUILD_MULTILIB_HOOK_DEMUX_REGISTERED} ]] ; then
	for _qwerpoiuqwerpoiu in qt4-build-multilib-per-abi-{pre,post}_src_{prepare,configure,compile,test,install} ; do
		ehook ${_qwerpoiuqwerpoiu} _qt4-build-multilib_best_abi_hook_demux
	done
	_QT4_BUILD_MULTILIB_HOOK_DEMUX_REGISTERED=yes
	unset _qwerpoiuqwerpoiu
fi

# @ECLASS-VARIABLE: PATCHES
# @DEFAULT_UNSET
# @DESCRIPTION:
# PATCHES array variable containing all various patches to be applied.
# This variable is expected to be defined in global scope of ebuild.
# Make sure to specify the full path. This variable is utilised in
# src_prepare() phase.
#
# @CODE
#   PATCHES=( "${FILESDIR}/mypatch.patch"
#             "${FILESDIR}/patches_folder/" )
# @CODE

# @FUNCTION: qt4-build-multilib_src_prepare
# @DESCRIPTION:
# Prepare the build directories for configuration
qt4-build-multilib_src_prepare() {
	debug-print-function ${FUNCNAME} "$@"

	# unfortunately, qt4-build runs its paches much, much
	# later after several extremely aggressive changes have
	# been made.  We'll just have to shore our patches up to
	# work before instead of after :(
	[[ -n ${PATCHES[@]} ]] && epatch "${PATCHES[@]}"
	declare -a PATCHES=( )

	_qt4-build-multilib_src_prepare_hookwrap() {
		local S="${BUILD_DIR}"
		if ehook_fire qt4-build-multilib-per-abi-pre_src_prepare ; then

			qt4-build_src_prepare "$@"

			# make sure PKG_CONFIG_{LIBDIR,PATH} go into the forced variables
			# lest we end up pulling in all kinds of DEFAULT_ABI crap

			debug-print "${FUNCNAME}: applying PKG_CONFIG_LIBDIR and PKG_CONFIG_PATH overrides in configure"
			sed -e "/^SYSTEM_VARIABLES=/i \
PKG_CONFIG_LIBDIR='${EPREFIX}/usr/$(get_libdir)/pkgconfig'\n\
PKG_CONFIG_PATH='${EPREFIX}/usr/share/pkgconfig'\n" \
				-i configure \
				|| die "sed SYSTEM_VARIABLES (round II) failed"
		fi
		ehook_fire qt4-build-multilib-per-abi-post_src_prepare -u
	}

	if ehook_fire qt4-build-multilib-global-pre_src_prepare ; then
		# sadly, qt4-build simply isn't designed for out-of-tree
		# builds, even though qt does support them according to
		# the website.  Even if it did, there are so many
		# ABI-specific hacks injected during qt4-build_src_prepare
		# that we would have to do both qt4-build_src_prepare and
		# qt4-build_src_configure in the qt4-build-multilib_src_configure
		# step to make it work.  What really needs to happen, I suppose,
		# is a full reimplementation here in qt4-build-multilib that
		# does not rely on qt4-build.  However, to get the ball rolling,
		# we follow the path of least resistance, for now...
		multilib_copy_sources

		multilib_parallel_foreach_abi run_in_build_dir \
			_qt4-build-multilib_src_prepare_hookwrap "$@"
	fi
	ehook_fire qt4-build-multilib-global-post_src_prepare -u
}

# @FUNCTION: qt4-build-multilib_src_configure
# @DESCRIPTION:
qt4-build-multilib_src_configure() {
	debug-print-function "${FUNCNAME}" "$@"

	_qt4-build-multilib_src_configure_hookwrap() {
		debug-print-function "${FUNCNAME}" "$@"

		_qt4_get_libdir_subst_myconf() {
			local oldconfarg newconfarg newmyconf
			for oldconfarg in ${myconf} ; do
				newconfarg="$(get_libdir_subst "${oldconfarg}")"
				[[ ${newconfarg} == ${oldconfarg} ]] || \
					einfo "patched configure argument for ABI ${ABI}: \"${newconfarg}\""
				newmyconf="${newmyconf}${newmyconf:+ }${newconfarg}"
			done
			myconf="${newmyconf}"
		}

		# prevent per-abi changes to "myconf" from persisting
		local myconf="${myconf} $*"
		_qt4_get_libdir_subst_myconf

		local S="${BUILD_DIR}"
		ehook_fire qt4-build-multilib-per-abi-pre_src_configure && \
			qt4-build_src_configure
		ehook_fire qt4-build-multilib-per-abi-post_src_configure -u
	}

	ehook_fire qt4-build-multilib-global-pre_src_configure && \
		multilib_parallel_foreach_abi run_in_build_dir \
			_qt4-build-multilib_src_configure_hookwrap "$@"
	ehook_fire qt4-build-multilib-global-post_src_configure -u
}

# @FUNCTION: qt4-build-multilib_src_compile
# @DESCRIPTION:
# Compile the sources
qt4-build-multilib_src_compile() {
	debug-print-function "${FUNCNAME}" "$@"

	_qt4-build-multilib_src_compile_hookwrap() {
		debug-print-function "${FUNCNAME}" "$@"
		local S="${BUILD_DIR}"
		ehook_fire qt4-build-multilib-per-abi-pre_src_compile && \
			qt4-build_src_compile "$@"
		ehook_fire qt4-build-multilib-per-abi-post_src_compile -u
	}

	ehook_fire qt4-build-multilib-global-pre_src_compile && \
		multilib_parallel_foreach_abi run_in_build_dir \
			_qt4-build-multilib_src_compile_hookwrap "$@"
	ehook_fire qt4-build-multilib-global-post_src_compile -u
}

# @FUNCTION: qt4-build-multilib_src_test
# @DESCRIPTION:
# Run the test suite for each supported ABI
qt4-build-multilib_src_test() {
	debug-print-function "${FUNCNAME}" "$@"

	_qt4-build-multilib_src_test_hookwrap() {
		debug-print-function "${FUNCNAME}" "$@"
		local S="${BUILD_DIR}"
		ehook_fire qt4-build-multilib-per-abi-pre_src_test && \
			qt4-build_src_test "$@"
		ehook_fire qt4-build-multilib-per-abi-post_src_test -u
	}
	
	ehook_fire qt4-build-multilib-global-pre_src_test && \
		multilib_parallel_foreach_abi run_in_build_dir \
			_qt4-build-multilib_src_test_hookwrap "$@"
	ehook_fire qt4-build-multilib-global-post_src_test -u
}

# @FUNCTION: qt4-build-multilib_src_install
# @DESCRIPTION:
# Install the compiled software
qt4-build-multilib_src_install() {
	debug-print-function "${FUNCNAME}" "$@"

	_qt4-build-multilib_secure_install() {
		debug-print-function "${FUNCNAME}" "$@"
		local S="${BUILD_DIR}"
		if ehook_fire qt4-build-multilib-per-abi-pre_src_install ; then

			# kludge: other ABI's leave this file around and it gets
			# appended to and then installed, leading to a header-
			# conflict.  Rather than clone all of qt4-build into my
			# overlay, I've elected to employ this hack as a work-
			# around; however, this snafu should be reported and
			# patched in qt4-build. -gmt
			[[ -f "${T}"/gentoo-${PN}-qconfig.h ]] && \
				rm -f "${T}"/gentoo-${PN}-qconfig.h

			qt4-build_src_install "${@}"
		fi
		ehook_fire qt4-build-multilib-per-abi-post_src_install -u
		# Do multilib magic only when >1 ABI is used.
		if [[ ${#MULTIBUILD_VARIANTS[@]} -gt 1 ]]; then
			multilib_prepare_wrappers
			# Make sure all headers are the same for each ABI.
			multilib_check_headers
		fi
	}

	if ehook_fire qt4-build-multilib-global-pre_src_install ; then
		multilib_foreach_abi run_in_build_dir \
			_qt4-build-multilib_secure_install "$@"
		# merge the wrappers
		multilib_install_wrappers
	fi
	ehook_fire qt4-build-multilib-global-post_src_install -u
}

# @FUNCTION: qt4-build-multilib_pkg_postinst
# @DESCRIPTION:
# Regenerate configuration when the package is installed.
qt4-build-multilib_pkg_postinst() {
	debug-print-function "${FUNCNAME}" "$@"
	ehook_fire qt4-build-multilib-global-pre_pkg_postinst && \
		qt4-build_pkg_postinst "$@"
	ehook_fire qt4-build-multilib-global-post_pkg_postinst -u
}

# @FUNCTION: qt4-build-multilib_pkg_postrm
# @DESCRIPTION:
# Regenerate configuration when the package is completely removed.
qt4-build-multilib_pkg_postrm() {
	debug-print-function "${FUNCNAME}" "$@"
	ehook_fire qt4-build-multilib-global-pre_pkg_postinst && \
		qt4-build_pkg_postrm "$@"
	ehook_fire qt4-build-multilib-global-post_pkg_postrm -u
}

EXPORT_FUNCTIONS pkg_setup src_unpack src_prepare src_configure src_compile src_test src_install pkg_postrm pkg_postinst
