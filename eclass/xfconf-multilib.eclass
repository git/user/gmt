# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# @ECLASS: xfconf-multilib.eclass
# @MAINTAINER:
# XFCE maintainers <xfce@gentoo.org>
# @BLURB: Default XFCE ebuild layout for multi-abi building
# @DESCRIPTION:
# Default XFCE ebuild layout for multi-abi building

# @ECLASS-VARIABLE: EAUTORECONF
# @DESCRIPTION:
# Run eautoreconf instead of elibtoolize if the variable is set

# @ECLASS-VARIABLE: XFCONF
# @DESCRIPTION:
# This should be an array defining arguments for econf

inherit autotools-multilib

# synchronize AUTOTOOLS_AUTORECONF and EAUTORECONF for purposes of
# semantic consistency (these do the same thing, more or less,
# respectively in the autotools-multilib and xfconf eclasses).
if [[ -n "${AUTOTOOLS_AUTORECONF}" || -n "${EAUTORECONF}" ]] ; then
	AUTOTOOLS_AUTORECONF=yes
	EAUTORECONF=yes
fi

inherit xfconf gnome2-multilib

# duplicate the logic from xfconf.ebuild for _xfconf_live (sort of), so as
# not to have to rely on internal variables flowing into our namespace
unset _xfconf_multilib_live
[[ $PV == *9999* ]] && _xfconf_multilib_live=yes

EXPORT_FUNCTIONS src_prepare src_configure src_compile src_install src_test

# ehook proxy: intercept ehooks matching "gnome2-multilib-*-{pre,post}_*"
# and rebroadcast into ehooks matching "xconf-multilib-*-{pre,post}_*"
_xfconf-gnome2-multilib_ehook_proxy() {
	debug-print-function "${FUNCNAME}" "$@"
	case $1 in
		gnome2-multilib-*-pre_src_configure) 
			declare -a XFCONF=("${G2CONF[@]}")
			declare -a XFCONF_OTHER=("${G2CONF_CMDLINE_ARGUMENTS[@]}")
			ehook_fire xfconf-multilib-pre_src_configure
			local fire_result=$?
			G2CONF=("${XFCONF[@]}")
			G2CONF_CMDLINE_ARGUMENTS=("${XFCONF_OTHER[@]}")
			return $fire_result
			;;
		gnome2-multilib-*-pre_*)
			ehook_fire "xconf-multilib${1#gnome2-multilib}"
			;;
		gnome2-multilib-*-post_*)
			ehook_fire "xconf-multilib${1#gnome2-multilib}" -u
			;;
		*)
			ewarn "_xfconf-gnome2-multilib_ehook_proxy received unexpected notification \"$1\"."
			;;
	esac
}

for hook in gnome2-multilib-{per-abi,best-abi,global}-{pre,post}_src_{configure,prepare,compile,install,test} ; do
	ehook "${hook}" _xfconf-gnome2-multilib_ehook_proxy
done

# @FUNCTION: xfconf-multilib_src_prepare
# @DESCRIPTION:
# A trivial proxy for gnome2-multilib_src_prepare
# Applys patches from the PATCHES array and performs any
# eautoreconf or elibtoolize steps as appropriate (see variables).
xfconf-multilib_src_prepare() {
	debug-print-function ${FUNCNAME} "$@"
	# synchronize AUTOTOOLS_AUTORECONF and EAUTORECONF for purposes of
	# semantic consistency (these do the same thing, more or less,
	# respectively in the autotools-multilib and xfconf eclasses).
	if [[ -n "${AUTOTOOLS_AUTORECONF}" || -n "${EAUTORECONF}" ]] ; then
		AUTOTOOLS_AUTORECONF=yes
		EAUTORECONF=yes
	fi
	AT_M4DIR=${EPREFIX}/usr/share/xfce4/dev-tools/m4macros \
		gnome2-multilib_src_prepare "$@"
}

# @FUNCTION: xfconf-multilib_src_configure
# @DESCRIPTION:
# Runs econf for each ABI with options from the XFCONF
# array and the command-line.
xfconf-multilib_src_configure() {
	debug-print-function ${FUNCNAME} "$@"
	if declare -p XFCONF >/dev/null 2>&1 ; then
		[[ $(declare -p XFCONF) == declare\ -a* ]] || \
			die "XFCONF must be an indexed array!"
	else
		XFCONF=( )
	fi
	[[ -n $_xfconf_multilib_live ]] && XFCONF+=( --enable-maintainer-mode )

	declare -a G2CONF=("${XFCONF[@]}")
	gnome2-multilib_src_configure "$@"
}

# @FUNCTION: xfconf-multilib_src_compile
# @DESCRIPTION:
# trivial wrapper of gnome2-multilib_src_compile
xfconf-multilib_src_compile() {
	debug-print-function ${FUNCNAME} "$@"
	gnome2-multilib_src_compile "$@"
}

# @FUNCTION: xfconf-multilib_src_test
# @DESCRIPTION:
# trivial wrapper of gnome2-multilib_src_test
xfconf-multilib_src_test() {
	debug-print-function ${FUNCNAME} "$@"
	gnome2-multilib_src_test "$@"
}

# @FUNCTION: xfconf-multilib_src_install
# @DESCRIPTION:
# Run emake install and install documentation in the DOCS array
# for each targeted ABI.
xfconf-multilib_src_install() {
	debug-print-function ${FUNCNAME} "$@"

	# FIXME: (but how?  this fixme cloned from xfconf eclass)
	if [[ -n $_xfconf_multilib_live ]] && ! [[ -e ChangeLog ]]; then
		touch ChangeLog
	fi

	GNOME2_LA_PUNT=fuck_yeah \
		gnome2-multilib_src_install "$@" || die
}

