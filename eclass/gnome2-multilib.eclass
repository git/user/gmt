# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# @ECLASS: gnome2-multilib.eclass
# @MAINTAINER:
# gnome@gentoo.org
# @BLURB: Extends gnome2.eclass to provide multi-abi support to Gnome/Gtk+-based packages.
# @DESCRIPTION:
# Exports portage base functions used by ebuilds written for packages using the
# GNOME framework. For additional functions, see gnome2{,-utils}.eclass

# src_prepare comes from autotools multilib
inherit ehooker gtk-doc gnome2 autotools-multilib

case "${EAPI:-0}" in
	4|5) :; ;;
	*) die "EAPI=${EAPI} is not supported" ;;
esac

EXPORT_FUNCTIONS src_unpack src_prepare src_configure src_compile src_test src_install pkg_preinst pkg_postinst pkg_postrm

# @ECLASS-VARIABLE: G2CONF
# @DESCRIPTION:
# An array of arguments to provide to configure.
# In gnome2.eclass, this was a string; if a string
# is used, it will be converted to an array automatically,
# but an ugly warning will issue forth unto the end-user
if declare -p G2CONF >/dev/null 2>&1 ; then
	if [[ $(declare -p G2CONF) != declare\ -[Aa]* ]] ; then
		if [[ "${G2CONF}" == "" ]] ; then
			# this is the default from inheriting gnome2.eclass, let's
			# go ahead and change it to an array...
			unset G2CONF
			declare -a G2CONF=()
		fi
	fi
fi

# the following six functions are not superflous; DUCY?

# @FUNCTION: gnome2-multilib_src_unpack
# @DESCRIPTION:
# Calls gnome2_src_unpack
gnome2-multilib_src_unpack() {
	debug-print-function ${FUNCNAME} "$@"
	gnome2_src_unpack "$@"
}

# @FUNCTION: gnome2-multilib_src_compile
# @DESCRIPTION:
# Calls autotools-multilib_src_compile
gnome2-multilib_src_compile() {
	debug-print-function ${FUNCNAME} "$@"
	autotools-multilib_src_compile "$@"
}

# @FUNCTION: gnome2-multilib_pkg_preinst
# @DESCRIPTION:
# Finds Icons, GConf and GSettings schemas for later handling in pkg_postinst
gnome2-multilib_pkg_preinst() {
	debug-print-function ${FUNCNAME} "$@"
	gnome2_gconf_savelist
	gnome2_icon_savelist
	gnome2_schemas_savelist
	gnome2_scrollkeeper_savelist
	gnome2-multilib_gdk_pixbuf_savelist
}

# @FUNCTION: gnome2-multilib_pkg_postinst
# @DESCRIPTION:
# Handle scrollkeeper, GConf, GSettings, Icons, desktop and mime
# database updates.
gnome2-multilib_pkg_postinst() {
	debug-print-function ${FUNCNAME} "$@"
	gnome2_gconf_install
	fdo-mime_desktop_database_update
	fdo-mime_mime_database_update
	gnome2_icon_cache_update
	gnome2_schemas_update
	gnome2_scrollkeeper_update
	gnome2-multilib_gdk_pixbuf_update
}

# @FUNCTION: gnome2-multilib_pkg_postrm
# @DESCRIPTION:
# Handle scrollkeeper, GSettings, Icons, desktop and mime database updates.
gnome2-multilib_pkg_postrm() {
	debug-print-function ${FUNCNAME} "$@"
	fdo-mime_desktop_database_update
	fdo-mime_mime_database_update
	gnome2_icon_cache_update
	gnome2_schemas_update
	gnome2_scrollkeeper_update
}

# @FUNCTION: gnome2-multilib_src_test
# @DESCRIPTION:
# Calls autotools-multilib_src_test
gnome2-multilib_src_test() {
	autotools-multilib_src_test "$@"
}

# @FUNCTION: gnome2-multilib_src_prepare
# @DESCRIPTION:
# Fires the gnome2-multilib-global-{pre,post}_src_prepare ehooks, and,
# not cancelled by hook listener, calls
# autotools-utils_src_prepare and gnome2_src_prepare.
gnome2-multilib_src_prepare() {
	if ehook_fire gnome2-multilib-global-pre_src_prepare ; then
		AT_NOELIBTOOLIZE=yes autotools-utils_src_prepare
		gnome2_src_prepare
		[[ ${AUTOTOOLS_IN_SOURCE_BUILD} ]] && multilib_copy_sources
	fi
	ehook_fire gnome2-multilib-global-post_src_prepare -u
}

# this ehook proxy intercepts ehooks matching:
#
#   autotools-multilib-{global,per-abi,best-abi}-{pre,post}_src_{configure,test,install}
#
# and fires corresponding gnome2-multilib ehooks as a convenience.  For example, it
# will listen for the
#
#   autotools-multilib-per-abi-pre_src_configure
#
# ehook and, upon receipt, broadcast:
#
#   gnome2-multilib-per-abi-pre_src_configure
#
# result codes are propagated to the original ehook if the hook is interruptible.
_gnome2-multilib_autotools-multilib_ehook-proxy() {
	debug-print-function ${FUNCNAME} "$@"
	case $1 in
		autotools-multilib-global-pre_src_install|autotools-multilib-global-post_src_install)
			# these two hooks are fired manually in gnome2-multilib_src_install so there is no
			# need to proxy them (to do so would fire broken duplicate hooks)
			return 0
			;;
		autotools-multilib-global-pre_src_configure|autotools-multilib-global-post_src_configure)
			# these two hooks are fired manually in gnome2-multilib_src_configure so there is no
			# need to proxy them (to do so would fire broken duplicate hooks)
			return 0
			;;
		autotools-multilib-*-pre_src_configure)
			# translate myeconfargs to G2CONF & back
			declare -a G2CONF=("${myeconfargs[@]}")
			declare -a G2CONF_CMDLINE_ARGUMENTS=("${othereconfargs[@]}")
			debug-print "${FUNCNAME}: G2CONF (before): $( declare -p G2CONF 2>&1 )"
			debug-print "${FUNCNAME}: G2CONF_CMDLINE_ARGUMENTS (before): $( declare -p G2CONF_CMDLINE_ARGUMENTS 2>&1 )"
			ehook_fire "gnome2-${1#autotools-}"
			local rslt=$?
			debug-print "${FUNCNAME}: G2CONF (after): $( declare -p G2CONF 2>&1 )"
			debug-print "${FUNCNAME}: G2CONF_CMDLINE_ARGUMENTS (after): $( declare -p G2CONF_CMDLINE_ARGUMENTS 2>&1 )"
			myeconfargs=("${G2CONF[@]}")
			othereconfargs=("${G2CONF_CMDLINE_ARGUMENTS[@]}")
			return $rslt
			;;
		autotools-multilib-*-pre_src_*)
			ehook_fire "gnome2-${1#autotools-}"
			;;
		autotools-multilib-*-post_src_*)
			ehook_fire "gnome2-${1#autotools-}" -u
			;;
		*)
			ewarn "_gnome2-multilib_autotools-utils_ehook-proxy received unexpected notification: \"$1\"."
			return 0
			;;
	esac
}

for hook in autotools-multilib-{global,per-abi,best-abi}-{pre,post}_src_{configure,compile,test,install} ; do
		ehook "${hook}" _gnome2-multilib_autotools-multilib_ehook-proxy
done

# Convenience function to check if an option or its inverse is present in _g2conf.
# Used only by gnome2-multilib_src_configure.  As an example of how this works,
# it will return 0 (true) if the argument is "--disable-monkey-poo" and
# ${G2CONF_CMDLINE_ARGUMENTS[4]} is --enable-monkey-poo="delicious!!!".
_g2conf_has_like() {
	local confitem directive="$1"
	# bool-flip negative directives
	case ${directive} in
		--disable-*) directive="--enable-${directive#--disable-}" ;;
		--without-*) directive="--with-${directive#--without-}" ;;
		*) :; ;;
	esac
	for confitem in "${G2CONF[@]}" "${G2CONF_CMDLINE_ARGUMENTS}" ; do
		# strip any equals sign and following junk
		confitem="${confitem%%=*}"
		# bool-flip negative confitems
		case ${confitem} in
			--disable-*) confitem="--enable-${confitem#--disable-}" ;;
			--without-*) confitem="--with-${confitem#--without-}" ;;
			*) :; ;;
		esac
		[[ ${directive} == ${confitem} ]] && return 0
	done
	return 1
}

# @FUNCTION: gnome2-multilib_src_configure
# @DESCRIPTION:
# Gnome specific configure handling.  Per-ABI code may be hooked
# by listening to the gnome2-multilib_{pre,post}-src_configure hooks.
# The configure arguments, including any passed in as arguments
# to gnome2-multilib_src_configure, are available in the G2CONF array
# during the gnome2-multilib-pre_src_configure hook event and any
# changes made by listeners will be passed on to econf.
#
# The default configure process is supressed entirely and
# gnome2-multilib-per-abi-pre_src_configure ehook processing cut short,
# if a nonzero value is returned by any
# gnome2-multilib-per-abi-pre_src_configure ehook listener.
# This can be used to "override" the default src_configure
# process on a per-abi basis.
#
# Any occurance of "@GET_LIBDIR@" in G2CONF or in the command-line
# arguments will be substituted with the results of $(get_libdir),
# on a per-multilib-abi-basis.  This feature is provided by the
# autotools-multilib framework (gnome2-multilib inherits the
# feature for "free")
gnome2-multilib_src_configure() {
	debug-print-function ${FUNCTION} "$@"
	# Assume any myeconfargs from the environment is a bug
	if declare -p myeconfargs >/dev/null 2>&1 ; then
		die "Detected myeconfargs in gnome2-multilib.  Use G2CONF instead."
	fi

	declare -a G2CONF_CMDLINE_ARGUMENTS=("$@")

	debug-print "${FUNCNAME}: G2CONF (before): $( declare -p G2CONF 2>&1 )"
	debug-print "${FUNCNAME}: G2CONF_CMDLINE_ARGUMENTS (before): $( declare -p G2CONF_CMDLINE_ARGUMENTS 2>&1 )"

	# if G2CONF is a string upgrade it to an array
	if declare -p G2CONF >/dev/null 2>&1 ; then
		if [[ $(declare -p G2CONF) != declare\ -[Aa]* ]] ; then
			# lets optimistically assume it's a string.  right thing probably happens if we create a local array shadow-var
			ewarn
			ewarn "In gnome2-multilib it's preferred to make G2CONF an array, although a string will probably work"
			ewarn "It was: $(declare -p G2CONF)"
			ewarn
			declare -a G2CONF=( ${G2CONF} )
		fi
	else
		# fine, then lets make a local, empty one
		declare -a G2CONF
	fi

	if ehook_fire gnome2-multilib-global-pre_src_configure ; then
		if [[ ${GCONF_DEBUG} != 'no' ]] && use debug ; then
			_gconf_has_like "--enable-debug" || G2CONF+=(--enable-debug=yes)
		fi
		if grep -q "enable-gtk-doc" "${ECONF_SOURCE:-.}"/configure ; then
			if ! _g2conf_has_like "--disable-gtk-doc" ; then
				if [[ ${EAPI:-4} == 4 ]] && in_iuse doc ; then
					G2CONF+=("$(use_enable doc gtk-doc)")
				else
					G2CONF+=("--disable-gtk-doc")
				fi
			fi
		fi
		if grep -q "^[[:space:]]*AM_MAINTAINER_MODE(\[enable\])" \
			"${ECONF_SOURCE:-.}"/configure.*; then
			_g2conf_has_like "--enable-maintainer-mode" || G2CONF+=(--disable-maintainer-mode)
		fi
		if grep -q "disable-scrollkeeper" "${ECONF_SOURCE:-.}"/configure; then
			_g2conf_has_like "--enable-scrollkeeper" || G2CONF+=(--disable-scrollkeeper)
		fi
		if [[ ${EAPI:-4} == 4 ]] && grep -q "disable-silent-rules" "${ECONF_SOURCE:-.}"/configure; then
			_g2conf_has_like "--enable-silent-rules" || G2CONF+=(--disable-silent-rules)
		fi
		if grep -q "disable-schemas-install" "${ECONF_SOURCE:-.}"/configure; then
			_g2conf_has_like "--enable-schemas-install" || G2CONF+=(--disable-schemas-install)
		fi
		if grep -q "disable-schemas-compile" "${ECONF_SOURCE:-.}"/configure; then
			_g2conf_has_like "--enable-schemas-compile" || G2CONF+=(--disable-schemas-compile)
		fi
		if grep -q "enable-compile-warnings" "${ECONF_SOURCE:-.}"/configure; then
			_g2conf_has_like "--enable-compile-warnings" || G2CONF+=(--enable-compile-warnings=minimum)
		fi
		addwrite "$(unset HOME; echo ~)/.gnome2"

		debug-print "${FUNCNAME}: G2CONF (after): $( declare -p G2CONF 2>&1 )"
		debug-print "${FUNCNAME}: G2CONF_CMDLINE_ARGUMENTS (after): $( declare -p G2CONF_CMDLINE_ARGUMENTS 2>&1 )"

		declare -a myeconfargs=("${G2CONF[@]}")
		autotools-multilib_src_configure "${G2CONF_CMDLINE_ARGUMENTS[@]}"
	fi
	ehook_fire gnome2-multilib-global-post_src_configure -u
}

# @FUNCTION: gnome2-multilib_src_install
# @DESCRIPTION:
# Gnome specific install. Handles typical GConf and scrollkeeper setup
# in packages and removal of .la files if requested
gnome2-multilib_src_install() {
	debug-print-function ${FUNCTION} "$@"

	# if this is not present, scrollkeeper-update may segfault and
	# create bogus directories in /var/lib/
	local sk_tmp_dir="/var/lib/scrollkeeper"
	dodir "${sk_tmp_dir}" || die "dodir failed"

	# we must delay gconf schema installation due to sandbox
	local GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL="1"

	# GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL will be auto-un-exported
	# when it falls out of scope at the end of this function.
	export GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL

	if ehook_fire gnome2-multilib-global-pre_src_install ; then
		# upgrade DOCS to an array if required
		if declare -p DOCS >/dev/null 2>&1 ; then
			if ! declare -p DOCS | grep -q '^declare -[aA]' ; then
				declare -a DOCS=( ${DOCS} )
			fi
		fi

		# defer the pruning of libtool files until later, as this is already a standard behavior in
		# gnome2.eclass, and there's no reason to do it twice.
		case x${AUTOTOOLS_PRUNE_LIBTOOL_FILES} in
			xnone|x) :; ;;
			xall) GNOME2_LA_PUNT=fuck_yeah ;;
			xmodules) GNOME2_LA_PUNT=modules ;;
			*) GNOME2_LA_PUNT=meh ;;
		esac

		local AUTOTOOLS_PRUNE_LIBTOOL_FILES=none

		# this last-minute registration ensures that no autotools-multilib-per-abi-pre_src_install
		# ehook listener is deprived of notification due to _gnome2-multilib-per-abi-pre_src_install
		# aborting the ehook (which it must do when USE_EINSTALL is set)
		ehook autotools-multilib-per-abi-pre_src_install _gnome2-multilib-per-abi-pre_src_install
		autotools-multilib_src_install "scrollkeeper_localstate_dir=${ED}${sk_tmp_dir}" "$@"
		eunhook autotools-multilib-per-abi-pre_src_install _gnome2-multilib-per-abi-pre_src_install

		# Handle documentation as 'default' for eapi5 and newer, bug #373131
		if [[ ${EAPI:-4} == 4 ]] ; then
			# Manual document installation
			if declare -p DOCS >/dev/null 2>&1 ; then
				if [[ ${#DOCS[@]} -gt 0 ]]; then
					dodoc "${DOCS[@]}" || die "dodoc failed"
				fi
			fi
		else
			einstalldocs
		fi

		# Delete all .la files
		if [[ ${EAPI:-4} == 4 ]] ; then
			if [[ "${GNOME2_LA_PUNT}" != "no" ]]; then
				if ! use_if_iuse static-libs ; then
					ebegin "Removing .la files"
					find "${D}" -name '*.la' -exec rm -f {} + || die "la file removal failed"
					eend
				fi
			fi
		else
			case "${GNOME2_LA_PUNT}" in
				yes) prune_libtool_files --modules ;;
				no) :; ;;
				fuck_yeah) prune_libtool_files --all ;;
				*) prune_libtool_files ;;
			esac
		fi
	fi

	ehook_fire gnome2-multilib-global-post_src_install -u

	# Do not keep /var/lib/scrollkeeper because:
	# 1. The scrollkeeper database is regenerated at pkg_postinst()
	# 2. ${ED}/var/lib/scrollkeeper contains only indexes for the current pkg
	#    thus it makes no sense if pkg_postinst ISN'T run for some reason.
	rm -rf "${ED}${sk_tmp_dir}"
	rmdir "${ED}var/lib" 2>/dev/null
	rmdir "${ED}var" 2>/dev/null

	# Make sure this one doesn't get in the portage db
	rm -fr "${ED}usr/share/applications/mimeinfo.cache"

}

# Since it was registered after the hook-wrapping listener to the same
# ehook, if any gnome2-multilib-per-abi-pre_src_install
# ehook listener aborts the ehook, this listener will never be called.
_gnome2-multilib-per-abi-pre_src_install() {
	if [[ -z "${USE_EINSTALL}" || "${USE_EINSTALL}" = "0" ]]; then
		debug-print "Installing multilib abi ${ABI} with 'make install'"
		return 0
	else
		debug-print "Installing multilib abi ${ABI} with 'einstall'"
		einstall "${myemakeinstallargs[@]}" "${otheremakeinstallargs[@]}" || die "einstall failed"
		return 1
	fi
}

# @FUNCTION: gnome2-multilib_gdk_pixbuf_update
# @USAGE: gnome2-multilib_gdk_pixbuf_update
# @DESCRIPTION:
# Updates gdk-pixbuf loader cache if GNOME2_ECLASS_GDK_PIXBUF_LOADERS has some.
# This function should be called from pkg_postinst and pkg_postrm; this is a
# multilib-utization of the gnome2-utils API  gnome2_gdk_pixbuf_update.  It supports
# either invocation on a per-abi basis, or singular invocation from a non-multilib
# context (in which case, it will iterate over all the multilib ABIS and update them all).
gnome2-multilib_gdk_pixbuf_update() {
	debug-print-function ${FUNCNAME} "$@"

	# note: fancy/confusing recursion
	if [[ ${MULTILIB_BUILD_ABI} ]] ; then
		local GDK_PIXBUF_QL_PATH=/usr/bin
		local GDK_PIXBUF_QL=gdk-pixbuf-query-loaders
		local GDK_PIXBUF_UPDATE_BIN=something_unlikely_to_exist
		if [[ -x ${EROOT}${GDK_PIXBUF_QL_PATH}/${CHOST}-${GDK_PIXBUF_QL} ]] ; then
			# modern MULTILIB_CHOST_TOOLS-based location
			GDK_PIXBUF_UPDATE_BIN="${EROOT}${GDK_PIXBUF_QL_PATH}/${CHOST}-${GDK_PIXBUF_QL}"
		elif [[ -x ${EROOT}${GDK_PIXBUF_QL_PATH}/${GDK_PIXBUF_QL}-${ABI} ]] ; then
			# legacy MULTILIB_WRAPPED_EXECUTABLES-based location
			GDK_PIXBUF_UPDATE_BIN="${EROOT}${GDK_PIXBUF_QL_PATH}/${GDK_PIXBUF_QL}-${ABI}"
		elif [[ -x ${EROOT}${GDK_PIXBUF_QL_PATH}/${GDK_PIXBUF_QL} ]] ; then
			# standard no-multilib location
			if multilib_build_binaries ; then
				GDK_PIXBUF_UPDATE_BIN="${EROOT}${GDK_PIXBUF_QL_PATH}/${GDK_PIXBUF_QL}"
			else
				ewarn "${FUNCNAME}: ${GDK_PIXBUF_QL} executable not found for ABI \"${ABI}\"."
				return 0
			fi
		else
			ewarn "${FUNCNAME}: ${GDK_PIXBUF_QL} executable not found for ABI \"${ABI}\"."
			return 0
		fi
		gnome2_gdk_pixbuf_update "$@"
	else
		multilib_foreach_abi gnome2-multilib_gdk_pixbuf_update "$@"
	fi
}

# @FUNCTION: gnome2-multilib_query_immodules_gtk2
# @USAGE: gnome2-multilib_query_immodules_gtk2
# @DESCRIPTION:
# Updates gtk2 immodules/gdk-pixbuf loaders listing; if called
# during multilib-build ABI iteration, will attempt to use
# the wrapped query tool.  Otherwise, this will iterate through
# all the ABI's, using the wrapped query tools as appropriate.
gnome2-multilib_query_immodules_gtk2() {
	debug-print-function ${FUNCNAME} "$@"

	if [[ ${MULTILIB_BUILD_ABI} ]] ; then
		local query_exec="${EPREFIX}"/usr/bin/gtk-query-immodules-2.0
		if [[ -x "${EPREFIX}"/usr/bin/gtk-query-immodules-2.0-${MULTILIB_BUILD_ABI} ]]; then
			# legacy MULTILIB_WRAPPED_EXECUTABLES-based location
			query_exec="${EPREFIX}"/usr/bin/gtk-query-immodules-2.0-${MULTILIB_BUILD_ABI}
		elif [[ -x "${EPREFIX}"/usr/bin/${CHOST}-gtk-query-immodules-2.0 ]]; then
			# MULTILIB_CHOST_TOOLS-based location
			query_exec="${EPREFIX}"/usr/bin/${CHOST}-gtk-query-immodules-2.0
		fi
		if [[ ! -x ${query_exec} ]]; then
			debug-print "${FUNCNAME}: Could not find any suitable gtk-query-immodules-2.0"
			return 0
		elif has_version ">=x11-libs/gtk+-2.24.20:2[${MULTILIB_USEDEP}]"; then
			${query_exec} --update-cache
		else
			local GTK2_CONFDIR="/etc/gtk-2.0/$(get_abi_CHOST ${MULTILIB_BUILD_ABI})"
			local gtk_conf="${EPREFIX}${GTK2_CONFDIR}/gtk.immodules"
			local gtk_conf_dir=$(dirname "${gtk_conf}")

			einfo "Generating Gtk2 immodules/gdk-pixbuf loaders listing:"
			einfo "-> ${gtk_conf}"

			mkdir -p "${gtk_conf_dir}"
			local tmp_file=$(mktemp -t tmp.XXXXXXXXXXgtk_query_immodules)
			if [ -z "${tmp_file}" ]; then
				ewarn "gtk_query_immodules: cannot create temporary file"
				return 1
			fi

			if ${query_exec} > "${tmp_file}"; then
				cat "${tmp_file}" > "${gtk_conf}" || \
					ewarn "Failed to write to ${gtk_conf}"
			else
				ewarn "Cannot update gtk.immodules, file generation failed"
			fi
			rm "${tmp_file}"
		fi
	else
		# fancy recursion...
		multilib_foreach_abi gnome2-multilib_query_immodules_gtk2 "$@"
	fi
}

# @FUNCTION: gnome2-multilib_query_immodules_gtk3
# @USAGE: gnome2-multilib_query_immodules_gtk3
# @DESCRIPTION:
# Updates gtk3 immodules/gdk-pixbuf loaders listing.
gnome2-multilib_query_immodules_gtk3() {
	debug-print-function ${FUNCNAME} "$@"

	if [[ ${MULTILIB_BUILD_ABI} ]]; then
		if [[ -f "${EPREFIX}"/usr/bin/gtk-query-immodules-3.0-${MULTILIB_BUILD_ABI} ]]; then
			# legacy MULTILIB_WRAPPED_EXECUTABLES-based location (delme, eventually)
			"${EPREFIX}"/usr/bin/gtk-query-immodules-3.0-${MULTILIB_BUILD_ABI} --update-cache
		elif [[ -f "${EPREFIX}"/usr/bin/${CHOST}-gtk-query-immodules-3.0 ]]; then
			# MULTILIB_CHOST_TOOLS-based location
			"${EPREFIX}"/usr/bin/${CHOST}-gtk-query-immodules-3.0 --update-cache
		else
			"${EPREFIX}"/usr/bin/gtk-query-immodules-3.0 --update-cache
		fi
	else
		multilib_foreach_abi gnome2-multilib_query_immodules_gtk3
	fi
}

# @FUNCTION: gnome2-multilib_gdk_pixbuf_savelist
# @DESCRIPTION:
# Find if there is any gdk-pixbuf loader to install and save the list in
# GNOME2_ECLASS_GDK_PIXBUF_LOADERS_${ABI} variable, or, iterate through the
# multilib ABI's (if iteration is not already underway), calling ourselves
# for each.  This function should be called from pkg_preinst.
gnome2-multilib_gdk_pixbuf_savelist() {
	debug-print-function ${FUNCNAME} "$@"

	if [[ ${MULTILIB_BUILD_ABI} ]]; then
		pushd "${ED}" >/dev/null || die "${FUNCNAME}: Cannot change directory to \"${ED}\"."
		export GNOME2_ECLASS_GDK_PIXBUF_LOADERS=$(find "usr/$(get_libdir)/gdk-pixbuf-2.0" -type f 2>/dev/null)
		popd >/dev/null || die "${FUNCNAME}: popd failed.  no clue what's going on."
	else
		multilib_foreach_abi gnome2-multilib_gdk_pixbuf_savelist "$@"
	fi
}
