# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/eclass/waf-utils.eclass,v 1.18 2013/11/23 04:35:16 jcallen Exp $

# @ECLASS: waf-utils-multilib.eclass
# @MAINTAINER:
# gnome@gentoo.org
# @AUTHOR:
# Original Author: Greg Turner <gmt@be-evil.net>
# @BLURB: common ebuild functions for multi-abi waf-based packages
# @DESCRIPTION:
# The waf-utils-multilib eclass contains functions that make creating ebuild for
# waf-based packages much easier, while supporting multi-abi installation, for example
# using the ABI_X86 USE_EXPAND variable.
# Its main features are support of common portage default settings.

inherit base eutils multilib toolchain-funcs multiprocessing multilib-build

case ${EAPI:-0} in
	4|5) EXPORT_FUNCTIONS src_configure src_compile src_install ;;
	*) die "EAPI=${EAPI} is not supported" ;;
esac

# Python with threads is required to run waf. We do not know which python slot
# is being used as the system interpreter, so we are forced to block all
# slots that have USE=-threads.
DEPEND="${DEPEND}
	dev-lang/python[${MULTILIB_USEDEP}]
	!dev-lang/python[-threads]"

# @ECLASS-VARIABLE: WAF_VERBOSE
# @DESCRIPTION:
# Set to OFF to disable verbose messages during compilation
# this is _not_ meant to be set in ebuilds
: ${WAF_VERBOSE:=ON}

# @FUNCTION: waf-utils_src_configure
# @DESCRIPTION:
# General function for configuring with waf.
waf-utils-multilib_src_configure() {
	debug-print-function ${FUNCNAME} "$@"

	multilib_copy_sources
	multilib_parallel_foreach_abi run_in_build_dir _in_waf_utils_multilib_env \
		_waf-utils-multilib-perabi_src_configure "$@"
}

_in_waf_utils_multilib_env() {
	debug-print-function ${FUNCNAME} "$@"

	local S="${BUILD_DIR}"

	local libdir=""

	# @ECLASS-VARIABLE: WAF_BINARY
	# @DESCRIPTION:
	# not supported in this eclass -- waf must always be at ${S}/waf or use WAF_BINARY_REL
	#
	# @ECLASS-VARIABLE: WAF_BINARY_REL
	# @DESCRIPTION:
	# Provides the location of the waf binary relative to "${S}"

	[[ ${WAF_BINARY} ]] && die "WAF_BINARY overriding is not supported by waf-utils-multilib"
	if [[ ${WAF_BINARY_REL} ]] ; then
		local WAF_BINARY="${S}/${WAF_BINARY_REL}"
	else
		local WAF_BINARY="${S}/waf"
	fi

	# @ECLASS-VARIABLE: NO_WAF_LIBDIR
	# @DEFAULT_UNSET
	# @DESCRIPTION:
	# Variable specifying that you don't want to set the libdir for waf script.
	# Some scripts does not allow setting it at all and die if they find it.
	[[ -z ${NO_WAF_LIBDIR} ]] && libdir="--libdir=${EPREFIX}/usr/$(get_libdir)"

	# when compiling for non-native ABI's, we hit a snag since python starts whispering
	# wrong things into WAF's ear about the native ABI which prevent the correct
	# python libraries being used... whisper the truth as a countermeasure....
	multilib_is_native_abi || local LDFLAGS="-L${EPREFIX}/usr/$(get_libdir)${LDFLAGS+ }${LDFLAGS}"

	MULTILIB_TC_EXPORT_VARS="AR CC CPP CXX RANLIB" multilib_tc_export "$@"
}

_waf-utils-multilib-perabi_src_configure() {
	debug-print-function ${FUNCNAME} "$@"

	echo "CCFLAGS=\"${CFLAGS}\" LINKFLAGS=\"${LDFLAGS}\" \"${WAF_BINARY}\" --prefix=${EPREFIX}/usr ${libdir} $@ configure"

	# This condition is required because waf takes even whitespace as function
	# calls, awesome isn't it?
	if [[ -z ${NO_WAF_LIBDIR} ]]; then
		CCFLAGS="${CFLAGS}" LINKFLAGS="${LDFLAGS}" "${WAF_BINARY}" \
			"--prefix=${EPREFIX}/usr" \
			"${libdir}" \
			"$@" \
			configure || die "configure failed"
	else
		CCFLAGS="${CFLAGS}" LINKFLAGS="${LDFLAGS}" "${WAF_BINARY}" \
			"--prefix=${EPREFIX}/usr" \
			"$@" \
			configure || die "configure failed"
	fi
}

# @FUNCTION: waf-utils_src_compile
# @DESCRIPTION:
# General function for compiling with waf.
waf-utils-multilib_src_compile() {
	debug-print-function ${FUNCNAME} "$@"
	multilib_parallel_foreach_abi run_in_build_dir _in_waf_utils_multilib_env \
		_waf-utils-multilib-perabi_src_compile "$@"
}

_waf-utils-multilib-perabi_src_compile() {
	debug-print-function ${FUNCNAME} "$@"

	local _mywafconfig
	[[ "${WAF_VERBOSE}" ]] && _mywafconfig="--verbose"

	local jobs="--jobs=$(makeopts_jobs)"
	echo "\"${WAF_BINARY}\" build ${_mywafconfig} ${jobs}"
	"${WAF_BINARY}" ${_mywafconfig} ${jobs} || die "build failed"
}

# @FUNCTION: waf-utils_src_install
# @DESCRIPTION:
# Function for installing the package.
waf-utils-multilib_src_install() {
	debug-print-function ${FUNCNAME} "$@"
	_waf-utils-multilib_secure_install() {
		debug-print-function ${FUNCNAME} "$@"
		_in_waf_utils_multilib_env _waf-utils-multilib-perabi_src_install "$@"
		# Do multilib magic only when >1 ABI is used.
		if [[ ${#MULTIBUILD_VARIANTS[@]} -gt 1 ]]; then
			multilib_prepare_wrappers
			# Make sure all headers are the same for each ABI.
			multilib_check_headers
		fi
	}
	multilib_foreach_abi run_in_build_dir _waf-utils-multilib_secure_install "$@"
	# merge any wrapped headers
	multilib_install_wrappers
}

_waf-utils-multilib-perabi_src_install() {
	debug-print-function ${FUNCNAME} "$@"

	echo "\"${WAF_BINARY}\" --destdir=\"${D}\" install"
	"${WAF_BINARY}" --destdir="${D}" install  || die "Make install failed"

	# Manual document installation
	base_src_install_docs
}
