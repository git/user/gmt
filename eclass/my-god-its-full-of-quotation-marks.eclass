# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# @ECLASS: my-god-its-full-of-quotation-marks.eclass 
# @MAINTAINER:
# Gregory M. Turner <gmt@be-evil.net>
# @AUTHOR:
# Gregory M. Turner <gmt@be-evil.net
# @BLURB: pretty printing for lists of arguments possibly containing spaces
# @DESCRIPTION:
# my-god-its-full-of-quotation-marks.eclass provides, unsurprisingly, perhaps, the
# my-god-its-full-of-quotation-marks function.

# @FUNCTION: my-god-its-full-of-quotation-marks
# @USAGE: [list of moderately sane string arguments, each possibly containing spaces]
# @RETURN: 0
# @DESCRIPTION:
# Pretty printer for configure-type arguments, which simply dumps its arguments
# back to stdout after adding quotation marks to ensure unambiguous display
# when some of the arguments may include spaces.  Note that this is designed for
# consumption by human eyeballs, not parsers!!!  Use printf "%q" for the latter.
# Here we concern ourselves only with dismbiguating the display of (otherwise
# presumptively pre-sanitized) lists of arguments which may contain spaces.
#
# Also replaces any backslashes in the arguments with double-backslashes, and then
# any quotation marks in the arguments with \". These pseudo-escapes
# should preserve disambiguation in the face of quotation-mark- or backslash-containing
# arguments.
my-god-its-full-of-quotation-marks() {
	local foo bar
	for foo in "$@" ; do
		# add a space if bar is nonempty
		bar="${bar}${bar:+ }"
		# add pseudo-escapes for '\' and '"'
		foo="${foo//\\/\\\\}"
		foo="${foo//\"/\\\"}"
		case $foo in
			"")
				bar="${bar}\"\""
				;;
			*\ *=*)
				# jesus, wtf
				bar="${bar}\"${foo}\""
				;;
			*=*\ *)
				bar="${bar}${foo%%=*}=\"${foo#*=}\""
				;;
			*\ *)
				bar="${bar}\"${foo}\""
				;;
			*)
				bar="${bar}${foo}"
				;;
		esac
	done
	echo "${bar}"
}

# @FUNCTION: mg-qm
# @USAGE: <stuff>...
# @RETURN: 0
# @DESCRIPTION:
# It is a bit of a pain in the but to keep typing "my-god-its-full-of-quotation-marks" so this
# handy-dandy alias is supplied.  There is no difference between this and the long version.
mg-qm() {
	my-god-its-full-of-quotation-marks "$@"
}
