# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# @ECLASS: clutter-multilib.eclass
# @MAINTAINER:
# Nobody
# @AUTHOR:
# Nirbheek Chauhan <nirbheek@gentoo.org>
# Greg Turner <gmt@be-evil.net>
# @BLURB: Sets SRC_URI, LICENSE, etc and exports src_install function.
# descendant of clutter.eclass which reimplements src_install in a
# multilib-build-compatible manner.

# EAPI=4 is required by our super-eclasses
case ${EAPI:-0} in
	4|5) ;;
	*) die "EAPI=${EAPI} is not supported" ;;
esac

HOMEPAGE="http://www.clutter-project.org/"
inherit eutils ehooker versionator multilib-build

RV=($(get_version_components))
SRC_URI="http://www.clutter-project.org/sources/${PN}/${RV[0]}.${RV[1]}/${P}.tar.bz2"

# All official clutter packages use LGPL-2.1 or later
LICENSE="${LICENSE:-LGPL-2.1+}"

# This will be used by all clutter packages
DEPEND="virtual/pkgconfig"

# @ECLASS-VARIABLE: CLUTTER_LA_PUNT
# @DESCRIPTION:
# Set to anything except 'no' to remove *all* .la files before installing.
# Not to be used without due consideration, sometimes .la files *are* needed.
CLUTTER_LA_PUNT="${CLUTTER_LA_PUNT:-"no"}"

# @ECLASS-VARIABLE: DOCS
# @DESCRIPTION:
# This variable holds an array of relative paths of files to be dodoc-ed.
# By default, it contains the standard list of autotools doc files.
# Because clutter declares it as a non-array by default, we adapt it here;
# this way, if other -multilib eclasses (most of which use an array for DOCS)
# are involved at the ebuild level, the right thing is more likely to happen
if [[ $(declare -p DOCS 2>/dev/null ) != declare\ -a* ]] ; then
	declare -a DOCS=( ${DOCS} )
fi

DOCS+=( AUTHORS ChangeLog NEWS README TODO )

# @ECLASS-VARIABLE: EXAMPLES
# @DESCRIPTION:
# This variable holds relative paths of files to be added as examples when the
# "examples" USE-flag exists, and is switched on. Bash expressions can be used
# since the variable is eval-ed before substitution. Empty by default.
EXAMPLES="${EXAMPLES:-""}"

# @FUNCTION: clutter-multilib_src_install
# @DESCRIPTION:
# Runs emake install for each abi, dodoc, and installs examples
# Performs @GET_LIBDIR@ substitution on arguments
# Fires the clutter-multilib-{pre,post}_src_install ehooks
# Checks header installation for abi-conflicts
clutter-multilib_src_install() {
	debug-print-function "${FUNCNAME}" "$@"

	if [[ $(declare -p DOCS) != declare\ -a* ]] ; then
		declare -a DOCS=( ${DOCS} )
	fi

	_clutter_multilib_secure_install() { 
		debug-print-function "${FUNCNAME}" "$@"

		# perform @GET_LIBDIR@ substitution on arguments
		declare -a clutter_multilib_install_args=()
		for arg in DESTDIR="${D}" install "$@" ; do
			clutter_multilib_install_args+=( "$(get_libdir_subst "${arg}")" )
		done

		#ehook_fire and emake install
		if ehook_fire clutter-multilib-per-abi-pre_src_install ; then
			if ( ! multilib_is_native_abi ) || ehook_fire clutter-multilib-best-abi-pre_src_install ; then
				emake "${clutter_multilib_install_args[@]}"
			fi
		fi
		multilib_is_native_abi && ehook_fire clutter-multilib-best-abi-post_src_install -u
		ehook_fire clutter-multilib-per-abi-post_src_install -u

		# Do multilib magic only when >1 ABI is used.
		if [[ ${#MULTIBUILD_VARIANTS[@]} -gt 1 ]]; then
			multilib_prepare_wrappers
			# Make sure all headers are the same for each ABI.
			multilib_check_headers
		fi
	}

	if ehook_fire clutter-multilib-global-pre_src_install ; then
		multilib_foreach_abi multilib-build_run_in_build_dir _clutter_multilib_secure_install "$@"
		multilib_install_wrappers

		pushd "${S}" > /dev/null || die

		dodoc "${DOCS[@]}"

		# examples
		if has examples ${IUSE} && use examples; then
			insinto /usr/share/doc/${PF}/examples

			# We use eval to be able to use globs and other bash expressions
			for example in $(eval echo ${EXAMPLES}); do
				# If directory
				if [[ ${example: -1} == "/" ]]; then
					doins -r ${example}
				else
					doins ${example}
				fi
			done
		fi

		popd > /dev/null || die

		# Delete all .la files
		if [[ "${CLUTTER_LA_PUNT}" != "no" ]]; then
			prune_libtool_files --all
		fi
	fi
	pushd "${S}" > /dev/null || die
	ehook_fire clutter-multilib-global-post_src_install -u
	popd > /dev/null || die
}

EXPORT_FUNCTIONS src_install
