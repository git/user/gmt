# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# @ECLASS: autotools-multilib.eclass
# @MAINTAINER:
# gx86-multilib team <multilib@gentoo.org>
# @AUTHOR:
# Author: Michał Górny <mgorny@gentoo.org>
# @BLURB: autotools-utils wrapper for multilib builds
# @DESCRIPTION:
# The autotools-multilib.eclass provides glue between
# autotools-utils.eclass(5) and multilib-minimal.eclass(5), aiming
# to provide a convenient way to build packages using autotools
# for multiple ABIs.
#
# Inheriting this eclass sets IUSE and exports default multilib_src_*()
# sub-phases that call autotools-utils phase functions for each ABI
# enabled, triggering various ehooks along the way in order to facilitate
# eclass and ebuild polymorphism.  The multilib_src_*() functions can
# also be defined in ebuild just as in multilib-minimal.

# EAPI=4 is required for meaningful MULTILIB_USEDEP.
case ${EAPI:-0} in
	4|5) ;;
	*) die "EAPI=${EAPI} is not supported" ;;
esac

inherit autotools-utils eutils multilib-minimal ehooker multilib my-god-its-full-of-quotation-marks

EXPORT_FUNCTIONS src_prepare src_configure src_compile src_test src_install

# Note: _at_args[@] passing is a backwards compatibility measure.
# Don't use it in new packages.

autotools-multilib_src_prepare() {
	debug-print-function ${FUNCNAME} "$(mg-qm "$@")"
	
	if ehook_fire autotools-multilib-global-pre_src_prepare ; then
		autotools-utils_src_prepare "${@}"
		[[ ${AUTOTOOLS_IN_SOURCE_BUILD} ]] && multilib_copy_sources
	fi
	ehook_fire autotools-multilib-global-post_src_prepare -u
}

# generate ehook listeners for autotools-utils per-phase pre/post ehooks, which
# fire an equivalent (but less confusingly named) autotools-multilib-level e-hook.
_autotools-multilib_wrap_autotools_utils_phase_hooks() {
	local phase evaltext
	for phase in src_configure src_compile src_test src_install ; do
		read -r -d '' evaltext <<-EOF
			_autotools-multilib-autotools-utils-pre_${phase}_wrapper() {
				debug-print-function \${FUNCNAME} "\$(mg-qm "\$@")"
				ehook_fire autotools-multilib-per-abi-pre_${phase}
				local rslt=\$?
				if [[ \${rslt} == 0 ]] ; then
					if multilib_is_native_abi ; then
						ehook_fire autotools-multilib-best-abi-pre_${phase}
						rslt=\$?
					fi
				fi
				[[ \$rslt == 0 ]] && _AUTOTOOLS_MULTILIB_PHASE=${phase} ehook_fire autotools-multilib-internal-per-abi_postproc -u
				return \$rslt
			}
			_autotools-multilib-autotools-utils-post_${phase}_wrapper() {
				debug-print-function \${FUNCNAME} "\$(mg-qm "\$@")"
				ehook_fire autotools-multilib-per-abi-post_${phase} -u
				if multilib_is_native_abi ; then
					ehook_fire autotools-multilib-best-abi-post_${phase} -u
				fi
			}
		EOF
		eval "${evaltext}"
	done
	_AUTOTOOLS_MULTILIB_AUTOTOOLS_UTILS_HOOKS_WRAPPED=yes
}
[[ ${_AUTOTOOLS_MULTILIB_AUTOTOOLS_UTILS_HOOKS_WRAPPED} ]] || \
	_autotools-multilib_wrap_autotools_utils_phase_hooks

# do @GET_LIBDIR@ substitution in each of the named arrays
_autotools-multilib_libdir_subst() {
	local _array _array_reference _item
	for _array in "$@"; do
		local _new_array=()
		_array_reference="${_array}[@]"
		for _item in "${!_array_reference}"; do
			_new_array+=("${_item//@GET_LIBDIR@/$(get_libdir)}")
		done
		eval "${_array}=(\"\${_new_array[@]}\")"
	done
}

ehook autotools-multilib-internal-per-abi_postproc _autotools-multilib_abi_postproc
# munge @GET_LIBDIR@ arguments flowing into phase actions as appropriate
_autotools-multilib_abi_postproc() {
	case ${_AUTOTOOLS_MULTILIB_PHASE} in
		src_configure) _autotools-multilib_libdir_subst myeconfargs othereconfargs ;;
		src_compile) _autotools-multilib_libdir_subst myemakeargs otheremakeargs ;;
		src_test) _autotools-multilib_libdir_subst myemaketestargs otheremaketestargs ;;
		src_install) _autotools-multilib_libdir_subst myemakeinstallargs otheremakeinstallargs ;;
		*) die "Don't know what to do about phase \"${_AUTOTOOLS_MULTILIB_PHASE}\"" ;;
	esac
}

multilib_src_configure() {
	debug-print-function ${FUNCNAME} "$(mg-qm "$@")"
	[[ ${AUTOTOOLS_IN_SOURCE_BUILD} ]] && local ECONF_SOURCE=${BUILD_DIR}
	ehook autotools-utils-pre_src_configure _autotools-multilib-autotools-utils-pre_src_configure_wrapper
	ehook autotools-utils-post_src_configure _autotools-multilib-autotools-utils-post_src_configure_wrapper
	autotools-utils_src_configure "${_at_args[@]}"
	eunhook autotools-utils-pre_src_configure _autotools-multilib-autotools-utils-pre_src_configure_wrapper
	eunhook autotools-utils-post_src_configure _autotools-multilib-autotools-utils-post_src_configure_wrapper
}

autotools-multilib_src_configure() {
	debug-print-function ${FUNCNAME} "$(mg-qm "$@")"
	local _at_args=( "${@}" )
	ehook_fire autotools-multilib-global-pre_src_configure && \
		multilib-minimal_src_configure
	ehook_fire autotools-multilib-global-post_src_configure -u
}

multilib_src_compile() {
	debug-print-function ${FUNCNAME} "$(mg-qm "$@")"
	ehook autotools-utils-pre_src_compile _autotools-multilib-autotools-utils-pre_src_compile_wrapper
	ehook autotools-utils-post_src_compile _autotools-multilib-autotools-utils-post_src_compile_wrapper
	autotools-utils_src_compile "${_at_args[@]}"
	eunhook autotools-utils-pre_src_compile _autotools-multilib-autotools-utils-pre_src_compile_wrapper
	eunhook autotools-utils-post_src_compile _autotools-multilib-autotools-utils-post_src_compile_wrapper
}

autotools-multilib_src_compile() {
	debug-print-function ${FUNCNAME} "$(mg-qm "$@")"
	local _at_args=( "${@}" )
	ehook_fire autotools-multilib-global-pre_src_compile && \
		multilib-minimal_src_compile
	ehook_fire autotools-multilib-global-post_src_compile -u
}

multilib_src_test() {
	debug-print-function ${FUNCNAME} "$(mg-qm "$@")"
	ehook autotools-utils-pre_src_test _autotools-multilib-autotools-utils-pre_src_test_wrapper
	ehook autotools-utils-post_src_test _autotools-multilib-autotools-utils-post_src_test_wrapper
	autotools-utils_src_test "${_at_args[@]}"
	eunhook autotools-utils-pre_src_test _autotools-multilib-autotools-utils-pre_src_test_wrapper
	eunhook autotools-utils-post_src_test _autotools-multilib-autotools-utils-post_src_test_wrapper
}

autotools-multilib_src_test() {
	debug-print-function ${FUNCNAME} "$(mg-qm "$@")"
	local _at_args=( "${@}" )
	ehook_fire autotools-multilib-global-pre_src_test && \
		multilib-minimal_src_test
	ehook_fire autotools-multilib-global-post_src_test -u
}

multilib_src_install() {
	debug-print-function ${FUNCNAME} "$(mg-qm "$@")"
	# override some variables that autotools-utils would attempt to process, but
	# for which we have alternate processing in multilib_src_install_all.  This
	# prevents pointless repeat-installation of nongenerated documentation.
	local AUTOTOOLS_PRUNE_LIBTOOL_FILES="none" DOCS=() HTML_DOCS=()
	ehook autotools-utils-pre_src_install _autotools-multilib-autotools-utils-pre_src_install_wrapper
	ehook autotools-utils-post_src_install _autotools-multilib-autotools-utils-post_src_install_wrapper
	autotools-utils_src_install "${_at_args[@]}"
	eunhook autotools-utils-pre_src_install _autotools-multilib-autotools-utils-pre_src_install_wrapper
	eunhook autotools-utils-post_src_install _autotools-multilib-autotools-utils-post_src_install_wrapper
}

multilib_src_install_all() {
	debug-print-function ${FUNCNAME} "$(mg-qm "$@")"
	einstalldocs

	# Remove libtool files and unnecessary static libs
	local prune_ltfiles=${AUTOTOOLS_PRUNE_LIBTOOL_FILES}
	if [[ ${prune_ltfiles} != none ]]; then
		prune_libtool_files ${prune_ltfiles:+--${prune_ltfiles}}
	fi
}

autotools-multilib_src_install() {
	debug-print-function ${FUNCNAME} "$(mg-qm "$@")"
	local _at_args=( "${@}" )
	ehook_fire autotools-multilib-global-pre_src_install && \
		multilib-minimal_src_install
	ehook_fire autotools-multilib-global-post_src_install -u
}
