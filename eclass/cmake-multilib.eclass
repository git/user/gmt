# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# @ECLASS: cmake-multilib.eclass
# @MAINTAINER:
# gx86-multilib team <multilib@gentoo.org>
# @AUTHOR:
# Author: Michał Górny <mgorny@gentoo.org>
# @BLURB: cmake-utils wrapper for multilib builds
# @DESCRIPTION:
# The cmake-multilib.eclass provides a glue between cmake-utils.eclass(5)
# and multilib-minimal.eclass(5), aiming to provide a convenient way
# to build packages using cmake for multiple ABIs.
#
# Inheriting this eclass sets IUSE and exports default multilib_src_*()
# sub-phases that call cmake-utils phase functions for each ABI enabled.
# The multilib_src_*() functions can be defined in ebuild just like
# in multilib-minimal, yet they ought to call appropriate cmake-utils
# phase rather than 'default'.

# EAPI=5 is required for meaningful MULTILIB_USEDEP.
case ${EAPI:-0} in
	5) :;;
	*) die "EAPI=${EAPI} is not supported" ;;
esac

inherit my-god-its-full-of-quotation-marks ehooker cmake-utils multilib-minimal ehooker

EXPORT_FUNCTIONS src_prepare src_configure src_compile src_test src_install

cmake-multilib_src_prepare() {
	debug-print-function "${FUNCNAME}" "$(mg-qm "$@")"

	if ehook_fire cmake-multilib-global-pre_src_prepare; then
		cmake-utils_src_prepare "$@"
		[[ ${CMAKE_IN_SOURCE_BUILD} ]] && multilib_copy_sources
	fi
	ehook_fire cmake-multilib-global-post_src_prepare -u
}

# this may not be neccesary but for now it's being left in as a backwards-
# compatbility paranoia thing.  Worth seeing what happens if we get rid of it
_cmake-multilib_localize_vars() {
    debug-print-function ${FUNCNAME} "$(mg-qm "$@")"

    # Forget these variables after each phase is processed
    local S="${S}"
    local BUILD_DIR="${BUILD_DIR}"
    local ECONF_SOURCE="${ECONF_SOURCE}"

    "$@"
}

# generate ehook listeners for cmake-utils per-phase pre/post ehooks, which wrap 
# near-equivalent (but less confusingly named) cmake-multilib e-hooks.
_cmake-multilib_wrap_cmake_utils_phase_hooks() {
	local phase evaltext
	for phase in src_configure src_compile src_test src_install; do
		read -r -d '' evaltext <<-EOF
			_cmake-multilib-cmake-utils-pre_${phase}_wrapper() {
				debug-print-function \${FUNCNAME} "\$(mg-qm "\$@")"
				ehook_fire cmake-multilib-per-abi-pre_${phase}
				local rslt=\$?
				if [[ \${rslt} == 0 ]] ; then
					if multilib_is_native_abi ; then
						ehook_fire cmake-multilib-best-abi-pre_${phase}
						rslt=\$?
					fi
				fi
				[[ \$rslt == 0 ]] && _CMAKE_MULTILIB_PHASE=${phase} ehook_fire cmake-multilib-internal-per-abi_postproc -u
				return \$rslt
			}
			_cmake-multilib-cmake-utils-post_${phase}_wrapper() {
				debug-print-function \${FUNCNAME} "\$(mg-qm "\$@")"
				ehook_fire cmake-multilib-per-abi-post_${phase} -u
				if multilib_is_native_abi ; then
					ehook_fire cmake-multilib-best-abi-post_${phase} -u
				fi
			}
		EOF
		eval "${evaltext}"
	done
	_CMAKE_MULTILIB_CMAKE_UTILS_HOOKS_WRAPPED=yes
}

[[ ${_CMAKE_MULTILIB_CMAKE_UTILS_HOOKS_WRAPPED} ]] || \
	_cmake-multilib_wrap_cmake_utils_phase_hooks

# do @GET_LIBDIR@ substitution in each of the named arrays
_cmake-multilib_libdir_subst() {
	local _array _array_reference _item
	for _array in "$@"; do
		local _new_array=()
		_array_reference="${_array}[@]"
		for _item in "${!_array_reference}"; do
			_new_array+=("${_item//@GET_LIBDIR@/$(get_libdir)}")
		done
		eval "${_array}=(\"\${_new_array[@]}\")"
	done
}

ehook cmake-multilib-internal-per-abi_postproc _cmake-multilib_abi_postproc
# munge @GET_LIBDIR@ arguments flowing into phase actions as appropriate
_cmake-multilib_abi_postproc() {
	case ${_CMAKE_MULTILIB_PHASE} in
		src_configure) _cmake-multilib_libdir_subst mycmakeargs ;;
		src_compile) _cmake-multilib_libdir_subst myninjamakeargs mycmakemakeargs;;
		src_test) _cmake-multilib_libdir_subst myctestargs ;;
		src_install) _cmake-multilib_libdir_subst mycmakeinstallargs ;;
		*) die "Don't know what to do about phase \"${_CMAKE_MULTILIB_PHASE}\"" ;;
	esac
}

multilib_src_configure() {
	debug-print-function "${FUNCNAME}" "$(mg-qm "$@")"

	if [[ ${CMAKE_MULTILIB_IN_SOURCE_BUILD} ]]; then
		local ECONF_SOURCE=${BUILD_DIR}
		local CMAKE_USE_DIR=${BUILD_DIR}
	fi
	ehook cmake-utils-pre_src_configure _cmake-multilib-cmake-utils-pre_src_configure_wrapper
	ehook cmake-utils-post_src_configure _cmake-multilib-cmake-utils-post_src_configure_wrapper
	_cmake-multilib_localize_vars cmake-utils_src_configure "${_cm_args[@]}"
	eunhook cmake-utils-pre_src_configure _cmake-multilib-cmake-utils-pre_src_configure_wrapper
	eunhook cmake-utils-post_src_configure _cmake-multilib-cmake-utils-post_src_configure_wrapper
}

cmake-multilib_src_configure() {
	debug-print-function ${FUNCNAME} "$(mg-qm "$@")"

	local _cm_args=( "$@" )
	ehook_fire cmake-multilib-global-pre_src_configure && \
		multilib-minimal_src_configure
	ehook_fire cmake-multilib-global-post_src_configure -u
}

multilib_src_compile() {
	debug-print-function "${FUNCNAME}" "$(mg-qm "$@")"

	[[ ${CMAKE_MULTILIB_IN_SOURCE_BUILD} ]] && local CMAKE_USE_DIR=${BUILD_DIR}
	ehook cmake-utils-pre_src_compile _cmake-multilib-cmake-utils-pre_src_compile_wrapper
	ehook cmake-utils-post_src_compile _cmake-multilib-cmake-utils-post_src_compile_wrapper
	_cmake-multilib_localize_vars cmake-utils_src_compile "${_cm_args[@]}"
	eunhook cmake-utils-pre_src_compile _cmake-multilib-cmake-utils-pre_src_compile_wrapper
	eunhook cmake-utils-post_src_compile _cmake-multilib-cmake-utils-post_src_compile_wrapper
}

cmake-multilib_src_compile() {
	debug-print-function ${FUNCNAME} "$(mg-qm "$@")"

	local _cm_args=( "$@" )
	ehook_fire cmake-multilib-global-pre_src_compile && \
		multilib-minimal_src_compile
	ehook_fire cmake-multilib-global-post_src_compile -u
}

multilib_src_test() {
	debug-print-function "${FUNCNAME}" "$(mg-qm "$@")"

	[[ ${CMAKE_MULTILIB_IN_SOURCE_BUILD} ]] && local CMAKE_USE_DIR=${BUILD_DIR}
	ehook cmake-utils-pre_src_test _cmake-multilib-cmake-utils-pre_src_test_wrapper
	ehook cmake-utils-post_src_test _cmake-multilib-cmake-utils-post_src_test_wrapper
	_cmake-multilib_localize_vars cmake-utils_src_test "${_cm_args[@]}"
	eunhook cmake-utils-pre_src_test _cmake-multilib-cmake-utils-pre_src_test_wrapper
	eunhook cmake-utils-post_src_test _cmake-multilib-cmake-utils-post_src_test_wrapper
}

cmake-multilib_src_test() {
	debug-print-function ${FUNCNAME} "$(mg-qm "$@")"

	local _cm_args=( "$@" )
	ehook_fire cmake-multilib-global-pre_src_test && \
		multilib-minimal_src_test
	ehook_fire cmake-multilib-global-post_src_test -u
}

multilib_src_install() {
	debug-print-function "${FUNCNAME}" "$(mg-qm "$@")"

	[[ ${CMAKE_MULTILIB_IN_SOURCE_BUILD} ]] && local CMAKE_USE_DIR=${BUILD_DIR}
	# override some variables that cmake-utils might attempt to process, but
	# for which we have alternate processing in multilib_src_install_all.  This
	# prevents pointless repeat-installation of nongenerated documentation.
	local DOCS=() HTML_DOCS=()
	ehook cmake-utils-pre_src_install _cmake-multilib-cmake-utils-pre_src_install_wrapper
	ehook cmake-utils-post_src_install _cmake-multilib-cmake-utils-post_src_install_wrapper
	_cmake-multilib_localize_vars cmake-utils_src_install "${_cm_args[@]}"
	eunhook cmake-utils-pre_src_install _cmake-multilib-cmake-utils-pre_src_install_wrapper
	eunhook cmake-utils-post_src_install _cmake-multilib-cmake-utils-post_src_install_wrapper
}

multilib_src_install_all() {
	debug-print-function ${FUNCNAME} "$(mg-qm  "$@")"
	einstalldocs
}

cmake-multilib_src_install() {
	debug-print-function "${FUNCNAME}" "$(mg-qm "$@")"

	local _cm_args=( "$@" )
	ehook_fire cmake-multilib-global-pre_src_install && \
		multilib-minimal_src_install
	ehook_fire cmake-multilib-global-post_src_install -u
}
