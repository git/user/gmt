# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# @ECLASS: autotools-utils.eclass
# @MAINTAINER:
# Maciej Mrozowski <reavertm@gentoo.org>
# Michał Górny <mgorny@gentoo.org>
# @BLURB: common ebuild functions for autotools-based packages
# @DESCRIPTION:
# autotools-utils.eclass is autotools.eclass(5) and base.eclass(5) wrapper
# providing all inherited features along with econf arguments as Bash array,
# out of source build with overridable build dir location, static archives
# handling, libtool files removal.
#
# Please note that autotools-utils does not support mixing of its phase
# functions with regular econf/emake calls. If necessary, please call
# autotools-utils_src_compile instead of the latter.
#
# @EXAMPLE:
# Typical ebuild using autotools-utils.eclass:
#
# @CODE
# EAPI="2"
#
# inherit autotools-utils
#
# DESCRIPTION="Foo bar application"
# HOMEPAGE="http://example.org/foo/"
# SRC_URI="mirror://sourceforge/foo/${P}.tar.bz2"
#
# LICENSE="LGPL-2.1"
# KEYWORDS=""
# SLOT="0"
# IUSE="debug doc examples qt4 static-libs tiff"
#
# CDEPEND="
# 	media-libs/libpng:0
# 	qt4? (
# 		dev-qt/qtcore:4
# 		dev-qt/qtgui:4
# 	)
# 	tiff? ( media-libs/tiff:0 )
# "
# RDEPEND="${CDEPEND}
# 	!media-gfx/bar
# "
# DEPEND="${CDEPEND}
# 	doc? ( app-doc/doxygen )
# "
#
# # bug 123456
# AUTOTOOLS_IN_SOURCE_BUILD=1
#
# DOCS=(AUTHORS ChangeLog README "Read me.txt" TODO)
#
# PATCHES=(
# 	"${FILESDIR}/${P}-gcc44.patch" # bug 123458
# 	"${FILESDIR}/${P}-as-needed.patch"
# 	"${FILESDIR}/${P}-unbundle_libpng.patch"
# )
#
# src_configure() {
# 	local myeconfargs=(
# 		$(use_enable debug)
# 		$(use_with qt4)
# 		$(use_enable threads multithreading)
# 		$(use_with tiff)
# 	)
# 	autotools-utils_src_configure
# }
#
# src_compile() {
# 	autotools-utils_src_compile
# 	use doc && autotools-utils_src_compile docs
# }
#
# src_install() {
# 	use doc && HTML_DOCS=("${BUILD_DIR}/apidocs/html/")
# 	autotools-utils_src_install
# 	if use examples; then
# 		dobin "${BUILD_DIR}"/foo_example{1,2,3} \\
# 			|| die 'dobin examples failed'
# 	fi
# }
#
# @CODE

# Keep variable names synced with cmake-utils and the other way around!

case ${EAPI:-0} in
	2|3|4|5) ;;
	*) die "EAPI=${EAPI} is not supported" ;;
esac

# @ECLASS-VARIABLE: AUTOTOOLS_AUTORECONF
# @DEFAULT_UNSET
# @DESCRIPTION:
# When set to a non-empty value, autotools-utils.eclass will
# automatically generate autotools dependencies and run autoreconf
# during autotools-utils_src_prepare.
#
# This is usually necessary when using live sources or applying patches
# modifying configure.ac or Makefile.am files. Note that in the latter case
# setting this variable is obligatory even though the eclass will work without
# it (to add the necessary dependencies).
#
# The eclass will try to determine the correct autotools to run including a few
# external tools: gettext, glib-gettext, intltool, gtk-doc, gnome-doc-prepare.
# If your tool is not supported, please open a bug and we'll add support for it.
#
# Note that dependencies are added for autoconf, automake and libtool only.
# If your package needs one of the external tools listed above, you need to add
# appropriate packages to DEPEND yourself.
[[ ${AUTOTOOLS_AUTORECONF} ]] || : ${AUTOTOOLS_AUTO_DEPEND:=no}

inherit autotools eutils libtool ehooker

EXPORT_FUNCTIONS src_prepare src_configure src_compile src_install src_test

# @ECLASS-VARIABLE: BUILD_DIR
# @DEFAULT_UNSET
# @DESCRIPTION:
# Build directory, location where all autotools generated files should be
# placed. For out of source builds it defaults to ${WORKDIR}/${P}_build.
#
# This variable was formerly called AUTOTOOLS_BUILD_DIR. The old name
# is deprcated, but partially still supported by autotools-utils.eclass
# (a QA warning will be emitted if it is used).

# @ECLASS-VARIABLE: AUTOTOOLS_IN_SOURCE_BUILD
# @DEFAULT_UNSET
# @DESCRIPTION:
# Set to enable in-source build.

# @ECLASS-VARIABLE: ECONF_SOURCE
# @DEFAULT_UNSET
# @DESCRIPTION:
# Specify location of autotools' configure script. By default it uses ${S}.

# @ECLASS-VARIABLE: DOCS
# @DEFAULT_UNSET
# @DESCRIPTION:
# Array containing documents passed to dodoc command.
#
# In EAPIs 4+, can list directories as well.
#
# Example:
# @CODE
# DOCS=( NEWS README )
# @CODE

# @ECLASS-VARIABLE: HTML_DOCS
# @DEFAULT_UNSET
# @DESCRIPTION:
# Array containing documents passed to dohtml command.
#
# Example:
# @CODE
# HTML_DOCS=( doc/html/ )
# @CODE

# @ECLASS-VARIABLE: PATCHES
# @DEFAULT_UNSET
# @DESCRIPTION:
# PATCHES array variable containing all various patches to be applied.
#
# Example:
# @CODE
# PATCHES=( "${FILESDIR}"/${P}-mypatch.patch )
# @CODE

# @ECLASS-VARIABLE: AUTOTOOLS_PRUNE_LIBTOOL_FILES
# @DEFAULT_UNSET
# @DESCRIPTION:
# Sets the mode of pruning libtool files. The values correspond to
# prune_libtool_files parameters, with leading dashes stripped.
#
# Defaults to pruning the libtool files when static libraries are not
# installed or can be linked properly without them. Libtool files
# for modules (plugins) will be kept in case plugin loader needs them.
#
# If set to 'modules', the .la files for modules will be removed
# as well. This is often the preferred option.
#
# If set to 'all', all .la files will be removed unconditionally. This
# option is discouraged and shall be used only if 'modules' does not
# remove the files.
#
# If set to 'none', no .la files will be pruned ever. Use in corner
# cases only.

# @ECLASS-VARIABLE: AUTOTOOLS_TEST_MAKE
# @DEFAULT_UNSET
# @DESCRIPTION:
# Set this to the name of an executable or function to invoke
# instead of "emake" during the src_test phase.  If unset,
# emake is used.
#
# Example:
# @CODE
# AUTOTOOLS_TEST_MAKE=Xemake
# @CODE

# Determine using IN or OUT source build
_check_build_dir() {
	debug-print-function ${FUNCNAME} "$@"
	: ${ECONF_SOURCE:=${S}}
	# Respect both the old variable and the new one, depending
	# on which one was set by the ebuild.
	if [[ ! ${BUILD_DIR} && ${AUTOTOOLS_BUILD_DIR} ]]; then
		eqawarn "The AUTOTOOLS_BUILD_DIR variable has been renamed to BUILD_DIR."
		eqawarn "Please migrate the ebuild to use the new one."

		# In the next call, both variables will be set already
		# and we'd have to know which one takes precedence.
		_RESPECT_AUTOTOOLS_BUILD_DIR=1
	fi

	if [[ ${_RESPECT_AUTOTOOLS_BUILD_DIR} ]]; then
		BUILD_DIR=${AUTOTOOLS_BUILD_DIR:-${WORKDIR}/${P}_build}
	else
		if [[ -n ${AUTOTOOLS_IN_SOURCE_BUILD} ]]; then
			: ${BUILD_DIR:=${ECONF_SOURCE}}
		else
			: ${BUILD_DIR:=${WORKDIR}/${P}_build}
		fi
	fi

	# Backwards compatibility for getting the value.
	AUTOTOOLS_BUILD_DIR=${BUILD_DIR}
	echo ">>> Working in BUILD_DIR: \"${BUILD_DIR}\""
}

# @FUNCTION: autotools-utils_run_in_build_dir
# @USAGE: [command [arguments]]
# @RETURN: result of command
# @DESCRIPTION:
# Sets up BUILD_DIR variable, if neccesary, and creates the
# directory, if needed.  Then, sets the current working directory
# to ${BUILD_DIR} and runs the specified command -- similar to
# run_in_build_dir from multibuild.eclass, but respects the
# legacy BUILD_DIR naming semantics supported by autotools-utils,
# and ensures the directory exists before running the command.
autotools-utils_run_in_build_dir() {
	_check_build_dir
	if [[ ! -d "${BUILD_DIR}" ]] ; then
		mkdir "${BUILD_DIR}" || \
			die "Could not create directory \"${BUILD_DIR}\" in which to run $*"
	fi
	run_in_build_dir "$@"
}

# @FUNCTION: remove_libtool_files
# @USAGE: [all]
# @DESCRIPTION:
# Determines unnecessary libtool files (.la) and libtool static archives (.a)
# and removes them from installation image.
#
# To unconditionally remove all libtool files, pass 'all' as argument.
# Otherwise, libtool archives required for static linking will be preserved.
#
# In most cases it's not necessary to manually invoke this function.
# See autotools-utils_src_install for reference.
remove_libtool_files() {
	debug-print-function ${FUNCNAME} "$@"
	local removing_all

	eqawarn "The remove_libtool_files() function was deprecated."
	eqawarn "Please use prune_libtool_files() from eutils eclass instead."

	[[ ${#} -le 1 ]] || die "Invalid number of args to ${FUNCNAME}()"
	if [[ ${#} -eq 1 ]]; then
		case "${1}" in
			all)
				removing_all=1
				;;
			*)
				die "Invalid argument to ${FUNCNAME}(): ${1}"
		esac
	fi

	local pc_libs=()
	if [[ ! ${removing_all} ]]; then
		local arg
		for arg in $(find "${D}" -name '*.pc' -exec \
					sed -n -e 's;^Libs:;;p' {} +); do
			[[ ${arg} == -l* ]] && pc_libs+=(lib${arg#-l}.la)
		done
	fi

	local f
	find "${D}" -type f -name '*.la' -print0 | while read -r -d '' f; do
		local shouldnotlink=$(sed -ne '/^shouldnotlink=yes$/p' "${f}")
		local archivefile=${f/%.la/.a}
		[[ "${f}" != "${archivefile}" ]] || die 'regex sanity check failed'

		# Remove static libs we're not supposed to link against.
		if [[ ${shouldnotlink} ]]; then
			einfo "Removing unnecessary ${archivefile#${D%/}}"
			rm -f "${archivefile}" || die
			# The .la file may be used by a module loader, so avoid removing it
			# unless explicitly requested.
			[[ ${removing_all} ]] || continue
		fi

		# Remove .la files when:
		# - user explicitly wants us to remove all .la files,
		# - respective static archive doesn't exist,
		# - they are covered by a .pc file already,
		# - they don't provide any new information (no libs & no flags).
		local removing
		if [[ ${removing_all} ]]; then removing='forced'
		elif [[ ! -f ${archivefile} ]]; then removing='no static archive'
		elif has "$(basename "${f}")" "${pc_libs[@]}"; then
			removing='covered by .pc'
		elif [[ ! $(sed -n -e \
			"s/^\(dependency_libs\|inherited_linker_flags\)='\(.*\)'$/\2/p" \
			"${f}") ]]; then removing='no libs & flags'
		fi

		if [[ ${removing} ]]; then
			einfo "Removing unnecessary ${f#${D%/}} (${removing})"
			rm -f "${f}" || die
		fi
	done
}

# @FUNCTION: autotools-utils_src_prepare
# @DESCRIPTION:
# The src_prepare function.
#
# Supporting PATCHES array and user patches. See base.eclass(5) for reference.
autotools-utils_src_prepare() {
	debug-print-function ${FUNCNAME} "$@"

	local want_autoreconf=${AUTOTOOLS_AUTORECONF}

	[[ ${PATCHES} ]] && epatch "${PATCHES[@]}"

	at_checksum() {
		find '(' -name 'Makefile.am' \
			-o -name 'configure.ac' \
			-o -name 'configure.in' ')' \
			-exec cksum {} + | sort -k2
	}

	[[ ! ${want_autoreconf} ]] && local checksum=$(at_checksum)
	epatch_user
	if [[ ! ${want_autoreconf} ]]; then
		if [[ ${checksum} != $(at_checksum) ]]; then
			einfo 'Will autoreconfigure due to user patches applied.'
			want_autoreconf=yep
		fi
	fi

	local _autotools_utils_src_prepare_result=0
	if ehook_fire autotools-utils-pre_src_prepare ; then
		[[ ${want_autoreconf} ]] && { eautoreconf ; _autotools_utils_src_prepare_result=$? ; }
		[[ ${AT_NOELIBTOOLIZE} != yes ]] && { elibtoolize ; _autotools_utils_src_prepare_result=$? ; }
	fi
	ehook_fire autotools-utils-post_src_prepare -u
	return $_autotools_utils_src_prepare_result
}

# @VARIABLE: AUTOTOOLS_NO_AUTO_STATIC_LIBS_CONFARGS
# @DEFAULT_UNSET
# @DESCRIPTION:
# Normally, if autotools-utils detects "static-libs" in the use-flags of an ebuild,
# it automatically adds "--enable-static" and "--enable-shared" arguments to the
# configure arguments.  If these are unknown to configure, an error message will be
# issued.  Setting this variable to a non-empty value will cause these arguments not
# to be automatically generated.

# @VARIABLE: myeconfargs
# @DEFAULT_UNSET
# @DESCRIPTION:
# Optional econf arguments as Bash array. Should be defined before calling src_configure.
# If modified during autotools-utils-pre_src_configure ehook processing (but no ehook
# supresses configure entirely by returning a nonzero value), then those changes will
# be respected during econf invocation.
# @CODE
# src_configure() {
# 	local myeconfargs=(
# 		--disable-readline
# 		--with-confdir="/etc/nasty foo confdir/"
# 		$(use_enable debug cnddebug)
# 		$(use_enable threads multithreading)
# 	)
# 	autotools-utils_src_configure
# }
# @CODE

# @VARIABLE: othereconfargs
# @DESCRIPTION:
# Contains additional configure arguments which are scheduled for
# inclusion in the econf argument-list but do not come from myeconfargs (specifically,
# those arguments passed to the command-line of autotools-utils_src_configure or
# generated by it automatically).
#
# Like myeconfargs, it can be modified by autotools-utils-pre_src_configure
# ehook listeners to change the configure process.

# @FUNCTION: autotools-utils_src_configure
# @DESCRIPTION:
# The src_configure function. For out of source build it creates build
# directory and runs econf there. Configuration parameters defined
# in myeconfargs are passed here to econf. Additionally following USE
# flags are known:
#
# IUSE="static-libs" passes --enable-shared and either --disable-static/--enable-static
# to econf respectively.
autotools-utils_src_configure() {
	debug-print-function ${FUNCNAME} "$@"

	[[ -z ${myeconfargs+1} || $(declare -p myeconfargs) == 'declare -a'* ]] \
		|| die 'autotools-utils.eclass: myeconfargs has to be an array.'

	[[ ${EAPI} == 2 ]] && ! use prefix && EPREFIX=

	local othereconfargs=("$@") myeconfargs=("${myeconfargs[@]}") testvar

	_check_build_dir
	if "${ECONF_SOURCE}"/configure --help 2>&1 | grep -q '^ *--docdir='; then
		testvar=no # has a similar arg already?
		for i in "${myeconfargs[@]}" "$@" ; do
			if [[ ${i} == "--docdir="* ]] ; then
				testvar=yes
			fi
		done
		[[ ${testvar} == no ]] && othereconfargs+=(
			--docdir="${EPREFIX}"/usr/share/doc/${PF}
		)
	fi

	# Handle static-libs found in IUSE, disable them by default
	if in_iuse static-libs && [[ -z "${AUTOTOOLS_NO_AUTO_STATIC_LIBS_CONFARGS}" ]] ; then
		has "--enable-shared" "${myeconfargs[@]}" "${othereconfargs[@]}" || {
			has "--disable-shared" "${myeconfargs[@]}" "${othereconfargs[@]}" || {
				othereconfargs+=("--enable-shared")
				debug-print "${FUNCNAME}: adding --enable-shared to othereconfargs (since in_iuse static-libs)"
			}
		}
		has "--enable-static" "${myeconfargs[@]}" "${othereconfargs[@]}" || {
			has "--disable-static" "${myeconfargs[@]}" "${othereconfargs[@]}" || {
				othereconfargs+=("$(use_enable static-libs static)")
				debug-print "${FUNCNAME}: adding $(use_enable static-libs static) to othereconfargs (since static-libs useflag $(
					if use static-libs ; then
						echo "is set"
					else
						echo "is not set"
					fi
				))"
			}
		}
	fi

	mkdir -p "${BUILD_DIR}" || die
	pushd "${BUILD_DIR}" > /dev/null || die

	local _autotools_utils_src_configure_result=0
	ehook_fire autotools-utils-pre_src_configure && {
		econf "${myeconfargs[@]}" "${othereconfargs[@]}"
		_autotools_utils_src_configure_result=$?
	}
	ehook_fire autotools-utils-post_src_configure -u

	popd > /dev/null || die
	return $_autotools_utils_src_configure_result
}

# @VARIABLE: myemakeargs
# @DEFAULT_UNSET
# @DESCRIPTION:
# Optional emake arguments as Bash array. Should be defined before calling src_compile
# If modified during autotools-utils-pre_src_compile ehook processing (but no ehook
# supresses emake entirely by returning a nonzero value), then those changes will
# be respected during emake invocation.
# @CODE
# src_compile() {
# 	local myemakeargs=(
#		-j1
#		FOO="${bar} ${baz}"
# 	)
# 	autotools-utils_src_compile
# }
# @CODE

# @VARIABLE: otheremakeargs
# @DESCRIPTION:
# Contains additional compile arguments which are scheduled for
# inclusion in the emake argument-list but do not come from myemakeargs (specifically,
# those arguments passed to the command-line of autotools-utils_src_compile or
# generated by it automatically).
#
# Like myemakeargs, it can be modified by autotools-utils-pre_src_compile
# ehook listeners to change the compile process.

# @FUNCTION: autotools-utils_src_compile
# @DESCRIPTION:
# The autotools src_compile function, invokes emake in specified BUILD_DIR.
autotools-utils_src_compile() {
	debug-print-function ${FUNCNAME} "$@"

	[[ -z ${myemakeargs+1} || $(declare -p myemakeargs) == 'declare -a'* ]] \
		|| die 'autotools-utils.eclass: myemakeargs has to be an array.'

	_check_build_dir
	local myemakeargs=("${myemakeargs[@]}")
	local otheremakeargs=("$@")
	pushd "${BUILD_DIR}" > /dev/null || die
	ehook_fire autotools-utils-pre_src_compile && {
		emake "${myemakeargs[@]}" "${otheremakeargs[@]}" || \
			die 'emake failed'
	}
	ehook_fire autotools-utils-post_src_compile -u
	popd > /dev/null || die
	return 0
}

# @VARIABLE: myemakeinstallargs
# @DEFAULT_UNSET
# @DESCRIPTION:
# Optional emake arguments as Bash array. Should be defined before calling src_install
# If modified during autotools-utils-pre_src_install ehook processing (but no ehook
# supresses emake entirely by returning a nonzero value), then those changes will
# be respected during emake invocation during autotools-utils_src_install.
# @CODE
# src_install() {
# 	local myemakeinstallargs=(
#		-j1
#		FOO="${bar} ${baz}"
# 	)
# 	autotools-utils_src_install
# }
# @CODE

# @VARIABLE: otheremakeinstallargs
# @DESCRIPTION:
# Contains additional emake arguments which are scheduled for
# inclusion in the emake argument-list but do not come from myemakeinstallargs (specifically,
# those arguments passed to the command-line of autotools-utils_src_install or
# generated by it automatically).
#
# Like myemakeinstallargs, it can be modified by autotools-utils-pre_src_install
# ehook listeners to change the install process.

# @FUNCTION: autotools-utils_src_install
# @DESCRIPTION:
# The autotools src_install function. Runs emake install, unconditionally
# removes unnecessary static libs (based on shouldnotlink libtool property)
# and removes unnecessary libtool files when static-libs USE flag is defined
# and unset.
#
# DOCS and HTML_DOCS arrays are supported. See base.eclass(5) for reference.
autotools-utils_src_install() {
	debug-print-function ${FUNCNAME} "$@"

	[[ -z ${myemakeinstallargs+1} || $(declare -p myemakeinstallargs) == 'declare -a'* ]] \
		|| die 'autotools-utils.eclass: myemakeinstallargs has to be an array.'

	_check_build_dir
	local myemakeinstallargs=("${myemakeinstallargs[@]}")
	local otheremakeinstallargs=("DESTDIR=${D}" "$@" "install")
	local _dostuff="no"
	pushd "${BUILD_DIR}" > /dev/null || die
	ehook_fire autotools-utils-pre_src_install && {
		emake "${myemakeinstallargs[@]}" "${otheremakeinstallargs[@]}" || \
			die "emake install failed"
		_dostuff="yes"
	}

	popd > /dev/null || die

	if [[ ${_dostuff} == yes ]] ; then
		# Move docs installed by autotools (in EAPI < 4).
		if [[ ${EAPI} == [23] ]] \
				&& path_exists "${D}${EPREFIX}"/usr/share/doc/${PF}/*; then
			if [[ $(find "${D}${EPREFIX}"/usr/share/doc/${PF}/* -type d) ]]; then
				eqawarn "autotools-utils: directories in docdir require at least EAPI 4"
			else
				mkdir "${T}"/temp-docdir
				mv "${D}${EPREFIX}"/usr/share/doc/${PF}/* "${T}"/temp-docdir/ \
					|| die "moving docs to tempdir failed"

				dodoc "${T}"/temp-docdir/* || die "docdir dodoc failed"
				rm -r "${T}"/temp-docdir || die
			fi
		fi
		# XXX: support installing them from builddir as well?
		if declare -p DOCS &>/dev/null; then
			# an empty list == don't install anything
			if [[ ${DOCS[@]} ]]; then
				if [[ ${EAPI} == [23] ]]; then
					dodoc "${DOCS[@]}" || die
				else
					# dies by itself
					dodoc -r "${DOCS[@]}"
				fi
			fi
		else
			local f
			# same list as in PMS
			for f in README* ChangeLog AUTHORS NEWS TODO CHANGES \
					THANKS BUGS FAQ CREDITS CHANGELOG; do
				if [[ -s ${f} ]]; then
					dodoc "${f}" || die "(default) dodoc ${f} failed"
				fi
			done
		fi
		if [[ ${HTML_DOCS} ]]; then
			dohtml -r "${HTML_DOCS[@]}" || die "dohtml failed"
		fi

		# Remove libtool files and unnecessary static libs
		local prune_ltfiles=${AUTOTOOLS_PRUNE_LIBTOOL_FILES}
		if [[ ${prune_ltfiles} != none ]]; then
			prune_libtool_files ${prune_ltfiles:+--${prune_ltfiles}}
		fi
	fi
	pushd "${BUILD_DIR}" > /dev/null || die
	ehook_fire autotools-utils-post_src_install -u
	popd > /dev/null || die
	return 0
}

# @VARIABLE: myemaketestargs
# @DEFAULT_UNSET
# @DESCRIPTION:
# Optional emake arguments as Bash array. Should be defined before calling src_test
# If modified during autotools-utils-pre_src_test ehook processing (but no ehook
# supresses emake entirely by returning a nonzero value), then those changes will
# be respected during emake invocation during autotools-utils_src_test.
# @CODE
# src_test() {
# 	local myemaketestargs=(
#		-j1
#		FOO="${bar} ${baz}"
# 	)
# 	autotools-utils_src_test
# }
# @CODE

# @VARIABLE: otheremaketestargs
# @DESCRIPTION:
# Contains additional emake arguments which are scheduled for
# inclusion in the emake argument-list but do not come from myemaketestargs (specifically,
# those arguments passed to the command-line of autotools-utils_src_test or
# generated by it automatically).
#
# Like myemaketestargs, it can be modified by autotools-utils-pre_src_test
# ehook listeners to change the test process.

# @FUNCTION: autotools-utils_src_test
# @DESCRIPTION:
# The autotools src_test function. Runs emake check in build directory.
autotools-utils_src_test() {
	debug-print-function ${FUNCNAME} "$@"

	[[ -z ${myemaketestargs+1} || $(declare -p myemaketestargs) == 'declare -a'* ]] \
		|| die 'autotools-utils.eclass: myemaketestargs has to be an array.'

	_check_build_dir
	local myemaketestargs=("${myemaketestargs[@]}") otheremaketestargs=("$@") _how="dont" _what=emake

	pushd "${BUILD_DIR}" > /dev/null || die
	if make -n check "${myemaketestargs[@]}" "${otheremaketestargs[@]}" &>/dev/null 2>&1 ; then
		_how="check"
	elif make -n test "${myemaketestargs[@]}" "${otheremaketestargs[@]}" &>/dev/null 2>&1 ; then
		_how="test"
	fi
	[[ ${AUTOTOOLS_TEST_MAKE} ]] && _what="${AUTOTOOLS_TEST_MAKE}"
	[[ ${_how} != dont ]] && otheremaketestargs+=("${_how}")
	if ehook_fire autotools-utils-pre_src_test ; then
		[[ ${_how} != dont ]] &&
			{ ${_what} "${myemaketestargs[@]}" "${otheremaketestargs[@]}" || die 'emake check failed.' ; }
	fi
	ehook_fire autotools-utils-post_src_test -u
	popd > /dev/null || die

	return 0
}
