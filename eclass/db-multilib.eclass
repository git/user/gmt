# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# This is a common location for functions used in the sys-libs/db ebuilds
#
# Bugs: maintainer-needed@gentoo.org

inherit eutils db multilib-build

EXPORT_FUNCTIONS src_test

DEPEND="test? ( >=dev-lang/tcl-8.4[${MULTILIB_USEDEP}] )"

RDEPEND=""

# @ECLASS-VARIABLE: DB_MULTILIB_PARALLEL_TEST
# @DESCRIPTION:
# Set to a nonempty value to run the parallelize the ABI-testing
# sub-phases
: ${DB_MULTILIB_PARALLEL_TEST:=}

db-multilib_src_test() {
	if [[ $UID -eq 0 ]]; then
		M="You must run the testsuite as non-root, skipping"
		ewarn "${M}"
		elog "${M}"
		return 0
	fi

	if use tcl; then
		einfo "Running sys-libs/db testsuite"
		ewarn "This can take 6+ hours on modern machines"
		# Fix stuff that fails with relative paths, and upstream moving files
		# around...
		local test_parallel=''
		for t in \
			"${S}"/test/parallel.tcl \
			"${S}"/../test/parallel.tcl \
			"${S}"/test/tcl/parallel.tcl \
			"${S}"/../test/tcl/parallel.tcl \
			; do
			[[ -f "${t}" ]] && test_parallel="${t}" && break
		done

		sed -ri \
			-e '/regsub .test_path ./s,(regsub),#\1,g' \
			-e '/regsub .src_root ./s,(regsub),#\1,g' \
			-e '/regsub .tcl_utils ./s,(regsub),#\1,g' \
			"${test_parallel}"

		if [[ -n ${DB_MULTILIB_PARALLEL_TEST} ]] ; then
			multilib_parallel_foreach_abi db_src_test_perabi
		else
			multilib_foreach_abi db_src_test_perabi
		fi

	else
		eerror "You must have USE=tcl to run the sys-libs/db testsuite."
	fi
}

db_src_test_perabi() {
	cd "${S}"
	for t in \
		../test/test.tcl \
		../test/tcl/test.tcl \
		; do
		[[ -f "${t}" ]] && testbase="${t}" && break
	done
	echo "source ${t}" > ${BUILD_DIR}/testrunner.tcl

	cd "${BUILD_DIR}"
	echo "run_parallel $(makeopts_jobs) run_std" >> testrunner.tcl

	tclsh testrunner.tcl
	egrep -qs '^FAIL' ALL.OUT* && die "Some tests failed, please see ${S}/ALL.OUT*"
}
