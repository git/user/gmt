# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
inherit eutils autotools flag-o-matic elisp-common multilib-minimal

DESCRIPTION="Scheme interpreter"
HOMEPAGE="http://www.gnu.org/software/guile/"
SRC_URI="mirror://gnu/guile/${P}.tar.gz"

LICENSE="LGPL-2.1"
KEYWORDS="~amd64"
IUSE="networking +regex discouraged +deprecated emacs nls debug-freelist debug-malloc debug +threads"
RESTRICT="!regex? ( test )"

DEPEND="
	>=dev-libs/gmp-4.1[${MULTILIB_USEDEP}]
	>=sys-devel/libtool-1.5.6[${MULTILIB_USEDEP}]
	sys-devel/gettext[${MULTILIB_USEDEP}]
	emacs? ( virtual/emacs )"
RDEPEND="${DEPEND}"

# Guile seems to contain some slotting support, /usr/share/guile/ is slotted,
# but there are lots of collisions. Most in /usr/share/libguile. Therefore
# I'm slotting this in the same slot as guile-1.6* for now.
SLOT="12"
MAJOR="1.8"

MULTILIB_WRAPPED_HEADERS=(/usr/include/libguile/scmconfig.h)
MULTILIB_WRAPPED_EXECUTABLES=(@/usr/bin/guile-config /usr/bin/guile)

pkg_setup() {
	declare -a abis=( $(multilib_get_enabled_abis) )
	if [[ ${#abis[@]} -gt 1 ]] ; then
		multiple_abis=yes
	fi
}

src_prepare() {
	epatch "${FILESDIR}/${P}-fix_guile-config.patch" \
		"${FILESDIR}/${P}-gcc46.patch" \
		"${FILESDIR}/${P}-makeinfo-5.patch" \
		"${FILESDIR}/${P}-dont-delete-my-binaries-please.patch" \
		"${FILESDIR}/${P}-at-least-_try_-to-test.patch" # <- still wont pass but... gx86 bug
	sed \
		-e "s/AM_CONFIG_HEADER/AC_CONFIG_HEADERS/g" \
		-e "/AM_PROG_CC_STDC/d" \
		-i guile-readline/configure.in
	eautoreconf
}

multilib_src_configure() {
	# see bug #178499
	filter-flags -ftree-vectorize

	#will fail for me if posix is disabled or without modules -- hkBst
	ECONF_SOURCE="${S}" econf \
		--disable-error-on-warning \
		--disable-static \
		--enable-posix \
		$(use_enable networking) \
		$(use_enable regex) \
		$(use deprecated || use_enable discouraged) \
		$(use_enable deprecated) \
		$(use_enable emacs elisp) \
		$(use_enable nls) \
		--disable-rpath \
		$(use_enable debug-freelist) \
		$(use_enable debug-malloc) \
		$(use_enable debug guile-debug) \
		$(use_with threads) \
		--with-modules \
		EMACS=no

	# prevent doc building for non-best abis
	if ! multilib_is_best_abi; then
		sed -e '/^SUBDIRS/,/^[^[:space:]]/ s/\([[:space:]]\)\(doc\|examples\)\([[:space:]]\|$\)/\1/g' \
			-i Makefile || die
		sed -e 's|^\(schemelib_DATA[[:space:]]*=\).*$|\1|' \
			-e '/^[^[:space:]]*\.\(texi\|txt\|doc\):\([[:space:]]\|$\)/,/^\([^[:space:]]\|$\)/ s/^/# (doc) &/' \
			-i libguile/Makefile || die
	fi
}

multilib_src_compile()  {
	default

	# Above we have disabled the build system's Emacs support;
	# for USE=emacs we compile (and install) the files manually
	if use emacs; then
		cd emacs
		elisp-compile *.el || die
	fi
}

src_test() {
	failed_abis=()
	multilib-minimal_src_test
	if (( ${#failed_abis[@]} )); then
		eerror
		eerror "NOTE: The following ABIs failed testing: ${failed_abis[*]}"
		eerror "Since these are known not to work, proceeding anyhow."
		eerror
	fi
}

multilib_src_test() {
	nonfatal emake check || failed_abis+=("${ABI}")
}

multilib_src_install() {
	default
	if [[ ${multiple_abis} ]] ; then
		# fix shebang in guile-config to run wrapped guile
		sed -e "1s|bin/guile|bin/guile-${MULTILIB_BUILD_ABI}|" \
			-i "${ED}"/usr/bin/guile-config || die
	fi
}

multilib_src_install_all() {
	dodoc AUTHORS ChangeLog GUILE-VERSION HACKING NEWS README THANKS || die

	# texmacs needs this, closing bug #23493
	dodir /etc/env.d
	echo "GUILE_LOAD_PATH=\"${EPREFIX}/usr/share/guile/${MAJOR}\"" > "${ED}"/etc/env.d/50guile

	# necessary for registering slib, see bug 206896
	keepdir /usr/share/guile/site

	if use emacs; then
		elisp-install ${PN} emacs/*.{el,elc} || die
		elisp-site-file-install "${FILESDIR}/50${PN}-gentoo.el" || die
	fi
}

pkg_postinst() {
	[ "${EROOT}" == "/" ] && pkg_config
	use emacs && elisp-site-regen
}

pkg_postrm() {
	use emacs && elisp-site-regen
}

pkg_config() {
	if has_version dev-scheme/slib; then
		einfo "Registering slib with guile"
		install_slib_for_guile
	fi
}

_pkg_prerm() {
	rm -f "${EROOT}"/usr/share/guile/site/slibcat
}
