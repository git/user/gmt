# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4
inherit flag-o-matic eutils toolchain-funcs multilib multilib-minimal

DESCRIPTION="Source-code libraries for standards-based RTP/RTCP/RTSP multimedia streaming, suitable for embedded and/or low-cost streaming applications"
HOMEPAGE="http://www.live555.com/"
SRC_URI="http://www.live555.com/liveMedia/public/${P/-/.}.tar.gz
	mirror://gentoo/${P/-/.}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64"
IUSE="examples static-libs"

S="${WORKDIR}"/${PN}

# Alexis Ballier <aballier@gentoo.org>
# Be careful, bump this everytime you bump the package and the ABI has changed.
# If you don't know, ask someone.
LIVE_ABI_VERSION=6

src_prepare() {
	epatch "${FILESDIR}/${PN}-recursive2.patch"

	multilib_copy_sources
	multilib_foreach_abi multilib-build_run_in_build_dir abi_prepare
}

abi_prepare() {
	if use static-libs ; then
		cp -pPR "${BUILD_DIR}" "${BUILD_DIR}"-shared
		mv "${BUILD_DIR}" "${BUILD_DIR}-static"
	else
		mv "${BUILD_DIR}" "${BUILD_DIR}-shared"
	fi

	use static-libs && cp "${FILESDIR}/config.gentoo" "${BUILD_DIR}-static"
	cp "${FILESDIR}/config.gentoo-so-r1" "${BUILD_DIR}-shared"

	case ${CHOST} in
		*-solaris*)
			use static-libs && { sed -i \
				-e '/^COMPILE_OPTS /s/$/ -DSOLARIS -DXLOCALE_NOT_USED/' \
				-e '/^LIBS_FOR_CONSOLE_APPLICATION /s/$/ -lsocket -lnsl/' \
				"${BUILD_DIR}-static"/config.gentoo \
				|| die ; }
			sed -i \
				-e '/^COMPILE_OPTS /s/$/ -DSOLARIS -DXLOCALE_NOT_USED/' \
				-e '/^LIBS_FOR_CONSOLE_APPLICATION /s/$/ -lsocket -lnsl/' \
				"${BUILD_DIR}-shared"/config.gentoo-so-r1 \
				|| die
		;;
		*-darwin*)
			use static-libs && { sed -i \
				-e '/^COMPILE_OPTS /s/$/ -DBSD=1 -DHAVE_SOCKADDR_LEN=1/' \
				-e '/^LINK /s/$/ /' \
				-e '/^LIBRARY_LINK /s/$/ /' \
				-e '/^LIBRARY_LINK_OPTS /s/-Bstatic//' \
				"${BUILD_DIR}-static"/config.gentoo \
				|| die static ; }
			sed -i \
				-e '/^COMPILE_OPTS /s/$/ -DBSD=1 -DHAVE_SOCKADDR_LEN=1/' \
				-e '/^LINK /s/$/ /' \
				-e '/^LIBRARY_LINK /s/=.*$/= $(CXX) -o /' \
				-e '/^LIBRARY_LINK_OPTS /s:-shared.*$:-undefined suppress -flat_namespace -dynamiclib -install_name '"${EPREFIX}/usr/$(get_libdir)/"'$@:' \
				-e '/^LIB_SUFFIX /s/so/dylib/' \
				"${BUILD_DIR}-shared"/config.gentoo-so-r1 \
				|| die shared
		;;
	esac
	[[ -d "${BUILD_DIR}" ]] || mkdir "${BUILD_DIR}"
}

src_configure() { :; }

multilib_src_compile() {
	local CC=${CC:-$(tc-getCC)} CXX=${CXX:-$(tc-getCXX)} LD=${LD:-$(tc-getLD)}
	export CC CXX LD

	# Still build the old synchronous interface as mplayer still needs it.
	# Please drop me at some point!
	append-flags '-DRTSPCLIENT_SYNCHRONOUS_INTERFACE'

	if use static-libs ; then
		cd "${BUILD_DIR}-static" || die

		einfo "Beginning static library build"
		./genMakefiles gentoo
		emake -j1 LINK_OPTS="-L. $(raw-ldflags)" || die "failed to build static libraries"
	fi

	cd "${BUILD_DIR}-shared" || die
	einfo "Beginning shared library build"
	./genMakefiles gentoo-so-r1
	local suffix=$(get_libname ${LIVE_ABI_VERSION})
	emake -j1 LINK_OPTS="-L. ${LDFLAGS}" LIB_SUFFIX="${suffix#.}" || die "failed to build shared libraries"

	for i in liveMedia groupsock UsageEnvironment BasicUsageEnvironment ; do
		pushd "${BUILD_DIR}-shared/${i}" > /dev/null || die
		ln -s lib${i}.${suffix#.} lib${i}$(get_libname) || die
		popd > /dev/null
	done

	if multilib_build_binaries; then
		einfo "Beginning programs build"
		for i in $(use examples && echo "testProgs") proxyServer mediaServer ; do
			cd "${BUILD_DIR}-shared/${i}" || die
			emake LINK_OPTS="-L. ${LDFLAGS}" || die "failed to build test programs"
		done
	fi
}

multilib_src_install() {
	for library in UsageEnvironment liveMedia BasicUsageEnvironment groupsock; do
		use static-libs && dolib.a "${BUILD_DIR}-static"/${library}/lib${library}.a
		dolib.so "${BUILD_DIR}-shared"/${library}/lib${library}$(get_libname ${LIVE_ABI_VERSION})
		dosym lib${library}$(get_libname ${LIVE_ABI_VERSION}) /usr/$(get_libdir)/lib${library}$(get_libname)

		insinto /usr/include/${library}
		doins "${BUILD_DIR}-shared"/${library}/include/*h
	done

	if multilib_is_best_abi; then
	# Should we really install these?
		use examples && find "${BUILD_DIR}-shared"/testProgs -type f -perm /111 -print0 | \
			xargs -0 dobin

		dobin "${BUILD_DIR}-shared"/mediaServer/live555MediaServer
		dobin "${BUILD_DIR}-shared"/proxyServer/live555ProxyServer

		# install docs
		dodoc "${BUILD_DIR}-shared"/README
	fi
}
