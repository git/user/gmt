# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"

inherit eutils multilib-minimal

DESCRIPTION="Program and text file generation"
HOMEPAGE="http://www.gnu.org/software/autogen/"
SRC_URI="mirror://gnu/${PN}/rel${PV}/${P}.tar.xz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE="libopts static-libs"

RDEPEND=">=dev-scheme/guile-1.8
	dev-libs/libxml2"
DEPEND="${RDEPEND}"

multilib_src_configure() {
	# suppress possibly incorrect -R flag
	export ag_cv_test_ldflags=

	ECONF_SOURCE="${S}" econf $(use_enable static-libs static)
}

multilib_src_install() {
	default
	# hmm... that doesn't look right, does it?
	mv -t "${ED}"usr/$(get_libdir) "${ED}"usr/share/pkgconfig/
}

multilib_src_install_all() {
	einstalldocs
	prune_libtool_files

	if ! use libopts ; then
		rm "${ED}"/usr/share/autogen/libopts-*.tar.gz || die
	fi
}
