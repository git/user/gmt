# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

SUPPORT_PYTHON_ABIS="1"
RESTRICT_PYTHON_ABIS="*-jython *-pypy-*"

inherit autotools flag-o-matic python eutils-multilib multilib-minimal

DESCRIPTION="POSIX 1003.1e capabilities"
HOMEPAGE="http://people.redhat.com/sgrubb/libcap-ng/"
SRC_URI="http://people.redhat.com/sgrubb/${PN}/${P}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64"
IUSE="python static-libs"

RDEPEND="python? ( dev-lang/python )"
DEPEND="${RDEPEND}
	sys-kernel/linux-headers
	python? ( >=dev-lang/swig-2 )"

PYTHON_CFLAGS=("2.* + -fno-strict-aliasing")

pkg_setup() {
	use python && python_pkg_setup
}

src_prepare() {
	# Disable byte-compilation of Python modules.
	>py-compile

	# Python bindings are built/tested/installed manually.
	sed -i -e "/^SUBDIRS/s/ python//" bindings/Makefile.am || die
	sed -i -e 's:AM_CONFIG_HEADER:AC_CONFIG_HEADERS:' configure.ac || die

	eautoreconf

	use sparc && replace-flags -O? -O0
}

multilib_src_configure() {
	local CFLAGS="${CFLAGS}"
	export CFLAGS
	ECONF_SOURCE="${S}" econf \
		$(use_enable static-libs static) \
		$(use_with_best_abi python)
}

multilib_src_compile() {
	default

	if multilib_is_best_abi && use python; then
		S="${BUILD_DIR}" python_copy_sources bindings/python
		building() {
			emake \
				CFLAGS="${CFLAGS} $(python-config --cflags)" \
				PYTHON_VERSION="$(python_get_version)" \
				pyexecdir="$(python_get_sitedir)" \
				pythondir="$(python_get_sitedir)"
		}
		S="${BUILD_DIR}" python_execute_function -s --source-dir bindings/python building
	fi
}

multilib_src_test() {
	if [[ "${EUID}" -eq 0 ]]; then
		ewarn "Skipping tests due to root permissions."
		return
	fi

	default

	if multilib_is_best_abi use python; then
		testing() {
			emake \
				CFLAGS="${CFLAGS} $(python-config --cflags)" \
				PYTHON_VERSION="$(python_get_version)" \
				pyexecdir="$(python_get_sitedir)" \
				pythondir="$(python_get_sitedir)" \
				TESTS_ENVIRONMENT="PYTHONPATH=..:../.libs" \
				check
		}
		S="${BUILD_DIR}" python_execute_function -s --source-dir bindings/python testing
	fi
}

multilib_src_install() {
	default

	if multilib_is_best_abi && use python; then
		installation() {
			emake \
				DESTDIR="${D}" \
				PYTHON_VERSION="$(python_get_version)" \
				pyexecdir="$(python_get_sitedir)" \
				pythondir="$(python_get_sitedir)" \
				install
		}
		S="${BUILD_DIR}" python_execute_function -s --source-dir bindings/python installation

		python_clean_installation_image
	fi

	rm -f "${ED}"/usr/lib*/${PN}.la
}

pkg_postinst() {
	use python && python_mod_optimize capng.py
}

pkg_postrm() {
	use python && python_mod_cleanup capng.py
}
