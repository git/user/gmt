# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"
GCONF_DEBUG="no"
GNOME2_LA_PUNT="yes"

inherit gnome2-multilib virtualx

DESCRIPTION="Network-related giomodules for glib"
HOMEPAGE="http://git.gnome.org/browse/glib-networking/"

LICENSE="LGPL-2+"
SLOT="0"
IUSE="+gnome +libproxy smartcard +ssl test"
KEYWORDS="~amd64"

RDEPEND="
	>=dev-libs/glib-2.35.8:2[${MULTILIB_USEDEP}]
	gnome? ( gnome-base/gsettings-desktop-schemas[${MULTILIB_USEDEP}] )
	libproxy? ( >=net-libs/libproxy-0.4.6-r3:=[${MULTILIB_USEDEP}] )
	smartcard? (
		>=app-crypt/p11-kit-0.8[${MULTILIB_USEDEP}]
		>=net-libs/gnutls-2.12.8:=[pkcs11,${MULTILIB_USEDEP}] )
	ssl? (
		app-misc/ca-certificates
		>=net-libs/gnutls-2.11.0:=[${MULTILIB_USEDEP}] )
"
DEPEND="${RDEPEND}
	>=dev-util/intltool-0.35.0
	sys-devel/gettext[${MULTILIB_USEDEP}]
	virtual/pkgconfig
	test? ( sys-apps/dbus[X] )
"
# eautoreconf needs >=sys-devel/autoconf-2.65:2.5

src_prepare() {
	# Failing tests, upstream #695062 (will be fixed in 2.38)
	sed -e '/tls.connection.simultaneous-async-rehandshake/,+1 d' \
		-e '/tls.connection.simultaneous-sync-rehandshake/,+1 d' \
		-i tls/tests/connection.c || die
	gnome2-multilib_src_prepare
}

src_configure() {
	# AUTHORS, ChangeLog are empty
	DOCS="NEWS README"
	gnome2-multilib_src_configure \
		--disable-static \
		--with-ca-certificates="${EPREFIX}"/etc/ssl/certs/ca-certificates.crt \
		$(use_with gnome gnome-proxy) \
		$(use_with libproxy) \
		$(use_with smartcard pkcs11) \
		$(use_with ssl gnutls)
}

ehook gnome2-multilib-per-abi-pre_src_test my_abi_pre_test
my_abi_pre_test() {
	Xemake check
	return 1
}
