# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"
GCONF_DEBUG="yes"
GNOME2_LA_PUNT="yes"

MY_PN=${PN/-gnome}
MY_P=${MY_PN}-${PV}

inherit gnome2-multilib autotools eutils

DESCRIPTION="GNOME plugin for libsoup"
HOMEPAGE="https://wiki.gnome.org/LibSoup"
SRC_URI="${SRC_URI//-gnome}"

LICENSE="LGPL-2+"
SLOT="2.4"
IUSE="debug +introspection"
KEYWORDS="~amd64"

RDEPEND="
	~net-libs/libsoup-${PV}[introspection?,${MULTILIB_USEDEP}]
	>=dev-db/sqlite-3.8.3:3=[${MULTILIB_USEDEP}]
	introspection? ( >=dev-libs/gobject-introspection-0.9.5[${MULTILIB_USEDEP}] )
	>=net-libs/libsoup-2.42.2-r1[${MULTILIB_USEDEP}]
"
DEPEND="${RDEPEND}
	>=dev-util/gtk-doc-am-1.10
	>=dev-util/intltool-0.35
	>=sys-devel/gettext-0.18.3.2[${MULTILIB_USEDEP}]
	virtual/pkgconfig
"

S=${WORKDIR}/${MY_P}

src_prepare() {
	# Use lib present on the system
	epatch "${FILESDIR}"/${PN}-2.46.0-system-lib.patch
	AUTOTOOLS_AUTORECONF=yes gnome2-multilib_src_prepare
}

src_configure() {
	# FIXME: we need addpredict to workaround bug #324779 until
	# root cause (bug #249496) is solved
	addpredict /usr/share/snmp/mibs/.index

	# Disable apache tests until they are usable on Gentoo, bug #326957
	gnome2-multilib_src_configure \
		--disable-static \
		--disable-tls-check \
		$(use_enable introspection) \
		--with-libsoup-system \
		--with-gnome \
		--without-apache-httpd
}
