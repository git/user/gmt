# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"

inherit eutils flag-o-matic autotools multilib-minimal

DESCRIPTION="An Portable Open Source UPnP Development Kit"
HOMEPAGE="http://pupnp.sourceforge.net/"
SRC_URI="mirror://sourceforge/pupnp/${P}.tar.bz2"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64"
IUSE="debug doc ipv6 static-libs"

DOCS="NEWS README ChangeLog"

src_prepare() {
	sed -e '/AX_CFLAGS_GCC_OPTION/s:-Os::g' \
		-i "${S}/configure.ac" || die

	# fix tests
	chmod +x ixml/test/test_document.sh || die

	eautoreconf
	multilib_copy_sources
}

multilib_src_configure() {
	abi_arch_use x86-fbsd && append-flags -O1
	# w/o docdir to avoid sandbox violations
	econf \
		$(use_enable debug) \
		$(use_enable ipv6) \
		$(use_enable static-libs static) \
		$(use_with doc documentation "${EPREFIX}/usr/share/doc/${PF}")
}

multilib_src_install() {
	default
	multilib_build_binaries && \
		dobin upnp/sample/.libs/tv_{combo,ctrlpt,device}
}

multilib_src_install_all() {
	einstalldocs
	use static-libs || find "${ED}" -type f -name '*.la' -delete
}

pkg_postinst() {
	ewarn "Please remember to run revdep-rebuild when upgrading"
	ewarn "from libupnp 1.4.x to libupnp 1.6.x , so packages"
	ewarn "gets linked with the new library."
	ewarn ""
	ewarn "The revdep-rebuild script is part of the"
	ewarn "app-portage/gentoolkit package."
}
