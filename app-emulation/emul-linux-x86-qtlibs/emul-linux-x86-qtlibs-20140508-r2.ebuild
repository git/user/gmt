# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
inherit eutils emul-linux-x86

LICENSE="!abi_x86_32? ( LGPL-2.1 GPL-3 ) abi_x86_32? ( metapackage )"
KEYWORDS="-* ~amd64"

IUSE="mng abi_x86_32"

DEPEND=""
RDEPEND="
	|| (
		~app-emulation/emul-linux-x86-baselibs-${PV}
		(
			dev-db/sqlite:3[abi_x86_32(-)]
			dev-libs/glib[abi_x86_32(-)]
			dev-libs/openssl[abi_x86_32(-)]
			mng? ( <media-libs/libmng-2[abi_x86_32(-)] )
			media-libs/libpng:0/16[abi_x86_32(-)]
			media-libs/tiff[abi_x86_32(-)]
			sys-apps/dbus[abi_x86_32(-)]
			sys-libs/zlib[abi_x86_32(-)]
			virtual/jpeg:62[abi_x86_32(-)]
		)
	)
	|| (
		~app-emulation/emul-linux-x86-medialibs-${PV}
		(
			media-libs/gstreamer:0.10[abi_x86_32(-)]
			media-libs/gst-plugins-base:0.10[abi_x86_32(-)]
		)
	)
	|| (
		~app-emulation/emul-linux-x86-opengl-${PV}
		virtual/opengl[abi_x86_32(-)]
	)
	|| (
		~app-emulation/emul-linux-x86-xlibs-${PV}
		(
			media-libs/fontconfig[abi_x86_32(-)]
			media-libs/freetype[abi_x86_32(-)]
			x11-libs/libICE[abi_x86_32(-)]
			x11-libs/libSM[abi_x86_32(-)]
			x11-libs/libX11[abi_x86_32(-)]
			x11-libs/libXcursor[abi_x86_32(-)]
			x11-libs/libXext[abi_x86_32(-)]
			x11-libs/libXfixes[abi_x86_32(-)]
			x11-libs/libXinerama[abi_x86_32(-)]
			x11-libs/libXi[abi_x86_32(-)]
			x11-libs/libXrandr[abi_x86_32(-)]
			x11-libs/libXrender[abi_x86_32(-)]
		)
	)
	abi_x86_32? (
		>=dev-qt/designer-4.8.5-r1[abi_x86_32(-)]
		>=dev-qt/qtcore-4.8.5-r1[abi_x86_32(-)]
		>=dev-qt/qtdbus-4.8.5-r1[abi_x86_32(-)]
		>=dev-qt/qtgui-4.8.5-r3[abi_x86_32(-)]
		>=dev-qt/qtopengl-4.8.5-r1[abi_x86_32(-)]
		>=dev-qt/qtscript-4.8.5-r1[abi_x86_32(-)]
		>=dev-qt/qtsql-4.8.5-r1[abi_x86_32(-)]
		>=dev-qt/qtsvg-4.8.5-r1[abi_x86_32(-)]
		>=dev-qt/qtwebkit-4.8.5-r1[abi_x86_32(-)]
		>=dev-qt/qtxmlpatterns-4.8.5-r1[abi_x86_32(-)]
		>=media-libs/phonon-4.7.1-r1[abi_x86_32(-)]
	)"

SRC_URI="!abi_x86_32? ( ${SRC_URI} )"

src_prepare() {
	use abi_x86_32 && return
	emul-linux-x86_src_prepare
}

src_install() {
	use abi_x86_32 && return
	emul-linux-x86_src_install

	if ! use mng; then
		rm "${D%/}"/usr/"$(get_abi_LIBDIR x86)"/qt4/plugins/imageformats/libqmng.so || die
	fi

	# Set LDPATH for not needing dev-qt/qtcore
	cat <<-EOF > "${T}/44qt4-emul"
	LDPATH=/usr/lib32/qt4
	EOF
	doenvd "${T}/44qt4-emul"
}
