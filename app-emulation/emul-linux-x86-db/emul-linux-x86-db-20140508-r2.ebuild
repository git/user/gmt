# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
inherit emul-linux-x86

LICENSE="GPL-2 LGPL-2.1"
KEYWORDS="-* ~amd64"
IUSE="abi_x86_32"

DEPEND=""
RDEPEND="~app-emulation/emul-linux-x86-baselibs-${PV}
	abi_x86_32? (
		virtual/mysql[abi_x86_32(-)]
		>=dev-db/unixODBC-2.3.2[abi_x86_32(-)]
	)
"

src_unpack() {
	use abi_x86_32 && return 0
	emul-linux-x86_src_unpack
}

src_compile() {
	use abi_x86_32 && return 0
	emul-linux-x86_src_compile
}

src_install() {
	use abi_x86_32 && return 0
	emul-linux-x86_src_install
}
