# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"
GCONF_DEBUG="no"

inherit gnome2-multilib

DESCRIPTION="C++ interface for glib2"
HOMEPAGE="http://www.gtkmm.org"

LICENSE="LGPL-2.1+ GPL-2+" # GPL-2+ applies only to the build system
SLOT="2"
KEYWORDS="~amd64"
IUSE="doc debug examples test"

RDEPEND="
	>=dev-libs/libsigc++-2.2.10:2[${MULTILIB_USEDEP}]
	>=dev-libs/glib-2.40.0:2[${MULTILIB_USEDEP}]
"
DEPEND="${RDEPEND}
	virtual/pkgconfig
	doc? ( app-doc/doxygen )
"
# dev-cpp/mm-common needed for eautoreconf

MULTILIB_PARALLEL_PHASES="src_configure src_compile src_test"

src_prepare() {
	if ! use test; then
		# don't waste time building tests
		sed 's/^\(SUBDIRS =.*\)tests\(.*\)$/\1\2/' \
			-i Makefile.am Makefile.in || die "sed 1 failed"
	fi

	if ! use examples; then
		# don't waste time building examples
		sed 's/^\(SUBDIRS =.*\)examples\(.*\)$/\1\2/' \
			-i Makefile.am Makefile.in || die "sed 2 failed"
	fi

	gnome2-multilib_src_prepare
}

src_configure() {
	gnome2-multilib_src_configure \
		$(use_enable debug debug-refcounting) \
		$(use_enable doc documentation) \
		--enable-deprecated-api
}

ehook gnome2-multilib-per-abi-post_src_configure my_abi_post_configure
my_abi_post_configure() {
	# configure forgets to create these OOT
	local d
	if use examples ; then
		for d in "${S}"/examples/* ; do
			if [[ -d "${d}" ]]; then
				mkdir "${d#${S}/}" || die "couldnt mkdir \"${d#${S}/}\" but [[ -d ${d} ]]."
			fi
		done
	fi
}

src_test() {
	multilib_foreach_abi multilib-build_run_in_build_dir testicles
}

testicles() {
	cd tests
	default
	for i in */test; do
		${i} || die "Running tests failed at ${i} for ABI ${ABI}"
	done
}

src_install() {
	gnome2-multilib_src_install

	if ! use doc && ! use examples; then
		rm -fr "${ED}usr/share/doc/glibmm*"
	fi

	if use examples; then
		find examples -type d -name '.deps' -exec rm -rf {} \; 2>/dev/null
		dodoc -r examples
	fi
}
