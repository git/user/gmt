# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
GCONF_DEBUG="no"
#GNOME2_LA_PUNT="yes"

inherit gnome2-multilib

DESCRIPTION="C++ interface for pango"
HOMEPAGE="http://www.gtkmm.org"

LICENSE="LGPL-2.1"
SLOT="1.4"
KEYWORDS="~amd64"
IUSE="doc"

COMMON_DEPEND="
	>=x11-libs/pango-1.23.0[${MULTILIB_USEDEP}]
	>=dev-cpp/glibmm-2.36.0:2[${MULTILIB_USEDEP}]
	>=dev-cpp/cairomm-1.2.2[${MULTILIB_USEDEP}]
	dev-libs/libsigc++:2[${MULTILIB_USEDEP}]
"
DEPEND="${COMMON_DEPEND}
	virtual/pkgconfig
	doc? (
		media-gfx/graphviz
		dev-libs/libxslt
		app-doc/doxygen )
"
RDEPEND="${COMMON_DEPEND}
	!<dev-cpp/gtkmm-2.13:2.4
"

src_configure() {
	gnome2-multilib_src_configure $(use_enable doc documentation)
}
