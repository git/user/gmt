# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"
GCONF_DEBUG="no"
GNOME2_LA_PUNT="yes"
GNOME_ORG_MODULE="${PN/pp/++}"

inherit gnome2-multilib

DESCRIPTION="C++ wrapper for the libxml2 XML parser library"
HOMEPAGE="http://libxmlplusplus.sourceforge.net/"

LICENSE="LGPL-2.1"
SLOT="2.6"
KEYWORDS="~amd64"
IUSE="doc test"

RDEPEND=">=dev-libs/libxml2-2.7.3[${MULTILIB_USEDEP}]
	>=dev-cpp/glibmm-2.32[${MULTILIB_USEDEP}]"
DEPEND="${RDEPEND}
	virtual/pkgconfig"

MULTILIB_PARALLEL_PHASES=(src_configure src_compile)

src_configure() {
	G2CONF=("$(use_enable doc documentation)")
	gnome2-multilib_src_configure
}

src_install() {
	gnome2-multilib_src_install

	rm -fr "${ED}"usr/share/doc/libxml++*
}

ehook gnome2-multilib-global-post_src_install my_post_install
my_post_install() {
	DOCS=(AUTHORS ChangeLog NEWS README*)
	einstalldocs
	use doc && dohtml docs/reference/html/*
}
