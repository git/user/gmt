# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
GCONF_DEBUG="no"
#GNOME2_LA_PUNT="yes"

inherit gnome2-multilib

DESCRIPTION="C++ interface for the ATK library"
HOMEPAGE="http://www.gtkmm.org"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64"
IUSE="doc"

RDEPEND="
	>=dev-cpp/glibmm-2.36.0:2[doc?,${MULTILIB_USEDEP}]
	>=dev-libs/atk-1.12[${MULTILIB_USEDEP}]
	dev-libs/libsigc++:2[${MULTILIB_USEDEP}]
	!<dev-cpp/gtkmm-2.22.0
"
DEPEND="${RDEPEND}
	virtual/pkgconfig
"

src_configure() {
	gnome2-multilib_src_configure $(use_enable doc documentation)
}
