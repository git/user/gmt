# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
GCONF_DEBUG="no"

inherit eutils gnome2-multilib

DESCRIPTION="C++ bindings for the Cairo vector graphics library"
HOMEPAGE="http://cairographics.org/cairomm"
SRC_URI="http://cairographics.org/releases/${P}.tar.gz"

LICENSE="LGPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE="doc +svg"

# FIXME: svg support is automagic
RDEPEND="
	>=x11-libs/cairo-1.10[svg?,${MULTILIB_USEDEP}]
	dev-libs/libsigc++:2[${MULTILIB_USEDEP}]
"
DEPEND="${RDEPEND}
	virtual/pkgconfig
	doc? (
		app-doc/doxygen
		dev-libs/libxslt[${MULTILIB_USEDEP}]
		media-gfx/graphviz )
"

src_prepare() {
	# don't waste time building examples because they are marked as "noinst"
	sed -i 's/^\(SUBDIRS =.*\)examples\(.*\)$/\1\2/' Makefile.in || die

	# don't waste time building tests
	# they require the boost Unit Testing framework, that's not in base boost
	sed -i 's/^\(SUBDIRS =.*\)tests\(.*\)$/\1\2/' Makefile.in || die

	# Fix docs installation, bug #443950
	sed -i 's:libdocdir = \$(datarootdir)/doc/\$(book_name):libdocdir = \$(docdir):' docs/Makefile.in || die

	gnome2-multilib_src_prepare
}

src_configure() {
	gnome2-multilib_src_configure \
		--docdir="${EPREFIX}"/usr/share/doc/${PF} \
		--disable-tests \
		$(use_enable doc documentation)
}
