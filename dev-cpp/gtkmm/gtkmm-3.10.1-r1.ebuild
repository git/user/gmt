# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"
GCONF_DEBUG="no"

inherit gnome2-multilib

DESCRIPTION="C++ interface for GTK+"
HOMEPAGE="http://www.gtkmm.org"

LICENSE="LGPL-2.1"
SLOT="3.0"
KEYWORDS="~amd64"
IUSE="aqua doc examples test wayland +X"
REQUIRED_USE="|| ( aqua wayland X )"

RDEPEND="
	>=dev-cpp/glibmm-2.38.0:2[${MULTILIB_USEDEP}]
	>=x11-libs/gtk+-3.10.0:3[aqua?,wayland?,X?,${MULTILIB_USEDEP}]
	>=x11-libs/gdk-pixbuf-2.22.1:2[${MULTILIB_USEDEP}]
	>=dev-cpp/atkmm-2.22.2[${MULTILIB_USEDEP}]
	>=dev-cpp/cairomm-1.9.2.2[${MULTILIB_USEDEP}]
	>=dev-cpp/pangomm-2.27.1:1.4[${MULTILIB_USEDEP}]
	dev-libs/libsigc++:2[${MULTILIB_USEDEP}]
"
DEPEND="${RDEPEND}
	virtual/pkgconfig
	doc? (
		media-gfx/graphviz
		dev-libs/libxslt
		app-doc/doxygen )
"
#	dev-cpp/mm-common"
# eautoreconf needs mm-common

src_prepare() {
	if ! use test; then
		# don't waste time building tests
		sed 's/^\(SUBDIRS =.*\)tests\(.*\)$/\1\2/' -i Makefile.am Makefile.in \
			|| die "sed 1 failed"
	fi

	if ! use examples; then
		# don't waste time building tests
		sed 's/^\(SUBDIRS =.*\)demos\(.*\)$/\1\2/' -i Makefile.am Makefile.in \
			|| die "sed 2 failed"
	fi

	gnome2-multilib_src_prepare
}

src_configure() {
	DOCS="AUTHORS ChangeLog PORTING NEWS README"
	gnome2-multilib_src_configure \
		--enable-api-atkmm \
		$(use_enable doc documentation) \
		$(use_enable aqua quartz-backend) \
		$(use_enable wayland wayland-backend) \
		$(use_enable X x11-backend)
}
