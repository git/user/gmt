# prevent mysql overlay dev-db/mysql-5.5.36 which is a live version
# and has a problem for the moment (needs sync w/ upstream & review)
=dev-db/mysql-5.5.36

# mask to prevent non-multilib versions spilling through
<dev-libs/json-glib-1.0.2-r1
<media-libs/cogl-1.18.0-r1
<x11-libs/gtk+-3.12.2-r1:3
<dev-libs/gobject-introspection-1.40.0-r1
<app-accessibility/at-spi2-atk-2.12.1-r1
