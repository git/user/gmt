# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"
WANT_AUTOCONF="2.1"
PYTHON_COMPAT=( python2_{6,7} )
PYTHON_REQ_USE="threads"
inherit autotools eutils toolchain-funcs multilib python-any-r1 versionator pax-utils autotools-multilib

MY_PN="mozjs"
MY_P="${MY_PN}-${PV/_/.}"
DESCRIPTION="Stand-alone JavaScript C library"
HOMEPAGE="http://www.mozilla.org/js/spidermonkey/"
SRC_URI="https://ftp.mozilla.org/pub/mozilla.org/js/${MY_P}.tar.bz2"

LICENSE="NPL-1.1"
SLOT="24"
KEYWORDS="~amd64"
IUSE="debug icu jit minimal static-libs +system-icu test"

RESTRICT="ia64? ( test )"

S="${WORKDIR}/${MY_P%.rc*}"
BUILD_DIR="${S}/js/src"

RDEPEND=">=dev-libs/nspr-4.9.4[${MULTILIB_USEDEP}]
	>=virtual/libffi-3.0.13-r1[${MULTILIB_USEDEP}]
	>=sys-libs/zlib-1.1.4[${MULTILIB_USEDEP}]
	system-icu? ( >=dev-libs/icu-1.51:=[${MULTILIB_USEDEP}] )"

DEPEND="${RDEPEND}
	${PYTHON_DEPS}
	app-arch/zip
	virtual/pkgconfig"

pkg_setup(){
	if [[ ${MERGE_TYPE} != "binary" ]]; then
		python-any-r1_pkg_setup
		export LC_ALL="C"
	fi

	declare -a myopts
}

MULTILIB_WRAPPED_HEADERS=(/usr/include/mozjs-24/js-config.h)
MULTILIB_WRAPPED_EXECUTABLES=(@/usr/bin/js24-config)
MULTILIB_CHOST_EXECUTABLES=(/usr/bin/js24-config)
MULTILIB_PARALLEL_PHASES="src_configure src_compile src_test"

multilib_src_prepare() {
	epatch "${FILESDIR}"/${PN}-${SLOT}-system-icu.patch
	epatch "${FILESDIR}"/${PN}-24.2.0-fix-file-permissions.patch
	epatch "${FILESDIR}"/${PN}-${SLOT}-upward-growing-stack.patch
	epatch_user

	if [[ ${CHOST} == *-freebsd* ]]; then
		# Don't try to be smart, this does not work in cross-compile anyway
		ln -sfn "${BUILD_DIR}/config/Linux_All.mk" "${S}/config/$(uname -s)$(uname -r).mk" || die
	fi

	cd "${BUILD_DIR}" || die
	eautoconf
}

multilib_src_configure() {
	local myopts=( "${myopts[@]}" )

	if multilib_is_native_abi ; then
		myopts+=("--with-system-nspr")
	else
		# --with-system-nspr configure magic does not check for multilib suffixed
		# alternatives and therefore full of fail here.  Thankfully we can
		# effect multilib system-nspr linkage manually with these alternate configure frobs
		myopts+=(
			--with-nspr-cflags="$(${CHOST}-nspr-config --cflags)"
			--with-nspr-libs="$(${CHOST}-nspr-config --libs)"
			--with-nspr-prefix="$(${CHOST}-nspr-config --prefix)"
			--with-nspr-exec-prefix="$(${CHOST}-nspr-config --exec-prefix)"
		)
    fi

	if use icu; then # make sure system-icu flag only affects icu-enabled build
		myopts+=( $(use_with system-icu) )
	else
		myopts+=( --without-system-icu )
	fi

	ECONF_SOURCE="${S}/js/src" \
	MULTILIB_TC_EXPORT_VARS="CC CXX AR RANLIB LD" multilib_tc_export \
	econf \
		"${myopts[@]}" \
		--enable-jemalloc \
		--enable-readline \
		--enable-threadsafe \
		--enable-system-ffi \
		--disable-optimize \
		$(use_enable icu intl-api) \
		$(use_enable debug) \
		$(use_enable jit yarr-jit) \
		$(use_enable jit ion) \
		$(use_enable static-libs static) \
		$(use_enable test tests)
}

multilib_src_compile() {
	if tc-is-cross-compiler; then
		make CFLAGS="" CXXFLAGS="" \
			CC=$(tc-getBUILD_CC) CXX=$(tc-getBUILD_CXX) \
			AR=$(tc-getBUILD_AR) RANLIB=$(tc-getBUILD_RANLIB) \
			MOZ_OPTIMIZE_FLAGS="" MOZ_DEBUG_FLAGS="" \
			HOST_OPTIMIZE_FLAGS="" MODULE_OPTIMIZE_FLAGS="" \
			MOZ_PGO_OPTIMIZE_FLAGS="" \
			jscpucfg host_jsoplengen host_jskwgen || die
		make CFLAGS="" CXXFLAGS="" \
			CC=$(tc-getBUILD_CC) CXX=$(tc-getBUILD_CXX) \
			AR=$(tc-getBUILD_AR) RANLIB=$(tc-getBUILD_RANLIB) \
			MOZ_OPTIMIZE_FLAGS="" MOZ_DEBUG_FLAGS="" HOST_OPTIMIZE_FLAGS="" \
			-C config nsinstall || die
		mv {,native-}jscpucfg || die
		mv {,native-}host_jskwgen || die
		mv {,native-}host_jsoplengen || die
		mv config/{,native-}nsinstall || die
		sed -e 's@./jscpucfg@./native-jscpucfg@' \
			-e 's@./host_jskwgen@./native-host_jskwgen@' \
			-e 's@./host_jsoplengen@./native-host_jsoplengen@' \
			-i Makefile || die
		sed -e 's@/nsinstall@/native-nsinstall@' -i config/config.mk || die
		rm -f config/host_nsinstall.o \
			config/host_pathsub.o \
			host_jskwgen.o \
			host_jsoplengen.o || die
	fi
	emake \
		MOZ_OPTIMIZE_FLAGS="" MOZ_DEBUG_FLAGS="" \
		HOST_OPTIMIZE_FLAGS="" MODULE_OPTIMIZE_FLAGS="" \
		MOZ_PGO_OPTIMIZE_FLAGS=""
}

multilib_src_test() {
	cd "jsapi-tests" || die
	emake check
}

multilib_src_install() {
	emake DESTDIR="${D}" install

	if ! use minimal; then
		if use jit; then
			pax-mark m "${ED}/usr/bin/js${SLOT}"
		fi
	else
		rm -f "${ED}/usr/bin/js${SLOT}"
	fi
}

multilib_src_install_all() {
	dodoc README
	if ! use static-libs; then
		# We can't actually disable building of static libraries
		# They're used by the tests and in a few other places
		find "${D}" -iname '*.a' -delete || die
	fi
}
