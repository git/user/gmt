# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"
WANT_AUTOMAKE="none"
WANT_LIBTOOL="none"

inherit autotools eutils flag-o-matic multilib pax-utils python-multilib-utils-r1 toolchain-funcs multiprocessing multilib-minimal

MY_P="Python-${PV}"
PATCHSET_VERSION="2.7.7-0"

DESCRIPTION="An interpreted, interactive, object-oriented programming language"
HOMEPAGE="http://www.python.org/"
SRC_URI="http://www.python.org/ftp/python/${PV}/${MY_P}.tar.xz
		 http://dev.gentoo.org/~floppym/python/python-gentoo-patches-${PATCHSET_VERSION}.tar.xz"

LICENSE="PSF-2"
SLOT="2.7"
KEYWORDS="~amd64"
IUSE="-berkdb build doc elibc_uclibc examples gdbm hardened ipv6 +ncurses +readline sqlite +ssl +threads tk +wide-unicode wininst +xml"

# Do not add a dependency on dev-lang/python to this ebuild.
# If you need to apply a patch which requires python for bootstrapping, please
# run the bootstrap code on your dev box and include the results in the
# patchset. See bug 447752.

RDEPEND="app-arch/bzip2[${MULTILIB_USEDEP}]
	>=sys-libs/zlib-1.1.3[${MULTILIB_USEDEP}]
	virtual/libffi[${MULTILIB_USEDEP}]
	virtual/libintl[${MULTILIB_USEDEP}]
	!build? (
		berkdb? ( || (
			sys-libs/db:5.3[${MULTILIB_USEDEP}]
			sys-libs/db:5.2[${MULTILIB_USEDEP}]
			sys-libs/db:5.1[${MULTILIB_USEDEP}]
			sys-libs/db:5.0[${MULTILIB_USEDEP}]
			sys-libs/db:4.8[${MULTILIB_USEDEP}]
			sys-libs/db:4.7[${MULTILIB_USEDEP}]
			sys-libs/db:4.6[${MULTILIB_USEDEP}]
			sys-libs/db:4.5[${MULTILIB_USEDEP}]
			sys-libs/db:4.4[${MULTILIB_USEDEP}]
			sys-libs/db:4.3[${MULTILIB_USEDEP}]
			sys-libs/db:4.2[${MULTILIB_USEDEP}]
		) )
		gdbm? ( sys-libs/gdbm[berkdb,${MULTILIB_USEDEP}] )
		ncurses? (
			>=sys-libs/ncurses-5.2[${MULTILIB_USEDEP}]
			readline? ( >=sys-libs/readline-4.1[${MULTILIB_USEDEP}] )
		)
		sqlite? ( >=dev-db/sqlite-3.3.8:3[${MULTILIB_USEDEP}] )
		ssl? ( dev-libs/openssl[${MULTILIB_USEDEP}] )
		tk? (
			>=dev-lang/tk-8.0[${MULTILIB_USEDEP}]
			dev-tcltk/blt[${MULTILIB_USEDEP}]
			dev-tcltk/tix[${MULTILIB_USEDEP}]
		)
		xml? ( >=dev-libs/expat-2.1[${MULTILIB_USEDEP}] )
	)
	!!<sys-apps/portage-2.1.9"
DEPEND="${RDEPEND}
	virtual/pkgconfig
	>=sys-devel/autoconf-2.65
	!sys-devel/gcc[libffi]
	abi_x86_32? (
		!<=app-emulation/emul-linux-x86-baselibs-20140508-r8
		!app-emulation/emul-linux-x86-baselibs[-abi_x86_32(-)]
 	)"
RDEPEND+=" !build? ( app-misc/mime-types )
	doc? ( dev-python/python-docs:${SLOT} )"
PDEPEND="app-admin/eselect-python
	app-admin/python-updater"

S="${WORKDIR}/${MY_P}"


MULTILIB_WRAPPED_HEADERS=(
	^/usr/include/python${SLOT}/pyconfig.h
)
MULTILIB_WRAPPED_EXECUTABLES=(
	/usr/bin/python${SLOT}
	/usr/bin/python${SLOT}-config
	/usr/bin/2to3-${SLOT}
	/usr/bin/pydoc${SLOT}
	/usr/bin/idle${SLOT}
)
MULTILIB_PARALLEL_PHASES=( src_configure src_test )

pkg_setup() {
	if use berkdb; then
		ewarn "'bsddb' module is out-of-date and no longer maintained inside"
		ewarn "dev-lang/python. 'bsddb' and 'dbhash' modules have been additionally"
		ewarn "removed in Python 3. A maintained alternative of 'bsddb3' module"
		ewarn "is provided by dev-python/bsddb3."
	else
		if has_version "=${CATEGORY}/${PN}-${PV%%.*}*[berkdb]"; then
			ewarn "You are migrating from =${CATEGORY}/${PN}-${PV%%.*}*[berkdb]"
			ewarn "to =${CATEGORY}/${PN}-${PV%%.*}*[-berkdb]."
			ewarn "You might need to migrate your databases."
		fi
	fi
}

src_prepare() {
	# Ensure that internal copies of expat, libffi and zlib are not used.
	rm -r Modules/expat || die
	rm -r Modules/_ctypes/libffi* || die
	rm -r Modules/zlib || die

	if tc-is-cross-compiler; then
		local EPATCH_EXCLUDE="*_regenerate_platform-specific_modules.patch"
	fi

	EPATCH_SUFFIX="patch" epatch "${WORKDIR}/patches"

	# Fix for cross-compiling.
	epatch "${FILESDIR}/python-2.7.5-nonfatal-compileall.patch"

	# multilib header includes
	local abis=( $(multilib_get_enabled_abis) )
	[[ ${#abis[@]} -gt 1 ]] && \
		epatch "${FILESDIR}/python-2.7.6-includeflags.patch"

	epatch_user

	multilib_copy_sources

	multilib_parallel_foreach_abi multilib-build_run_in_build_dir abi_src_prepare
}

abi_src_prepare() {
	sed -i -e "s:@@GENTOO_LIBDIR@@:$(get_libdir):g" \
		Lib/distutils/command/install.py \
		Lib/distutils/sysconfig.py \
		Lib/site.py \
		Lib/sysconfig.py \
		Lib/test/test_site.py \
		Makefile.pre.in \
		Modules/Setup.dist \
		Modules/getpath.c \
		setup.py || die "sed failed to replace @@GENTOO_LIBDIR@@"

	eautoconf
	eautoheader
}

src_configure() {
	if use build; then
		# Disable extraneous modules with extra dependencies.
		export PYTHON_DISABLE_MODULES="dbm _bsddb gdbm _curses _curses_panel readline _sqlite3 _tkinter _elementtree pyexpat"
		export PYTHON_DISABLE_SSL="1"
	else
		# dbm module can be linked against berkdb or gdbm.
		# Defaults to gdbm when both are enabled, #204343.
		local disable
		use berkdb   || use gdbm || disable+=" dbm"
		use berkdb   || disable+=" _bsddb"
		use gdbm     || disable+=" gdbm"
		use ncurses  || disable+=" _curses _curses_panel"
		use readline || disable+=" readline"
		use sqlite   || disable+=" _sqlite3"
		use ssl      || export PYTHON_DISABLE_SSL="1"
		use tk       || disable+=" _tkinter"
		use xml      || disable+=" _elementtree pyexpat" # _elementtree uses pyexpat.
		export PYTHON_DISABLE_MODULES="${disable}"

		if ! use xml; then
			ewarn "You have configured Python without XML support."
			ewarn "This is NOT a recommended configuration as you"
			ewarn "may face problems parsing any XML documents."
		fi
	fi

	if [[ -n "${PYTHON_DISABLE_MODULES}" ]]; then
		einfo "Disabled modules: ${PYTHON_DISABLE_MODULES}"
	fi

	if [[ "$(gcc-major-version)" -ge 4 ]]; then
		append-flags -fwrapv
	fi

	filter-flags -malign-double

	[[ "${ARCH}" == "alpha" ]] && append-flags -fPIC

	# https://bugs.gentoo.org/show_bug.cgi?id=50309
	if is-flagq -O3; then
		is-flagq -fstack-protector-all && replace-flags -O3 -O2
		use hardened && replace-flags -O3 -O2
	fi

	if tc-is-cross-compiler; then
		# Force some tests that try to poke fs paths.
		export ac_cv_file__dev_ptc=no
		export ac_cv_file__dev_ptmx=yes
	fi

	local dbmliborder
	if use gdbm; then
		dbmliborder+="${dbmliborder:+:}gdbm"
	fi
	if use berkdb; then
		dbmliborder+="${dbmliborder:+:}bdb"
	fi

	multilib-minimal_src_configure
}

multilib_src_configure() {
	# Export CXX so it ends up in /usr/lib/python2.X/config/Makefile.
	local CXX
	export CXX="${CXX:-$(tc-getCXX)}"

	# The configure script fails to use pkg-config correctly.
	# http://bugs.python.org/issue15506
	export ac_cv_path_PKG_CONFIG=${PKG_CONFIG:-$(tc-getPKG_CONFIG)}

	# Set LDFLAGS so we link modules with -lpython2.7 correctly.
	# Needed on FreeBSD unless Python 2.7 is already installed.
	# Please query BSD team before removing this!
	local LDFLAGS="${LDFLAGS}"
	append-ldflags "-L."

	local BUILD_DIR="${BUILD_DIR}"
	local S="${BUILD_DIR}"

	local ABI="${ABI:-default}"
	BUILD_DIR="${WORKDIR}/${CHOST}-${ABI}"

	mkdir -p "${BUILD_DIR}" || die
	pushd "${BUILD_DIR}" > /dev/null || die

	ECONF_SOURCE="${S}" OPT="" \
	econf \
		--with-fpectl \
		--enable-shared \
		$(use_enable ipv6) \
		$(use_with threads) \
		$(use wide-unicode && echo "--enable-unicode=ucs4" || echo "--enable-unicode=ucs2") \
		--infodir='${prefix}/share/info' \
		--mandir='${prefix}/share/man' \
		--with-dbmliborder="${dbmliborder}" \
		--with-libc="" \
		--enable-loadable-sqlite-extensions \
		--with-system-expat \
		--with-system-ffi

	if use threads && grep -q "#define POSIX_SEMAPHORES_NOT_ENABLED 1" pyconfig.h; then
		eerror "configure has detected that the sem_open function is broken."
		eerror "Please ensure that /dev/shm is mounted as a tmpfs with mode 1777."
		die "Broken sem_open function (bug 496328)"
	fi
	popd > /dev/null || die
}

multilib_src_compile() {
	local S="${BUILD_DIR}"
	local ABI="${ABI:-default}"
	local BUILD_DIR="${WORKDIR}/${CHOST}-${ABI}"

	# Avoid invoking pgen for cross-compiles.
	touch Include/graminit.h Python/graminit.c

	pushd "${BUILD_DIR}" > /dev/null || die
	emake

	# Work around bug 329499. See also bug 413751 and 457194.
	if has_version dev-libs/libffi[pax_kernel]; then
		pax-mark E python
	else
		pax-mark m python
	fi
	popd > /dev/null || die
}

multilib_src_test() {
	# Tests will not work when cross compiling.
	if tc-is-cross-compiler; then
		elog "Disabling tests due to crosscompiling."
		return
	fi

	local S="${BUILD_DIR}"
	local ABI="${ABI:-default}"
	local BUILD_DIR="${WORKDIR}/${CHOST}-${ABI}"
	pushd "${BUILD_DIR}" > /dev/null || die

	# Skip failing tests.
	local skipped_tests="distutils gdb"

	for test in ${skipped_tests}; do
		mv "${S}"/Lib/test/test_${test}.py "${T}/test_${test}-${ABI}.py"
	done

	# Rerun failed tests in verbose mode (regrtest -w).
	emake test EXTRATESTOPTS="-w" < /dev/tty
	local result="$?"

	for test in ${skipped_tests}; do
		mv "${T}/test_${test}-${ABI}.py" "${S}/Lib/test/test_${test}.py"
	done

	elog "The following tests have been skipped:"
	for test in ${skipped_tests}; do
		elog "test_${test}.py"
	done

	elog "If you would like to run them, you may:"
	elog "cd '${EPREFIX}/usr/$(get_libdir)/python${SLOT}/test'"
	elog "and run the tests separately."

	if [[ "${result}" -ne 0 ]]; then
		die "emake test failed"
	fi
	popd > /dev/null || die
}

multilib_src_install() {
	local S="${BUILD_DIR}"
	local ABI="${ABI:-default}"
	local BUILD_DIR="${WORKDIR}/${CHOST}-${ABI}"
	pushd "${BUILD_DIR}" > /dev/null || die

	emake DESTDIR="${D}" altinstall

	local libdir=${ED}usr/$(get_libdir)/python${SLOT}
	sed -e "s/\(LDFLAGS=\).*/\1/" -i "${libdir}/config/Makefile" || die "sed failed"

	# Fix collisions between different slots of Python.
	mv "${ED}usr/bin/2to3" "${ED}usr/bin/2to3-${SLOT}"
	mv "${ED}usr/bin/pydoc" "${ED}usr/bin/pydoc${SLOT}"
	mv "${ED}usr/bin/idle" "${ED}usr/bin/idle${SLOT}"
	rm -f "${ED}usr/bin/smtpd.py"

	if use build; then
		rm -fr "${ED}usr/bin/idle${SLOT}" "${libdir}/"{bsddb,dbhash.py,idlelib,lib-tk,sqlite3,test}
	else
		use berkdb || rm -r "${libdir}/"{bsddb,dbhash.py,test/test_bsddb*} || die
		use sqlite || rm -r "${libdir}/"{sqlite3,test/test_sqlite*} || die
		use tk || rm -r "${ED}usr/bin/idle${SLOT}" "${libdir}/"{idlelib,lib-tk} || die
		use elibc_uclibc && rm -fr "${libdir}/"{bsddb/test,test}
	fi

	use threads || rm -r "${libdir}/multiprocessing" || die
	use wininst || rm -r "${libdir}/distutils/command/"wininst-*.exe || die

	insinto /usr/share/gdb/auto-load/usr/$(get_libdir) #443510
	local libname=$(printf 'e:\n\t@echo $(INSTSONAME)\ninclude Makefile\n' | \
		emake --no-print-directory -s -f - 2>/dev/null)
	newins "${S}"/Tools/gdb/libpython.py "${libname}"-gdb.py

	# for python-exec
	local EPYTHON PYTHON PYTHON_SITEDIR

	python_export python${SLOT}-${ABI} EPYTHON PYTHON PYTHON_SITEDIR

	# if not using a cross-compiler, use the fresh binary
	if ! tc-is-cross-compiler; then
		local PYTHON=./python
		local -x LD_LIBRARY_PATH=${LD_LIBRARY_PATH+${LD_LIBRARY_PATH}:}.
	fi

	echo "EPYTHON='${EPYTHON}'" > epython.py
	python_domodule epython.py

	popd > /dev/null || die
}

multilib_src_install_all() {
	# Backwards compat with Gentoo divergence
	dosym python${SLOT}-config /usr/bin/python-config-${SLOT}

	dodoc "${S}"/Misc/{ACKS,HISTORY,NEWS}

	if use examples; then
		insinto /usr/share/doc/${PF}/examples
		doins -r "${S}"/Tools
	fi

	newconfd "${FILESDIR}/pydoc.conf" pydoc-${SLOT}
	newinitd "${FILESDIR}/pydoc.init" pydoc-${SLOT}

	declare -a abis=( $(multilib_get_enabled_abis) )
	local bestabi=${abis[$(( ${#abis[@]} - 1))]} abi file abifile

	sed \
		-e "s:@PYDOC_PORT_VARIABLE@:PYDOC${SLOT/./_}_PORT:" \
		-e "s:@PYDOC@:pydoc${SLOT}:" \
		-i "${ED}etc/conf.d/pydoc-${SLOT}" "${ED}etc/init.d/pydoc-${SLOT}" \
		|| die "sed failed"

	# if more than one multilib abi is enabled we have some extra work.
	if [[ ${#abis[@]} -gt 1 ]] ; then
		# for each abi ...
		for abi in "${abis[@]}" ; do
			# Backwards compat with Gentoo divergence
			dosym python${SLOT}-config-${abi} /usr/bin/python${SLOT}-${abi}-config

			# ... fix multilib scripts with build-generated no-multilib-shebangs.
			for file in "${MULTILIB_WRAPPED_EXECUTABLES[@]#@}" ; do
				[[ ${file} == /usr/bin/python${SLOT} ]] && continue # not a script

				abifile="${ED}${file#/}-${abi}"
				if [[ -f ${abifile} ]] ; then
					# keep native ABI suffix unmangled by using EPYTHON export rather than argument
					EPYTHON="python${SLOT}-${abi}" python_rewrite_shebang_multilib "${abifile}"
				else
					die "Couldn't find \"${abifile}\""
				fi
			done

			# Skip the remaining steps for the native abi.  For non-native abis, ...
			[[ ${abi} == ${bestabi} ]] && continue

			# ... install initscript templates, ...
			newconfd "${FILESDIR}/pydoc.conf" pydoc-${SLOT}-${abi}
			newinitd "${FILESDIR}/pydoc.init" pydoc-${SLOT}-${abi}
			
			# ... and instantiate them.
			sed \
				-e "s:@PYDOC_PORT_VARIABLE@:PYDOC${SLOT/./_}_${abi/-/_}_PORT:" \
				-e "s:@PYDOC@:pydoc${SLOT}-${abi}:" \
				-i "${ED}etc/conf.d/pydoc-${SLOT}-${abi}" "${ED}etc/init.d/pydoc-${SLOT}-${abi}" \
				|| die "sed failed"
		done
	fi
}

pkg_preinst() {
	if has_version "<${CATEGORY}/${PN}-${SLOT}" && ! has_version "${CATEGORY}/${PN}:2.7[${MULTILIB_USEDEP}]"; then
		python_updater_warning="1"
	fi
}

eselect_python_update() {
	if [[ -z "$(eselect python show)" || ! -f "${EROOT}usr/bin/$(eselect python show)" ]]; then
		eselect python update
	fi

	if [[ -z "$(eselect python show --python${PV%%.*})" || ! -f "${EROOT}usr/bin/$(eselect python show --python${PV%%.*})" ]]; then
		eselect python update --python${PV%%.*}
	fi
}

pkg_postinst() {
	eselect_python_update

	if [[ "${python_updater_warning}" == "1" ]]; then
		ewarn "You have just upgraded from an older version of Python."
		ewarn "You should switch active version of Python ${PV%%.*} and run"
		ewarn "'python-updater [options]' to rebuild Python modules."
	fi
}

pkg_postrm() {
	eselect_python_update
}
