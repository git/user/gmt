# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit eutils multilib prefix toolchain-funcs versionator virtualx autotools-multilib

MY_P="${PN}${PV/_beta/b}"

DESCRIPTION="Tk Widget Set"
HOMEPAGE="http://www.tcl.tk/"
SRC_URI="mirror://sourceforge/tcl/${MY_P}-src.tar.gz"

LICENSE="tcltk"
SLOT="0/8.6"
KEYWORDS="~amd64"
IUSE="debug +threads truetype aqua xscreensaver"

RDEPEND="
	!aqua? (
		media-libs/fontconfig[${MULTILIB_USEDEP}]
		x11-libs/libX11[${MULTILIB_USEDEP}]
		x11-libs/libXt[${MULTILIB_USEDEP}]
		truetype? ( x11-libs/libXft[${MULTILIB_USEDEP}] )
		xscreensaver? ( x11-libs/libXScrnSaver[${MULTILIB_USEDEP}] )
	)
	~dev-lang/tcl-${PV}[${MULTILIB_USEDEP}]"
DEPEND="${RDEPEND}
	!aqua? ( x11-proto/xproto[${MULTILIB_USEDEP}] )"

# Not bumped to 8.6
#RESTRICT=test

SPARENT="${WORKDIR}/${MY_P}"
S="${SPARENT}"/unix

MULTILIB_WRAPPED_HEADERS=( usr/include/tkPlatDecls.h )
MULTILIB_WRAPPED_EXECUTABLES=( "/usr/bin/wish${V1}" )

AUTOTOOLS_TEST_MAKE=Xemake

V1=$(get_version_component_range 1-2)

src_prepare() {
	find \
		"${SPARENT}"/compat/* \
		-delete || die

	epatch \
		"${FILESDIR}"/${PN}-8.5.11-fedora-xft.patch \
		"${FILESDIR}"/${PN}-8.5.13-multilib.patch

	epatch "${FILESDIR}"/${PN}-8.4.15-aqua.patch
	eprefixify Makefile.in

	# Bug 125971
	epatch "${FILESDIR}"/${PN}-8.5.14-conf.patch

	# Bug 354067 : the same applies to tcl, since the patch is about tcl.m4, just
	# copy the tcl patch
	epatch "${FILESDIR}"/tcl-8.5.9-gentoo-fbsd.patch

	# Make sure we use the right pkg-config, and link against fontconfig
	# (since the code base uses Fc* functions).
	sed \
		-e 's/FT_New_Face/XftFontOpen/g' \
		-e "s:\<pkg-config\>:$(tc-getPKG_CONFIG):" \
		-e 's:xft freetype2:xft freetype2 fontconfig:' \
		-i configure.in || die
	rm -f configure || die

	sed \
		-e 's:-O[2s]\?::g' \
		-i tcl.m4 || die

	autotools-multilib_src_prepare
	eautoconf
}

src_configure() {
	local CC="$(tc-getCC)"
	export CC
	autotools-multilib_src_configure \
		--with-tcl="${EPREFIX}/usr/@GET_LIBDIR@" \
		$(use_enable threads) \
		$(use_enable aqua) \
		$(use_enable truetype xft) \
		$(use_enable xscreensaver xss) \
		$(use_enable debug symbols)
}

ehook autotools-multilib-per-abi-pre_src_install pre_abi_install
pre_abi_install() {
	old_S="${S}"
	S= 
	return 0
}

ehook autotools-multilib-per-abi-post_src_install post_abi_install
post_abi_install() {
	#short version number
	local mylibdir=$(get_libdir)
	S="${old_S}"

	# normalize $S path, bug #280766 (pkgcore)
	local nS="$(cd "${S}"; pwd)"

	# fix the tkConfig.sh to eliminate refs to the build directory
	# and drop unnecessary -L inclusion to default system libdir

	sed \
		-e "/^TK_BUILD_LIB_SPEC=/s:-L${SPARENT}.*unix *::g" \
		-e "/^TK_LIB_SPEC=/s:-L${EPREFIX}/usr/${mylibdir} *::g" \
		-e "/^TK_SRC_DIR=/s:${SPARENT}:${EPREFIX}/usr/${mylibdir}/tk${V1}/include:g" \
		-e "/^TK_BUILD_STUB_LIB_SPEC=/s:-L${SPARENT}.*unix *::g" \
		-e "/^TK_STUB_LIB_SPEC=/s:-L${EPREFIX}/usr/${mylibdir} *::g" \
		-e "/^TK_BUILD_STUB_LIB_PATH=/s:${SPARENT}.*unix:${EPREFIX}/usr/${mylibdir}:g" \
		-e "/^TK_LIB_FILE=/s:'libtk${V1}..TK_DBGX..so':\"libk${V1}\$\{TK_DBGX\}.so\":g" \
		-i "${ED}"usr/${mylibdir}/tkConfig.sh || die
	if use prefix && [[ ${CHOST} != *-darwin* && ${CHOST} != *-mint* ]] ; then
		sed \
			-e "/^TK_CC_SEARCH_FLAGS=/s|'$|:${EPREFIX}/usr/${mylibdir}'|g" \
			-e "/^TK_LD_SEARCH_FLAGS=/s|'$|:${EPREFIX}/usr/${mylibdir}'|" \
			-i "${ED}"usr/${mylibdir}/tkConfig.sh || die
	fi

	# install private headers
	insinto /usr/${mylibdir}/tk${V1}/include/unix
	doins "${S}"/*.h
	insinto /usr/${mylibdir}/tk${V1}/include/generic
	doins "${SPARENT}"/generic/*.h
	rm -f "${ED}"usr/${mylibdir}/tk${V1}/include/generic/{tk,tkDecls,tkPlatDecls}.h || die

	# install symlink for libraries
	dosym libtk${V1}$(get_libname) /usr/${mylibdir}/libtk$(get_libname)
	dosym libtkstub${V1}.a /usr/${mylibdir}/libtkstub.a

	dosym wish${V1} /usr/bin/wish

	if multilib_is_best_abi ; then
		dodoc "${SPARENT}"/{ChangeLog*,README,changes}
	else
		# remove demos from non-best-abis
		rm -rf "${ED}usr/${mylibdir}/tk${V1}/demos" || die
	fi
}
