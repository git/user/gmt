# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"
inherit multilib-build

DESCRIPTION="Virtual for MySQL client or database"
HOMEPAGE=""
SRC_URI=""

LICENSE=""
SLOT="0"
KEYWORDS="~amd64"
IUSE="embedded minimal static"

DEPEND=""
# TODO: add Drizzle here
RDEPEND="|| (
       =dev-db/mariadb-${PV}*[embedded=,minimal=,static=,${MULTILIB_USEDEP}]
       =dev-db/mysql-${PV}*[embedded=,minimal=,static=,${MULTILIB_USEDEP}]
       =dev-db/percona-server-${PV}*[embedded=,minimal=,static=,${MULTILIB_USEDEP}]
       =dev-db/mariadb-galera-${PV}*[embedded=,minimal=,static=,${MULTILIB_USEDEP}]
)"
