# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
inherit gtk-doc eutils multilib pam-multilib pax-utils systemd user multilib-minimal

DESCRIPTION="Policy framework for controlling privileges for system-wide services"
HOMEPAGE="http://www.freedesktop.org/wiki/Software/polkit"
SRC_URI="http://www.freedesktop.org/software/${PN}/releases/${P}.tar.gz"

LICENSE="LGPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE="examples gtk +introspection jit kde nls pam selinux systemd"

RDEPEND="ia64? ( =dev-lang/spidermonkey-1.8.5*[-debug,jit=,${MULTILIB_USEDEP}] )
	mips? ( =dev-lang/spidermonkey-1.8.5*[-debug,${MULTILIB_USEDEP}] )
	!ia64? ( !mips? ( dev-lang/spidermonkey:17[-debug,${MULTILIB_USEDEP}] ) )
	>=dev-libs/glib-2.32[${MULTILIB_USEDEP}]
	>=dev-libs/expat-2:=[${MULTILIB_USEDEP}]
	introspection? ( >=dev-libs/gobject-introspection-1[${MULTILIB_USEDEP}] )
	pam? (
		sys-auth/pambase
		virtual/pam[${MULTILIB_USEDEP}]
		)
	selinux? ( sec-policy/selinux-policykit )
	systemd? ( sys-apps/systemd:0=[${MULTILIB_USEDEP}] )"
DEPEND="${RDEPEND}
	app-text/docbook-xml-dtd:4.1.2
	app-text/docbook-xsl-stylesheets
	dev-libs/libxslt
	dev-util/intltool
	virtual/pkgconfig"
PDEPEND="
	gtk? ( || (
		>=gnome-extra/polkit-gnome-0.105
		lxde-base/lxpolkit
		) )
	kde? ( sys-auth/polkit-kde-agent )
	!systemd? ( sys-auth/consolekit[policykit] )"

QA_MULTILIB_PATHS="
	usr/lib/polkit-1/polkit-agent-helper-1
	usr/lib/polkit-1/polkitd"

pkg_setup() {
	local u=polkitd
	local g=polkitd
	local h=/var/lib/polkit-1

	enewgroup ${g}
	enewuser ${u} -1 -1 ${h} ${g}
	esethome ${u} ${h}
}

src_prepare() {
	sed -i -e 's|unix-group:wheel|unix-user:0|' src/polkitbackend/*-default.rules || die #401513
}

multilib_src_configure() {
	ECONF_SOURCE="${S}" econf \
		--localstatedir="${EPREFIX}"/var \
		--disable-static \
		--enable-man-pages \
		--disable-gtk-doc \
		$(use_enable systemd libsystemd-login) \
		$(use_enable introspection) \
		--disable-examples \
		$(use_enable nls) \
		$(if use ia64 || use mips; then echo --with-mozjs=mozjs185; else echo --with-mozjs=mozjs-17.0; fi) \
		"$(systemd_with_unitdir)" \
		--with-authfw=$(usex pam pam shadow) \
		$(use pam && echo --with-pam-module-dir="$(getpam_mod_dir)") \
		--with-os-type=gentoo
}

multilib_src_compile() {
	default

	# Required for polkitd on hardened/PaX due to spidermonkey's JIT
	local f='src/polkitbackend/.libs/polkitd test/polkitbackend/.libs/polkitbackendjsauthoritytest'
	local m=''
	# Only used when USE="jit" is enabled for 'dev-lang/spidermonkey:17' wrt #485910
	has_version 'dev-lang/spidermonkey:17[jit]' && m='m'
	# ia64 and mips uses spidermonkey-1.8.5 which requires different pax-mark flags
	use ia64 && m='mr'
	use mips && m='mr'
	pax-mark ${m} ${f}
}

multilib_src_install_all() {
	dodoc docs/TODO HACKING NEWS README

	fowners -R polkitd:root /{etc,usr/share}/polkit-1/rules.d

	diropts -m0700 -o polkitd -g polkitd
	keepdir /var/lib/polkit-1

	if use examples; then
		insinto /usr/share/doc/${PF}/examples
		doins src/examples/{*.c,*.policy*}
	fi

	prune_libtool_files
}

pkg_postinst() {
	chown -R polkitd:root "${EROOT}"/{etc,usr/share}/polkit-1/rules.d
	chown -R polkitd:polkitd "${EROOT}"/var/lib/polkit-1
}
