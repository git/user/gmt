# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit qt4-build-multilib

DESCRIPTION="The QtScript module for the Qt toolkit"
SLOT="4"
if [[ ${QT4_BUILD_TYPE} == live ]]; then
	KEYWORDS=""
else
	KEYWORDS="~amd64"
fi
IUSE="iconv +jit"

DEPEND="
	~dev-qt/qtcore-${PV}[aqua=,debug=,${MULTILIB_USEDEP}]
"
RDEPEND="${DEPEND}
	abi_x86_32? (
		!<=app-emulation/emul-linux-x86-qtlibs-20140508-r1
		!app-emulation/emul-linux-x86-qtlibs[-abi_x86_32(-)]
	)
"

PATCHES=( "${FILESDIR}/4.8.2-javascriptcore-x32.patch" )

MULTILIB_WRAPPED_HEADERS=(
	/usr/include/qt4/Gentoo/gentoo-qtscript-qconfig.h
)

pkg_setup() {
	QT4_TARGET_DIRECTORIES="
		src/script"

	QT4_EXTRACT_DIRECTORIES="${QT4_TARGET_DIRECTORIES}
		include/Qt
		include/QtCore
		include/QtScript
		src/3rdparty/javascriptcore
		src/corelib"

	QCONFIG_ADD="script"
	QCONFIG_DEFINE="QT_SCRIPT"

	qt4-build-multilib_pkg_setup
}

ehook qt4-build-multilib-global-pre_src_configure global_pre_src_configure
global_pre_src_configure() {
	myconf+="
		$(qt_use iconv)
		$(qt_use jit javascript-jit)
		-no-xkb -no-fontconfig -no-xrender -no-xrandr -no-xfixes -no-xcursor -no-xinerama
		-no-xshape -no-sm -no-opengl -no-nas-sound -no-dbus -no-cups -no-nis -no-gif
		-no-libpng -no-libmng -no-libjpeg -no-openssl -system-zlib -no-webkit -no-phonon
		-no-qt3support -no-xmlpatterns -no-freetype -no-libtiff
		-no-accessibility -no-fontconfig -no-glib -no-opengl -no-svg
		-no-gtkstyle"
}

ehook qt4-build-multilib-per-abi-post_src_install qtscript_perabi_src_install
qtscript_perabi_src_install() {
	# install private headers
	insinto "${QTHEADERDIR#${EPREFIX}}"/QtScript/private
	find "${S}"/src/script -type f -name "*_p.h" -exec doins {} +
}
