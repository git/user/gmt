# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit qt4-build-multilib

DESCRIPTION="Cross-platform application development framework"
SLOT="4"
if [[ ${QT4_BUILD_TYPE} == live ]]; then
	KEYWORDS=""
else
	KEYWORDS="~amd64"
fi
IUSE="+glib iconv icu qt3support ssl"

DEPEND="
	sys-libs/zlib[${MULTILIB_USEDEP}]
	glib? ( dev-libs/glib:2[${MULTILIB_USEDEP}] )
	icu? ( >=dev-libs/icu-49:=[${MULTILIB_USEDEP}] )
	ssl? ( dev-libs/openssl[${MULTILIB_USEDEP}] )
	!<x11-libs/cairo-1.10.2-r2
"
RDEPEND="${DEPEND}
	abi_x86_32? (
		!<=app-emulation/emul-linux-x86-qtlibs-20140508-r1
		!app-emulation/emul-linux-x86-qtlibs[-abi_x86_32(-)]
	)
"
PDEPEND="
	qt3support? ( ~dev-qt/qtgui-${PV}[aqua=,debug=,glib=,qt3support,${MULTILIB_USEDEP}] )
"

PATCHES=(
	"${FILESDIR}/moc-boost-lexical-cast.patch"
	"${FILESDIR}/CVE-2013-4549-01-disallow-deep-or-widely-nested-entity-refs.patch"
	"${FILESDIR}/CVE-2013-4549-02-fully-expand-entities.patch"
)

MULTILIB_WRAPPED_HEADERS=(
	/usr/include/qt4/Gentoo/gentoo-qtcore-qconfig.h
	/usr/include/qt4/Qt/qconfig.h
	/usr/include/qt4/QtCore/qconfig.h
)

# fixme: migrate to MULTILIB_CHOST_TOOLS once some equivalent to @wrapping
# is implemented for it.
MULTILIB_WRAPPED_EXECUTABLES=(
	@/usr/bin/qmake
)

pkg_setup() {
	QT4_TARGET_DIRECTORIES="
		src/tools/bootstrap
		src/tools/moc
		src/tools/rcc
		src/tools/uic
		src/corelib
		src/xml
		src/network
		src/plugins/codecs
		tools/linguist/lconvert
		tools/linguist/lrelease
		tools/linguist/lupdate"

	QT4_EXTRACT_DIRECTORIES="${QT4_TARGET_DIRECTORIES}
		include
		src/plugins/plugins.pro
		src/plugins/qpluginbase.pri
		src/src.pro
		src/3rdparty/des
		src/3rdparty/harfbuzz
		src/3rdparty/md4
		src/3rdparty/md5
		src/3rdparty/sha1
		src/3rdparty/easing
		src/3rdparty/zlib_dependency.pri
		src/declarative
		src/gui
		src/script
		tools/shared
		tools/linguist/shared
		translations"

	qt4-build_pkg_setup
}

ehook qt4-build-multilib-global-pre_src_prepare qtcore_pre_src_prepare
qtcore_pre_src_prepare() {
	# Don't pre-strip, bug 235026
	for i in kr jp cn tw; do
		echo "CONFIG+=nostrip" >> "${S}"/src/plugins/codecs/${i}/${i}.pro
	done
	return 0
}

ehook qt4-build-multilib-per-abi-post_src_prepare qtcore_post_abi_src_prepare
qtcore_post_abi_src_prepare() {
	# bug 172219
	sed -i -e "s:CXXFLAGS.*=:CXXFLAGS=${CXXFLAGS} :" \
		"${S}/qmake/Makefile.unix" || die "sed qmake/Makefile.unix CXXFLAGS failed"
	sed -i -e "s:LFLAGS.*=:LFLAGS=${LDFLAGS} :" \
		"${S}/qmake/Makefile.unix" || die "sed qmake/Makefile.unix LDFLAGS failed"

	# bug 427782
	sed -i -e "/^CPPFLAGS/s/-g//" \
		"${S}/qmake/Makefile.unix" || die "sed qmake/Makefile.unix CPPFLAGS failed"
	sed -i -e "s/setBootstrapVariable QMAKE_CFLAGS_RELEASE/QMakeVar set QMAKE_CFLAGS_RELEASE/" \
		-e "s/setBootstrapVariable QMAKE_CXXFLAGS_RELEASE/QMakeVar set QMAKE_CXXFLAGS_RELEASE/" \
		"${S}/configure" || die "sed configure setBootstrapVariable failed"
}

ehook qt4-build-multilib-global-pre_src_configure qtcore_pre_src_configure
qtcore_pre_src_configure() {
	myconf+="
		-no-accessibility -no-xmlpatterns -no-multimedia -no-audio-backend -no-phonon
		-no-phonon-backend -no-svg -no-webkit -no-script -no-scripttools -no-declarative
		-system-zlib -no-gif -no-libtiff -no-libpng -no-libmng -no-libjpeg
		-no-cups -no-dbus -no-gtkstyle -no-nas-sound -no-opengl -no-openvg
		-no-sm -no-xshape -no-xvideo -no-xsync -no-xinerama -no-xcursor -no-xfixes
		-no-xrandr -no-xrender -no-mitshm -no-fontconfig -no-freetype -no-xinput -no-xkb
		$(qt_use glib)
		$(qt_use iconv)
		$(qt_use icu)
		$(use ssl && echo -openssl-linked || echo -no-openssl)
		$(qt_use qt3support)"
}

ehook qt4-build-multilib-global-post_src_install qtcore_post_src_install
qtcore_post_src_install() {
	# remove .la files
	prune_libtool_files
}

ehook qt4-build-multilib-per-abi-pre_src_install qtcore_perabi_src_install
qtcore_perabi_src_install() {

	if multilib_is_best_abi ; then
		dobin bin/{moc,rcc,uic,lconvert,lrelease,lupdate}
	fi
	dobin bin/qmake

	install_directories src/{corelib,xml,network,plugins/codecs}

	emake INSTALL_ROOT="${D}" install_mkspecs

	# install private headers
	insinto "${QTHEADERDIR#${EPREFIX}}"/QtCore/private
	find "${S}"/src/corelib -type f -name "*_p.h" -exec doins {} +

	# use freshly built libraries
	local DYLD_FPATH=
	[[ -d "${S}"/lib/QtCore.framework ]] \
		&& DYLD_FPATH=$(for x in "${S}"/lib/*.framework; do echo -n ":$x"; done)
	DYLD_LIBRARY_PATH="${S}/lib${DYLD_FPATH}" \
		LD_LIBRARY_PATH="${S}/lib" \
		"${S}"/bin/lrelease translations/*.ts \
		|| die "generating translations failed"
	insinto "${QTTRANSDIR#${EPREFIX}}"
	doins translations/*.qm

	setqtenv
	fix_library_files

	# List all the multilib libdirs
	local libdirs=
	for libdir in $(get_all_libdirs); do
		libdirs+=":${EPREFIX}/usr/${libdir}/qt4"
	done

	cat <<-EOF > "${T}"/44qt4$( multilib_is_best_abi || echo "-${ABI}" )
	LDPATH="${libdirs:1}"
	EOF
	doenvd "${T}"/44qt4$( multilib_is_best_abi || echo "-${ABI}" )

	if multilib_is_best_abi ; then
		dodir "${QTDATADIR#${EPREFIX}}"/mkspecs/gentoo
		mv "${D}${QTDATADIR}"/mkspecs/qconfig.pri "${D}${QTDATADIR}"/mkspecs/gentoo \
			|| die "failed to move qconfig.pri"
	else
		# TODO? This file contains a QT_ARCH... are non-best-abi's prone to
		# problems, at all, owing to the best abi's QT_ARCH value being there?
		# If so, then we should probably keep the other .pri's as we
		# did with 44qt4 above, and use them, as appropriate, during builds.
		rm "${D}${QTDATADIR}"/mkspecs/qconfig.pri \
			|| die "failed to remove qconfig.pri for abi ${ABI}"
	fi

	# Framework hacking
	if use aqua && [[ ${CHOST#*-darwin} -ge 9 ]]; then
		# TODO: do this better
		sed -i -e '2a#include <QtCore/Gentoo/gentoo-qconfig.h>\n' \
				"${D}${QTLIBDIR}"/QtCore.framework/Headers/qconfig.h \
			|| die "sed for qconfig.h failed."
		dosym "${QTHEADERDIR#${EPREFIX}}"/Gentoo "${QTLIBDIR#${EPREFIX}}"/QtCore.framework/Headers/Gentoo
	else
		sed -i -e '2a#include <Gentoo/gentoo-qconfig.h>\n' \
				"${D}${QTHEADERDIR}"/QtCore/qconfig.h \
				"${D}${QTHEADERDIR}"/Qt/qconfig.h \
			|| die "sed for qconfig.h failed"
	fi

	QCONFIG_DEFINE="QT_ZLIB"
	install_qconfigs

	# remove .la files
	prune_libtool_files

	keepdir "${QTSYSCONFDIR#${EPREFIX}}"

	# Framework magic
	fix_includes

	# don't perform default installation steps, everything is done.
	return 1
}
