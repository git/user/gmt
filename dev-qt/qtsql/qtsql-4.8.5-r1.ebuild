# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit qt4-build-multilib

DESCRIPTION="The SQL module for the Qt toolkit"
SLOT="4"
if [[ ${QT4_BUILD_TYPE} == live ]]; then
	KEYWORDS=""
else
	KEYWORDS="~amd64"
fi
IUSE="firebird freetds mysql oci8 odbc postgres qt3support +sqlite"

REQUIRED_USE="
	|| ( firebird freetds mysql oci8 odbc postgres sqlite )
"

DEPEND="
	~dev-qt/qtcore-${PV}[aqua=,debug=,qt3support=,${MULTILIB_USEDEP}]
	firebird? ( dev-db/firebird[${MULTILIB_USEDEP}] )
	freetds? ( dev-db/freetds[${MULTILIB_USEDEP}] )
	mysql? ( virtual/mysql[${MULTILIB_USEDEP}] )
	oci8? ( dev-db/oracle-instantclient-basic[${MULTILIB_USEDEP}] )
	odbc? ( || ( dev-db/unixODBC dev-db/libiodbc[${MULTILIB_USEDEP}] ) )
	postgres? ( dev-db/postgresql-base[${MULTILIB_USEDEP}] )
	sqlite? ( dev-db/sqlite:3[${MULTILIB_USEDEP}] )
"
RDEPEND="${DEPEND}
	abi_x86_32? (
		!<=app-emulation/emul-linux-x86-qtlibs-20140508-r1
		!app-emulation/emul-linux-x86-qtlibs[-abi_x86_32(-)]
	)
"

pkg_setup() {
	QT4_TARGET_DIRECTORIES="
		src/sql
		src/plugins/sqldrivers"

	QT4_EXTRACT_DIRECTORIES="${QT4_TARGET_DIRECTORIES}
		include/Qt
		include/QtCore
		include/QtSql
		src/src.pro
		src/corelib
		src/plugins
		src/tools/tools.pro"

	qt4-build-multilib_pkg_setup
}

src_configure() {
	myconf+="
		$(qt_use firebird sql-ibase  plugin)
		$(qt_use freetds  sql-tds    plugin)
		$(qt_use mysql    sql-mysql  plugin) $(use mysql && echo "-I${EPREFIX}/usr/include/mysql -L${EPREFIX}/usr/@GET_LIBDIR@/mysql")
		$(qt_use oci8     sql-oci    plugin) $(use oci8 && echo "-I${ORACLE_HOME}/include -L${ORACLE_HOME}/@GET_LIBDIR@")
		$(qt_use odbc     sql-odbc   plugin) $(use odbc && echo "-I${EPREFIX}/usr/include/iodbc")
		$(qt_use postgres sql-psql   plugin) $(use postgres && echo "-I${EPREFIX}/usr/include/postgresql/pgsql")
		$(qt_use sqlite   sql-sqlite plugin) $(use sqlite && echo -system-sqlite)
		-no-sql-db2
		-no-sql-sqlite2
		-no-sql-symsql
		$(qt_use qt3support)
		-no-accessibility -no-xmlpatterns -no-multimedia -no-audio-backend -no-phonon
		-no-phonon-backend -no-svg -no-webkit -no-script -no-scripttools -no-declarative
		-system-zlib -no-gif -no-libtiff -no-libpng -no-libmng -no-libjpeg -no-openssl
		-no-cups -no-dbus -no-gtkstyle -no-nas-sound -no-opengl
		-no-sm -no-xshape -no-xvideo -no-xsync -no-xinerama -no-xcursor -no-xfixes
		-no-xrandr -no-xrender -no-mitshm -no-fontconfig -no-freetype -no-xinput -no-xkb
		-no-glib"

	qt4-build-multilib_src_configure
}
