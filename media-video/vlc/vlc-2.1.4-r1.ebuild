# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"

SCM=""
if [ "${PV%9999}" != "${PV}" ] ; then
	SCM="git-r3"

	if [ "${PV%.9999}" != "${PV}" ] ; then
		EGIT_REPO_URI="git://git.videolan.org/vlc/vlc-${PV%.9999}.git"
	else
		EGIT_REPO_URI="git://git.videolan.org/vlc.git"
	fi
fi

inherit eutils eutils-multilib multilib autotools toolchain-funcs flag-o-matic virtualx multilib-minimal ${SCM}

MY_PV="${PV/_/-}"
MY_PV="${MY_PV/-beta/-test}"
MY_P="${PN}-${MY_PV}"

DESCRIPTION="VLC media player - Video player and streamer"
HOMEPAGE="http://www.videolan.org/vlc/"
if [ "${PV%9999}" != "${PV}" ] ; then # Live ebuild
	SRC_URI=""
elif [[ "${MY_P}" == "${P}" ]]; then
	SRC_URI="http://download.videolan.org/pub/videolan/${PN}/${PV}/${P}.tar.xz"
else
	SRC_URI="http://download.videolan.org/pub/videolan/testing/${MY_P}/${MY_P}.tar.xz"
fi

LICENSE="LGPL-2.1 GPL-2"
SLOT="0/5-7" # vlc - vlccore

if [ "${PV%9999}" = "${PV}" ] ; then
	KEYWORDS="~amd64"
else
	KEYWORDS=""
fi

IUSE="a52 aalib alsa altivec atmo +audioqueue avahi +avcodec
	+avformat bidi bluray cdda cddb chromaprint dbus dc1394 debug dirac
	directfb directx dts dvb +dvbpsi dvd dxva2 elibc_glibc egl +encode faad fdk
	fluidsynth +ffmpeg flac fontconfig +gcrypt gme gnome gnutls
	growl httpd ieee1394 ios-vout jack kate kde libass libcaca libnotify
	libsamplerate libtiger linsys libtar lirc live lua +macosx
	+macosx-audio +macosx-dialog-provider +macosx-eyetv +macosx-quartztext
	+macosx-qtkit +macosx-vout matroska media-library mmx modplug mp3 mpeg
	mtp musepack ncurses neon ogg omxil opencv opengl optimisememory opus
	png +postproc projectm pulseaudio +qt4 qt5 rdp rtsp run-as-root samba
	schroedinger sdl sdl-image sftp shout sid skins speex sse svg +swscale
	taglib theora tremor truetype twolame udev upnp vaapi v4l vcdx vdpau
	vlm vnc vorbis wma-fixed +X x264 +xcb xml xv zvbi"

RDEPEND="
		!<media-video/ffmpeg-1.2:0
		dev-libs/libgpg-error:0[${MULTILIB_USEDEP}]
		net-dns/libidn:0[${MULTILIB_USEDEP}]
		>=sys-libs/zlib-1.2.5.1-r2:0[minizip,${MULTILIB_USEDEP}]
		virtual/libintl:0[${MULTILIB_USEDEP}]
		a52? ( >=media-libs/a52dec-0.7.4-r3:0[${MULTILIB_USEDEP}] )
		aalib? ( media-libs/aalib:0[${MULTILIB_USEDEP}] )
		alsa? ( >=media-libs/alsa-lib-1.0.24:0[${MULTILIB_USEDEP}] )
		avahi? ( >=net-dns/avahi-0.6:0[dbus,${MULTILIB_USEDEP}] )
		avcodec? ( virtual/ffmpeg:0[${MULTILIB_USEDEP}] )
		avformat? ( virtual/ffmpeg:0[${MULTILIB_USEDEP}] )
		bidi? ( >=dev-libs/fribidi-0.10.4:0[${MULTILIB_USEDEP}] )
		bluray? ( >=media-libs/libbluray-0.2.1:0[${MULTILIB_USEDEP}] )
		cddb? ( >=media-libs/libcddb-1.2.0:0[${MULTILIB_USEDEP}] )
		chromaprint? ( >=media-libs/chromaprint-0.6:0[${MULTILIB_USEDEP}] )
		dbus? ( >=sys-apps/dbus-1.0.2:0[${MULTILIB_USEDEP}] )
		dc1394? ( >=sys-libs/libraw1394-2.0.1:0[${MULTILIB_USEDEP}] >=media-libs/libdc1394-2.1.0:2[${MULTILIB_USEDEP}] )
		dirac? ( >=media-video/dirac-0.10.0:0[${MULTILIB_USEDEP}] )
		directfb? ( dev-libs/DirectFB:0[${MULTILIB_USEDEP}] sys-libs/zlib:0[${MULTILIB_USEDEP}] )
		dts? ( media-libs/libdca:0[${MULTILIB_USEDEP}] )
		dvbpsi? ( >=media-libs/libdvbpsi-0.2.1:0[${MULTILIB_USEDEP}] )
		dvd? ( media-libs/libdvdread:0 >=media-libs/libdvdnav-0.1.9:0[${MULTILIB_USEDEP}] )
		egl? ( virtual/opengl:0[${MULTILIB_USEDEP}] )
		elibc_glibc? ( >=sys-libs/glibc-2.8:2.2 )
		faad? ( >=media-libs/faad2-2.6.1:0[${MULTILIB_USEDEP}] )
		fdk? ( media-libs/fdk-aac:0[${MULTILIB_USEDEP}] )
		flac? ( media-libs/libogg:0 >=media-libs/flac-1.1.2:0[${MULTILIB_USEDEP}] )
		fluidsynth? ( >=media-sound/fluidsynth-1.1.2:0[${MULTILIB_USEDEP}] )
		fontconfig? ( media-libs/fontconfig:1.0[${MULTILIB_USEDEP}] )
		gcrypt? ( >=dev-libs/libgcrypt-1.2.0:0[${MULTILIB_USEDEP}] )
		gme? ( media-libs/game-music-emu:0[${MULTILIB_USEDEP}] )
		gnome? ( gnome-base/gnome-vfs:2[${MULTILIB_USEDEP}] dev-libs/glib:2[${MULTILIB_USEDEP}] )
		gnutls? ( >=net-libs/gnutls-3.0.20:0[${MULTILIB_USEDEP}] )
		ieee1394? ( >=sys-libs/libraw1394-2.0.1:0[${MULTILIB_USEDEP}] >=sys-libs/libavc1394-0.5.3:0[${MULTILIB_USEDEP}] )
		ios-vout? ( virtual/opengl:0[${MULTILIB_USEDEP}] )
		jack? ( >=media-sound/jack-audio-connection-kit-0.99.0-r1:0[${MULTILIB_USEDEP}] )
		kate? ( >=media-libs/libkate-0.3.0:0[${MULTILIB_USEDEP}] )
		libass? ( >=media-libs/libass-0.9.8:0[${MULTILIB_USEDEP}] media-libs/fontconfig:1.0[${MULTILIB_USEDEP}] )
		libcaca? ( >=media-libs/libcaca-0.99_beta14:0[${MULTILIB_USEDEP}] )
		libnotify? ( x11-libs/libnotify:0[${MULTILIB_USEDEP}]
			x11-libs/gtk+:2[${MULTILIB_USEDEP}]
			x11-libs/gdk-pixbuf:2[${MULTILIB_USEDEP}]
			dev-libs/glib:2[${MULTILIB_USEDEP}] )
		libsamplerate? ( media-libs/libsamplerate:0[${MULTILIB_USEDEP}] )
		libtar? ( >=dev-libs/libtar-1.2.11-r3:0[${MULTILIB_USEDEP}] )
		libtiger? ( >=media-libs/libtiger-0.3.1:0[${MULTILIB_USEDEP}] )
		linsys? ( >=media-libs/zvbi-0.2.28:0[${MULTILIB_USEDEP}] )
		lirc? ( app-misc/lirc:0[${MULTILIB_USEDEP}] )
		live? ( >=media-plugins/live-2011.12.23:0[${MULTILIB_USEDEP}] )
		lua? ( >=dev-lang/lua-5.1:0 )
		macosx-vout? ( virtual/opengl:0[${MULTILIB_USEDEP}] )
		matroska? (	>=dev-libs/libebml-1.0.0:0=[${MULTILIB_USEDEP}] >=media-libs/libmatroska-1.0.0:0=[${MULTILIB_USEDEP}] )
		modplug? ( >=media-libs/libmodplug-0.8.8.1:0[${MULTILIB_USEDEP}] )
		mp3? ( media-libs/libmad:0[${MULTILIB_USEDEP}] )
		mpeg? ( >=media-libs/libmpeg2-0.3.2:0[${MULTILIB_USEDEP}] )
		mtp? ( >=media-libs/libmtp-1.0.0:0[${MULTILIB_USEDEP}] )
		musepack? ( >=media-sound/musepack-tools-444:0[${MULTILIB_USEDEP}] )
		ncurses? ( sys-libs/ncurses:5[unicode,${MULTILIB_USEDEP}] )
		ogg? ( media-libs/libogg:0[${MULTILIB_USEDEP}] )
		opencv? ( >media-libs/opencv-2.0:0[${MULTILIB_USEDEP}] )
		opengl? ( virtual/opengl:0[${MULTILIB_USEDEP}] >=x11-libs/libX11-1.3.99.901:0[${MULTILIB_USEDEP}] )
		opus? ( >=media-libs/opus-1.0.3:0[${MULTILIB_USEDEP}] )
		png? ( media-libs/libpng:0=[${MULTILIB_USEDEP}] sys-libs/zlib:0[${MULTILIB_USEDEP}] )
		postproc? ( || ( >=media-video/ffmpeg-1.2:0[${MULTILIB_USEDEP}] media-libs/libpostproc:0[${MULTILIB_USEDEP}] ) )
		projectm? ( media-libs/libprojectm:0[${MULTILIB_USEDEP}] media-fonts/dejavu:0 )
		pulseaudio? ( >=media-sound/pulseaudio-0.9.22:0[${MULTILIB_USEDEP}] )
		qt4? ( >=dev-qt/qtgui-4.6.0:4[${MULTILIB_USEDEP}] >=dev-qt/qtcore-4.6.0:4[${MULTILIB_USEDEP}] )
		qt5? ( >=dev-qt/qtgui-5.1.0:5[${MULTILIB_USEDEP}]
			>=dev-qt/qtcore-5.1.0:5[${MULTILIB_USEDEP}] dev-qt/qtwidgets:5[${MULTILIB_USEDEP}] )
		rdp? ( net-misc/freerdp:0= )
		samba? ( || ( >=net-fs/samba-3.4.6:0[smbclient,${MULTILIB_USEDEP}] >=net-fs/samba-4.0.0:0[client,${MULTILIB_USEDEP}] ) )
		schroedinger? ( >=media-libs/schroedinger-1.0.10:0[${MULTILIB_USEDEP}] )
		sdl? ( >=media-libs/libsdl-1.2.10:0[${MULTILIB_USEDEP}]
			sdl-image? ( >=media-libs/sdl-image-1.2.10:0[${MULTILIB_USEDEP}] sys-libs/zlib:0[${MULTILIB_USEDEP}] ) )
		sftp? ( net-libs/libssh2:0[${MULTILIB_USEDEP}] )
		shout? ( media-libs/libshout:0[${MULTILIB_USEDEP}] )
		sid? ( media-libs/libsidplay:2[${MULTILIB_USEDEP}] )
		skins? ( x11-libs/libXext:0[${MULTILIB_USEDEP}]
			x11-libs/libXpm:0[${MULTILIB_USEDEP}]
			x11-libs/libXinerama:0[${MULTILIB_USEDEP}] )
		speex? ( media-libs/speex:0[${MULTILIB_USEDEP}] )
		svg? ( >=gnome-base/librsvg-2.9.0:2[${MULTILIB_USEDEP}] )
		swscale? ( virtual/ffmpeg:0[${MULTILIB_USEDEP}] )
		taglib? ( >=media-libs/taglib-1.6.1:0[${MULTILIB_USEDEP}] sys-libs/zlib:0[${MULTILIB_USEDEP}] )
		theora? ( >=media-libs/libtheora-1.0_beta3:0[${MULTILIB_USEDEP}] )
		tremor? ( media-libs/tremor:0[${MULTILIB_USEDEP}] )
		truetype? ( media-libs/freetype:2[${MULTILIB_USEDEP}] virtual/ttf-fonts:0
			!fontconfig? ( media-fonts/dejavu:0 ) )
		twolame? ( media-sound/twolame:0[${MULTILIB_USEDEP}] )
		udev? ( >=virtual/udev-142:0[${MULTILIB_USEDEP}] )
		upnp? ( net-libs/libupnp:0[${MULTILIB_USEDEP}] )
		v4l? ( media-libs/libv4l:0[${MULTILIB_USEDEP}] )
		vaapi? ( x11-libs/libva:0[${MULTILIB_USEDEP}] virtual/ffmpeg[vaapi,${MULTILIB_USEDEP}] )
		vcdx? ( >=dev-libs/libcdio-0.78.2:0[${MULTILIB_USEDEP}] >=media-video/vcdimager-0.7.22:0[${MULTILIB_USEDEP}] )
		vdpau? ( >=x11-libs/libvdpau-0.6:0[${MULTILIB_USEDEP}] !<media-video/libav-10_beta1 )
		vnc? ( >=net-libs/libvncserver-0.9.9:0 )
		vorbis? ( media-libs/libvorbis:0[${MULTILIB_USEDEP}] )
		X? ( x11-libs/libX11:0[${MULTILIB_USEDEP}] )
		x264? ( >=media-libs/x264-0.0.20090923:0=[${MULTILIB_USEDEP}] )
		xcb? ( >=x11-libs/libxcb-1.6:0[${MULTILIB_USEDEP}]
			>=x11-libs/xcb-util-0.3.4:0[${MULTILIB_USEDEP}]
			>=x11-libs/xcb-util-keysyms-0.3.4:0[${MULTILIB_USEDEP}] )
		xml? ( dev-libs/libxml2:2[${MULTILIB_USEDEP}] )
		zvbi? ( >=media-libs/zvbi-0.2.25:0[${MULTILIB_USEDEP}] )
"

DEPEND="${RDEPEND}
	kde? ( >=kde-base/kdelibs-4:4 )
	xcb? ( x11-proto/xproto:0[${MULTILIB_USEDEP}] )
	app-arch/xz-utils:0[${MULTILIB_USEDEP}]
	>=sys-devel/gettext-0.18.3:*[${MULTILIB_USEDEP}]
	virtual/pkgconfig:*
"

REQUIRED_USE="
	aalib? ( X )
	bidi? ( truetype )
	cddb? ( cdda )
	dvb? ( dvbpsi )
	dxva2? ( avcodec )
	egl? ( X )
	ffmpeg? ( avcodec avformat swscale postproc )
	fontconfig? ( truetype )
	gnutls? ( gcrypt )
	httpd? ( lua )
	libcaca? ( X )
	libtar? ( skins )
	libtiger? ( kate )
	qt4? ( X !qt5 )
	qt5? ( X !qt4 )
	sdl? ( X )
	skins? ( truetype X ^^ ( qt4 qt5 ) )
	vaapi? ( avcodec X )
	vlm? ( encode )
	xv? ( xcb )
"

S="${WORKDIR}/${MY_P}"

MULTILIB_PARALLEL_PHASES="src_configure src_compile src_test"

pkg_setup() {
	if [[ "$(tc-getCC)" == *"gcc"* ]] ; then
		if [[ $(gcc-major-version) < 4 || ( $(gcc-major-version) == 4 && $(gcc-minor-version) < 5 ) ]] ; then
			die "You need to have at least >=sys-devel/gcc-4.5 to build and/or have a working vlc, see bug #426754."
		fi
	fi
	if use '!prefix' && use test; then
		if ! has userpriv ${FEATURES}; then
			ewarn
			ewarn "The VLC testsuite cannot run as root and will therefore"
			ewarn "be skipped.  Set FEATURES=userpriv if you want to test"
			ewarn
		fi
	fi
}

src_unpack() {
	if [ "${PV%9999}" != "${PV}" ] ; then
		git-r3_src_unpack
	else
		unpack ${A}
	fi
}

src_prepare() {
	# Support for Qt5.
	if use qt5 ; then
		export UIC="/usr/lib64/qt5/bin/uic"
		export MOC="/usr/lib64/qt5/bin/moc"
	fi

	# Remove unnecessary warnings about unimplemented pragmas on gcc for now.
	# Need to recheck this with gcc 4.9 and every subsequent minor bump of gcc.
	#
	# config.h:792: warning: ignoring #pragma STDC FENV_ACCESS [-Wunknown-pragmas]
	# config.h:793: warning: ignoring #pragma STDC FP_CONTRACT [-Wunknown-pragmas]
	#
	# http://gcc.gnu.org/c99status.html
	if [[ "$(tc-getCC)" == *"gcc"* ]] ; then
		sed -i 's/ifndef __FAST_MATH__/if 0/g' configure.ac || die
	fi

	# _FORTIFY_SOURCE is set to 2 by default on Gentoo, remove redefine warnings.
	sed -i '/_FORTIFY_SOURCE.*, 2,/d' configure.ac || die

	# Bootstrap when we are on a git checkout.
	if [[ "${PV%9999}" != "${PV}" ]] ; then
		./bootstrap
	fi

	# Make it build with libtool 1.5
	rm -f m4/lt* m4/libtool.m4 || die

	# We are not in a real git checkout due to the absence of a .git directory.
	touch src/revision.txt || die

	# Patch up incompatibilities and reconfigure autotools.
	epatch "${FILESDIR}"/${PN}-2.1.0-newer-rdp.patch
	epatch "${FILESDIR}"/${PN}-2.1.0-libva-1.2.1-compat.patch

	# Fix up broken audio when skipping using a fixed reversed bisected commit.
	epatch "${FILESDIR}"/${PN}-2.1.0-TomWij-bisected-PA-broken-underflow.patch

	# questionable out-of-tree build hack for qt missing target directories
	epatch "${FILESDIR}"/${PN}-2.1.2-qt-mkdir-kludge.patch

	# Disable avcodec checks when avcodec is not used.
	sed -i 's/^#if LIBAVCODEC_VERSION_CHECK(.*)$/#if 0/' modules/codec/avcodec/fourcc.c || die

	# Don't use --started-from-file when not using dbus.
	if ! use dbus ; then
		sed -i 's/ --started-from-file//' share/vlc.desktop.in || die
	fi

	eautoreconf

	# Disable automatic running of tests.
	find . -name 'Makefile.in' -exec sed -i 's/\(..*\)check-TESTS/\1/' {} \; || die
}

multilib_src_configure() {
	# Compatibility fix for Samba 4.
	use samba && append-cppflags "-I/usr/include/samba-4.0"

	# Needs libresid-builder from libsidplay:2 which is in another directory...
	# FIXME!
	append-ldflags "-L/usr/$(get_libdir)/sidplay/builders/"

	if use truetype || use projectm ; then
		local dejavu="/usr/share/fonts/dejavu/"
		myconf="--with-default-font=${dejavu}/DejaVuSans.ttf \
				--with-default-font-family=Sans \
				--with-default-monospace-font=${dejavu}/DejaVuSansMono.ttf
				--with-default-monospace-font-family=Monospace"
	fi

	local qt_flag=""
	if use qt4 || use qt5 ; then
		qt_flag="--enable-qt"
	fi

	ECONF_SOURCE="${S}" econf \
		${myconf} \
		--enable-vlc \
		--docdir=/usr/share/doc/${PF} \
		--disable-dependency-tracking \
		--disable-optimizations \
		--disable-update-check \
		--enable-fast-install \
		--enable-screen \
		$(use_enable a52) \
		$(use_enable aalib aa) \
		$(use_enable alsa) \
		$(use_enable altivec) \
		$(use_enable atmo) \
		$(use_enable audioqueue) \
		$(use_enable avahi bonjour) \
		$(use_enable avcodec) \
		$(use_enable avformat) \
		$(use_enable bidi fribidi) \
		$(use_enable bluray) \
		$(use_enable cdda vcd) \
		$(use_enable cddb libcddb) \
		$(use_enable chromaprint) \
		$(use_enable dbus) \
		$(use_enable dirac) \
		$(use_enable directfb) \
		$(use_enable directx) \
		$(use_enable dc1394) \
		$(use_enable debug) \
		$(use_enable dts dca) \
		$(use_enable dvbpsi) \
		$(use_enable dvd dvdread) $(use_enable dvd dvdnav) \
		$(use_enable dxva2) \
		$(use_enable egl) \
		$(use_enable encode sout) \
		$(use_enable faad) \
		$(use_enable fdk fdkaac) \
		$(use_enable flac) \
		$(use_enable fluidsynth) \
		$(use_enable fontconfig) \
		$(use_enable gcrypt libgcrypt) \
		$(use_enable gme) \
		$(use_enable gnome gnomevfs) \
		$(use_enable gnutls) \
		$(use_enable growl) \
		$(use_enable httpd) \
		$(use_enable ieee1394 dv1394) \
		$(use_enable ios-vout) \
		$(use_enable ios-vout ios-vout2) \
		$(use_enable jack) \
		$(use_enable kate) \
		$(multilib_native_use_with kde kde-solid) \
		$(use_enable libass) \
		$(use_enable libcaca caca) \
		$(use_enable libnotify notify) \
		$(use_enable libsamplerate samplerate) \
		$(use_enable libtar) \
		$(use_enable libtiger tiger) \
		$(use_enable linsys) \
		$(use_enable lirc) \
		$(use_enable live live555) \
		$(multilib_native_use_enable lua) \
		$(use_enable macosx-audio) \
		$(use_enable macosx-dialog-provider) \
		$(use_enable macosx-eyetv) \
		$(use_enable macosx-qtkit) \
		$(use_enable macosx-quartztext) \
		$(use_enable macosx-vout) \
		$(use_enable matroska mkv) \
		$(use_enable mmx) \
		$(use_enable modplug mod) \
		$(use_enable mp3 mad) \
		$(use_enable mpeg libmpeg2) \
		$(use_enable mtp) \
		$(use_enable musepack mpc) \
		$(use_enable ncurses) \
		$(use_enable neon) \
		$(use_enable ogg) $(use_enable ogg mux_ogg) \
		$(use_enable omxil) \
		$(use_enable omxil omxil-vout) \
		$(use_enable opencv) \
		$(use_enable opengl glx) \
		$(use_enable opus) \
		$(use_enable optimisememory optimize-memory) \
		$(use_enable png) \
		$(use_enable postproc) \
		$(use_enable projectm) \
		$(use_enable pulseaudio pulse) \
		${qt_flag} \
		$(multilib_native_use_enable rdp libfreerdp) \
		$(use_enable rtsp realrtsp) \
		$(use_enable run-as-root) \
		$(use_enable samba smbclient) \
		$(use_enable schroedinger) \
		$(use_enable sdl) \
		$(use_enable sdl-image) \
		$(use_enable sid) \
		$(use_enable sftp) \
		$(use_enable shout) \
		$(use_enable skins skins2) \
		$(use_enable speex) \
		$(use_enable sse) \
		$(use_enable svg) \
		$(use_enable swscale) \
		$(use_enable taglib) \
		$(use_enable theora) \
		$(use_enable tremor) \
		$(use_enable truetype freetype) \
		$(use_enable twolame) \
		$(use_enable udev) \
		$(use_enable upnp) \
		$(use_enable v4l v4l2) \
		$(use_enable vaapi libva) \
		$(use_enable vcdx) \
		$(use_enable vdpau) \
		$(use_enable vlm) \
		$(use_enable vnc libvnc) \
		$(use_enable vorbis) \
		$(use_enable wma-fixed) \
		$(use_with X x) \
		$(use_enable x264) \
		$(use_enable xcb) \
		$(use_enable xml libxml2) \
		$(use_enable xv xvideo) \
		$(use_enable zvbi) $(use_enable !zvbi telx) \
		--disable-coverage \
		--disable-cprof \
		--disable-crystalhd \
		--disable-decklink \
		--disable-gles1 \
		--disable-gles2 \
		--disable-goom \
		--disable-ios-audio \
		--disable-kai \
		--disable-kva \
		--disable-maintainer-mode \
		--disable-merge-ffmpeg \
		--disable-opensles \
		--disable-oss \
		--disable-quicksync \
		--disable-quicktime \
		--disable-rpi-omxil \
		--disable-shine \
		--disable-sndio \
		--disable-vda \
		--disable-vsxu \
		--disable-wasapi

		# ^ We don't have these disabled libraries in the Portage tree yet.
}

multilib_src_test() {
	if use test; then
		if (( $(id -u) )) ; then
			Xemake check-TESTS PACKAGE_STRING="${P} (${ABI})"
			cd test
			Xmake check-TESTS
		else
			einfo
			einfo "Skipping VLC test-suite because we are root"
			einfo
		fi
	fi
}

DOCS="AUTHORS THANKS NEWS README doc/fortunes.txt doc/intf-vcd.txt"

multilib_src_install_all() {
	dodoc ${DOCS}
	# Punt useless libtool's .la files
	find "${D}" -name '*.la' -delete
}

abi_pkg_postinst() {
	if [ "$ROOT" = "/" ] && [ -x "/usr/$(get_libdir)/vlc/vlc-cache-gen" ] ; then
		einfo "Running /usr/$(get_libdir)/vlc/vlc-cache-gen on /usr/$(get_libdir)/vlc/plugins/"
		"/usr/$(get_libdir)/vlc/vlc-cache-gen" -f "/usr/$(get_libdir)/vlc/plugins/"
	else
		ewarn "We cannot run vlc-cache-gen (most likely ROOT!=/)"
		ewarn "Please run /usr/$(get_libdir)/vlc/vlc-cache-gen manually"
		ewarn "If you do not do it, vlc will take a long time to load."
	fi
}

pkg_postinst() {
	multilib_foreach_abi abi_pkg_postinst
}
