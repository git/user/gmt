# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"
GCONF_DEBUG="no"

inherit bash-completion-r1 virtualx gnome2-multilib

DESCRIPTION="Simple low-level configuration system"
HOMEPAGE="https://wiki.gnome.org/dconf"

LICENSE="LGPL-2.1+"
SLOT="0"

# TODO: coverage ?
IUSE="test +X"

KEYWORDS="~amd64"

RDEPEND="
	>=dev-libs/glib-2.39.1:2[${MULTILIB_USEDEP}]
	sys-apps/dbus[${MULTILIB_USEDEP}]
	X? (
		>=dev-libs/libxml2-2.7.7:2[${MULTILIB_USEDEP}]
		>=x11-libs/gtk+-3.4:3[${MULTILIB_USEDEP}] )
"
DEPEND="${RDEPEND}
	dev-libs/libxslt[${MULTILIB_USEDEP}]
	dev-util/gdbus-codegen
	>=dev-util/gtk-doc-am-1.15
	>=dev-util/intltool-0.50
	sys-devel/gettext[${MULTILIB_USEDEP}]
	virtual/pkgconfig
"

src_configure() {
	gnome2-multilib_src_configure \
		--disable-gcov \
		--enable-man \
		$(use_enable X editor) \
		VALAC=$(type -P true)
}

ehook gnome2-multilib-per-abi_pre_src_test my_abi_pre_test
my_abi_pre_test() {
	Xemake check
	return 1
}

src_install() {
	gnome2-multilib_src_install

	# GSettings backend may be one of: memory, gconf, dconf
	# Only dconf is really considered functional by upstream
	# must have it enabled over gconf if both are installed
	echo 'CONFIG_PROTECT_MASK="/etc/dconf"' >> 51dconf
	echo 'GSETTINGS_BACKEND="dconf"' >> 51dconf
	doenvd 51dconf

	# Install bash-completion file properly to the system
	rm -rv "${ED}usr/share/bash-completion" || die
	dobashcomp "${S}/bin/completion/dconf"
}

pkg_postinst() {
	gnome2-multilib_pkg_postinst
	# Kill existing dconf-service processes as recommended by upstream due to
	# possible changes in the dconf private dbus API.
	# dconf-service will be dbus-activated on next use.
	pids=$(pgrep -x dconf-service)
	if [[ $? == 0 ]]; then
		ebegin "Stopping dconf-service; it will automatically restart on demand"
		kill ${pids}
		eend $?
	fi
}
