# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"
GCONF_DEBUG="no"

inherit gnome2-multilib

DESCRIPTION="Collection of GSettings schemas for GNOME desktop"
HOMEPAGE="https://git.gnome.org/browse/gsettings-desktop-schemas"

LICENSE="LGPL-2.1+"
SLOT="0"
IUSE="+introspection"
KEYWORDS="~amd64"

RDEPEND="
	>=dev-libs/glib-2.31:2[${MULTILIB_USEDEP}]
	introspection? ( >=dev-libs/gobject-introspection-1.31.0[${MULTILIB_USEDEP}] )
	!<gnome-base/gdm-3.8
"
DEPEND="${RDEPEND}
	>=dev-util/intltool-0.40
	sys-devel/gettext[${MULTILIB_USEDEP}]
	virtual/pkgconfig
"

src_configure() {
	DOCS="AUTHORS HACKING NEWS README"
	gnome2-multilib_src_configure $(use_enable introspection)
}
