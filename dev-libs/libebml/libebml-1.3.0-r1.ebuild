# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit eutils multilib toolchain-funcs multilib-minimal

DESCRIPTION="Extensible binary format library (kinda like XML)"
HOMEPAGE="http://www.matroska.org/ https://github.com/Matroska-Org/libebml/"
SRC_URI="https://github.com/Matroska-Org/${PN}/archive/release-${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0/4" # subslot = soname major version
KEYWORDS="~amd64"
IUSE="debug static-libs"

S=${WORKDIR}/${PN}-release-${PV}

src_prepare() {
	cd make/linux
	epatch "${FILESDIR}"/${P}-makefile.patch

	sed -i "s:\(DEBUGFLAGS=\)-g :\1:" Makefile || die
	multilib_copy_sources
}

multilib_src_compile() {
	cd make/linux
	local targets
	if [[ ${CHOST} != *-darwin* ]] ; then
		targets="sharedlib"
	else
		targets="macholib"
	fi
	use static-libs && targets+=" staticlib"

	# keep the prefix in here to make sure the binary is built with a correct
	# install_name on Darwin
	emake \
		prefix="${EPREFIX}"/usr \
		AR="${AR:-$(tc-getAR)}" \
		CC="${CC:-$(tc-getCC)}" \
		CXX="${CXX:-$(tc-getCXX)}" \
		$(use debug && echo DEBUG=yes || echo DEBUG=no) \
		${targets}
}

multilib_src_install() {
	cd make/linux
	local targets="install_headers"
	if [[ ${CHOST} != *-darwin* ]] ; then
		targets+=" install_sharedlib"
	else
		targets+=" install_macholib"
	fi
	use static-libs && targets+=" install_staticlib"

	emake \
		DESTDIR="${D}" \
		prefix="${EPREFIX}"/usr \
		libdir="${EPREFIX}"/usr/$(get_libdir) \
		${targets}
}

multilib_src_install_all() {
	dodoc ChangeLog
}
