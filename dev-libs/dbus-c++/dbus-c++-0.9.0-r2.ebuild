# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"

inherit eutils multilib-minimal

DESCRIPTION="provide a C++ API for D-BUS"
HOMEPAGE="http://sourceforge.net/projects/dbus-cplusplus/ http://sourceforge.net/apps/mediawiki/dbus-cplusplus/index.php?title=Main_Page"
SRC_URI="mirror://sourceforge/dbus-cplusplus/lib${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE="doc ecore glib static-libs test"

RDEPEND="sys-apps/dbus[${MULTILIB_USEDEP}]
	ecore? ( dev-libs/ecore[${MULTILIB_USEDEP}] )
	glib? ( dev-libs/glib[${MULTILIB_USEDEP}] )"
DEPEND="${RDEPEND}
	doc? ( app-doc/doxygen )
	dev-util/cppunit
	virtual/pkgconfig"

S=${WORKDIR}/lib${P}

MULTILIB_PARALLEL_PHASES="src_configure src_compile"

src_prepare() {
	epatch "${FILESDIR}"/${P}-gcc-4.7.patch #424707
	multilib_copy_sources
}

multilib_src_configure() {
	econf \
		--disable-examples \
		$(use_enable doc doxygen-docs) \
		$(use_enable ecore) \
		$(use_enable glib) \
		$(use_enable static-libs static) \
		$(use_enable test tests)
}

multilib_src_install() {
	default
	prune_libtool_files
}
