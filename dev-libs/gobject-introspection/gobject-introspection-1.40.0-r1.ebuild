# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"
GCONF_DEBUG="no"
PYTHON_COMPAT=( python2_7 )
PYTHON_REQ_USE="xml"

inherit python-single-r1 gtk-doc toolchain-funcs gnome2-multilib

DESCRIPTION="Introspection infrastructure for generating gobject library bindings for various languages"
HOMEPAGE="http://live.gnome.org/GObjectIntrospection/"

LICENSE="LGPL-2+ GPL-2+"
SLOT="0"
IUSE="cairo doctool test"
REQUIRED_USE="
	${PYTHON_REQUIRED_USE}
	test? ( cairo )
"
KEYWORDS="~amd64"

RDEPEND="
	>=dev-libs/gobject-introspection-common-${PV}
	>=dev-libs/glib-2.36:2[${MULTILIB_USEDEP}]
	doctool? ( dev-python/mako )
	virtual/libffi:=[${MULTILIB_USEDEP}]
	!<dev-lang/vala-0.20.0
	${PYTHON_DEPS}
"
# Wants real bison, not virtual/yacc
DEPEND="${RDEPEND}
	>=dev-util/gtk-doc-am-1.19
	sys-devel/bison
	sys-devel/flex
	virtual/pkgconfig
"
# PDEPEND to avoid circular dependencies, bug #391213
PDEPEND="cairo? ( x11-libs/cairo[glib,${MULTILIB_USEDEP}] )"

pkg_setup() {
	python-single-r1_pkg_setup
}

src_configure() {
	if ! has_version "x11-libs/cairo[glib,${MULTILIB_USEDEP}]"; then
		# Bug #391213: enable cairo-gobject support even if it's not installed
		# We only PDEPEND on cairo to avoid circular dependencies
		export CAIRO_LIBS="-lcairo -lcairo-gobject"
		export CAIRO_CFLAGS="-I${EPREFIX}/usr/include/cairo"
	fi

	# To prevent crosscompiling problems, bug #414105
	gnome2-multilib_src_configure \
		--disable-static \
		YACC=$(type -p yacc) \
		$(use_with cairo) \
		$(use_enable doctool)
}

ehook gnome2-multilib-per-abi-pre_src_configure abi_src_configure
abi_src_configure() {
	G2CONF+=( "CC=${CC:-$(tc-getCC)}" )
}

###############################################################
# DANGER WILL ROBINSON!!!  OBSCENELY INELEGANT HACK AHEAD
#
# this is tricky.  the scanner needs to build for the native
# abi, only, or else all hell breaks loose (python trouble).
# So, we very gingerly pre-build those executables, and
# unceremoneously wedge them into the non-native-abi build-trees,
# right where they would have gone, had we allowed the makefile
# to generate them (if it was able to, which it isn't).
#

_goi_giscanner_build_native() {
	pushd "${BUILD_DIR}" >/dev/null || die
	emake g-ir-scanner
	[[ -x g-ir-scanner ]] || die "Dude, where's my scanner?"
	local stuff
	# FIXME: use platform independent suffix for library here
	find . \( -name '*.so' -o -name '*.la' -o -name '*.o' -o -name 'g-ir-scanner' -o -name '*.lo' -o -name '*.lai' \) \
		-print | sed "s:^:$(pwd) :" > "${T}"/its_for_scanning_things_ldo
	popd > /dev/null || die
}

_goi_giscanner_inject() {
	pushd "${BUILD_DIR}" > /dev/null || die
	einfo "injecting scanner (darkly) from \"${DEFAULT_ABI}\" build-tree into that of \"${ABI}\""
	[[ -f "${T}"/its_for_scanning_things_ldo ]] || die
	local d f dn
	cat "${T}"/its_for_scanning_things_ldo | while read d f ; do
		[[ -d "${d}" ]] || die "bad source \"${d}\""
		[[ -f "${d}/${f}" ]] || die "bad file \"${d}/${f}\""
		dn=$(dirname "${f}")
		[[ ${dn} == . ]] || mkdir -p "${dn}"
		# hard link
		ln -P ${d}/${f} ./${f} || die
	done
	popd > /dev/null || die
}

#
# n.b.: I'm aware that the above was pretty damn evil.
# By all means, let's find some better way to do it!
# It is likely that some vastly better way exists.  In
# the meanwhile, the above seems to work.
# -gmt
###############################################################

_if_is_native_abi() {
	if multilib_is_native_abi; then
		"$@"
	else
		return 0
	fi
}

_if_aint_native_abi() {
	if multilib_is_native_abi; then
		return 0
	else
		"$@"
	fi
}

_for_native_abi_only() {
	multilib_parallel_foreach_abi _if_is_native_abi "$@"
}

_for_nonnative_abis() {
	multilib_parallel_foreach_abi _if_aint_native_abi "$@"
}

src_compile() {
	_for_native_abi_only _goi_giscanner_build_native
	_for_nonnative_abis _goi_giscanner_inject

	gnome2-multilib_src_compile
}

src_test() {
	multilib_foreach_abi run_in_build_dir abi_src_test
}

abi_src_test() {
	# there seems to be an oot-build bug for tests/warn:
	local mysrcdir="${S}"/tests/warn
	local mydestdir="${BUILD_DIR}"/tests/warn
	ebegin "OOT bug workaround for abi ${ABI}: copying *.h from \"${mysrcdir}\" into \"${mydestdir}\"..."
	for f in "${mysrcdir}"/*.h ; do
		[[ -f "${f}" ]] && { cp -f "${f}" "${mydestdir}" || die ; }
	done
	eend

	emake check \
		&& einfo "ABI \"${ABI}\" ran the test suite without incident" \
		|| { eerror "at least one test for ABI \"${ABI}\" failed" ; die ; }
}

src_install() {
	gnome2-multilib_src_install

	dodoc AUTHORS CONTRIBUTORS ChangeLog NEWS README TODO
	# Prevent collision with gobject-introspection-common
	rm -v "${ED}"usr/share/aclocal/introspection.m4 \
		"${ED}"usr/share/gobject-introspection-1.0/Makefile.introspection || die
	rmdir "${ED}"usr/share/aclocal || die


	# prevent installation of 64-bit giscanner hacks
	_for_nonnative_abis _zap_scanner_files
}

_zap_scanner_files() {
	einfo "zapping scanner files for abi ${ABI}"
	rm -rf "${ED}"usr/lib32/gobject-introspection/giscanner || die "failed to zap scanner files"
}
