# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"

inherit eutils gnome.org flag-o-matic multilib-minimal

DESCRIPTION="Typesafe callback system for standard C++"
HOMEPAGE="http://libsigc.sourceforge.net/"

LICENSE="LGPL-2.1"
SLOT="2"
KEYWORDS="~amd64"
IUSE="doc static-libs test"

RDEPEND=""
DEPEND="sys-devel/m4"

# Needs mm-common for eautoreconf
src_prepare() {
	# don't waste time building examples
	sed -i 's|^\(SUBDIRS =.*\)examples\(.*\)$|\1\2|' \
		Makefile.am Makefile.in || die "sed examples failed"

	# don't waste time building tests unless USE=test
	if ! use test ; then
		sed -i 's|^\(SUBDIRS =.*\)tests\(.*\)$|\1\2|' \
			Makefile.am Makefile.in || die "sed tests failed"
	fi
}

multilib_src_configure() {
	filter-flags -fno-exceptions

	ECONF_SOURCE="${S}" econf \
		$(use_enable doc documentation) \
		$(use_enable static-libs static)
}

multilib_src_install_all() {
	DOCS="AUTHORS ChangeLog README NEWS TODO"
	einstalldocs

	if ! use static-libs ; then
		prune_libtool_files
	fi

	if use doc ; then
		dohtml -r docs/reference/html/* docs/images/*
		insinto /usr/share/doc/${PF}
		doins -r examples
	fi
}
