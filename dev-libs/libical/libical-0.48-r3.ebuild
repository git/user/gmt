# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
inherit eutils multilib-minimal

DESCRIPTION="An implementation of basic iCAL protocols from citadel, previously known as aurore"
HOMEPAGE="http://freeassociation.sourceforge.net"
#SRC_URI="mirror://sourceforge/freeassociation/files/${PN}/${P}/${P}.tar.gz"
SRC_URI="mirror://sourceforge/freeassociation/${PN}/${P}/${P}.tar.gz"

LICENSE="|| ( MPL-1.1 LGPL-2 )"
SLOT="0/0"
KEYWORDS="~amd64"
IUSE="examples static-libs"

# https://sourceforge.net/tracker2/index.php?func=detail&aid=2196790&group_id=16077&atid=116077
# Upstream states that tests are supposed to fail (I hope sf updates archives
# and answer became visible):
# http://sourceforge.net/mailarchive/forum.php?thread_name=1257441040.20584.3431.camel%40tablet&forum_name=freeassociation-devel
RESTRICT="test"

DEPEND="dev-lang/perl"

MULTILIB_WRAPPED_HEADERS=(
	/usr/include/libical/icalderivedvalue.h
	/usr/include/libical/ical.h
)

src_prepare() {
	# Do not waste time building examples
	sed -i -e 's/^\(SUBDIRS =.*\)examples\(.*\)$/\1\2/' Makefile.{am,in} || die
	# If errors are fatal, some software can segfault
	sed -i \
		-e 's/^#define ICAL_ERRORS_ARE_FATAL 0/#undef ICAL_ERRORS_ARE_FATAL/' \
		configure || die
}

multilib_src_configure() {
	ECONF_SOURCE="${S}" econf \
		$(use_enable static-libs static) \
		--disable-icalerrors-are-fatal
}

DOCS=(
	AUTHORS ChangeLog NEWS README TEST THANKS TODO
	doc/{AddingOrModifyingComponents,UsingLibical}.txt
)

multilib_src_install_all() {
	einstalldocs

	prune_libtool_files

	if use examples; then
		rm examples/Makefile* examples/CMakeLists.txt
		insinto /usr/share/doc/${PF}/examples
		doins examples/*
	fi
}
