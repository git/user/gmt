# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
inherit cmake-multilib

DESCRIPTION="An implementation of basic iCAL protocols from citadel, previously known as aurore"
HOMEPAGE="http://freeassociation.sourceforge.net"
SRC_URI="mirror://sourceforge/freeassociation/${PN}/${P}/${P}.tar.gz"

LICENSE="|| ( MPL-1.1 LGPL-2 )"
SLOT="0/1"
KEYWORDS="~amd64"
IUSE="doc examples introspection static-libs"

RDEPEND="introspection? ( dev-libs/gobject-introspection )"
DEPEND="${RDEPEND}
	dev-lang/perl"

src_configure() {
	local mycmakeargs=" $( cmake-utils_use introspection GOBJECT_INTROSPECTION )"
	cmake-multilib_src_configure
}

DOCS=(
	AUTHORS ChangeLog NEWS README TEST THANKS TODO
	doc/{AddingOrModifyingComponents,UsingLibical}.txt
)

MULTILIB_WRAPPED_HEADERS=(
	/usr/include/libical/icalderivedvalue.h
	/usr/include/libical/ical.h
)

src_install() {
	cmake-multilib_src_install

	# Remove static libs, cmake-flag is a trap.
	use static-libs || find "${ED}" -name '*.a' -delete

	if use examples; then
		rm examples/Makefile* examples/CMakeLists.txt
		insinto /usr/share/doc/${PF}/examples
		doins examples/*
	fi
}
