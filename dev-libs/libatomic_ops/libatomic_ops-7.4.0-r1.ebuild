# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit eutils autotools-multilib

DESCRIPTION="Implementation for atomic memory update operations"
HOMEPAGE="http://www.hpl.hp.com/research/linux/atomic_ops/"
SRC_URI="http://www.hpl.hp.com/research/linux/atomic_ops/download/${P}.tar.gz"

LICENSE="MIT boehm-gc GPL-2+"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

AUTOTOOLS_AUTORECONF=yes
AUTOTOOLS_PRUNE_LIBTOOL_FILES=none

src_prepare() {
	epatch "${FILESDIR}"/${P}-docs.patch
	autotools-multilib_src_prepare
}

src_configure() {
	autotools-multilib_src_configure --docdir="${EPREFIX}"/usr/share/doc/${PF}
}
