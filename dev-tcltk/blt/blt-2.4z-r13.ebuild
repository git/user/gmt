# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit autotools-multilib eutils flag-o-matic multilib toolchain-funcs

MY_V_SUFFIX="-8.5.2"

DESCRIPTION="Extension to Tk, adding new widgets, geometry managers, and misc commands"
HOMEPAGE="
	http://blt.sourceforge.net/
	http://jos.decoster.googlepages.com/bltfortk8.5.2"
SRC_URI="
	http://dev.gentoo.org/~jlec/distfiles/${PN}${PV}${MY_V_SUFFIX}.tar.gz
	http://jos.decoster.googlepages.com/${PN}${PV}${MY_V_SUFFIX}.tar.gz"

IUSE="jpeg static-libs X"
SLOT="0"
LICENSE="BSD"
KEYWORDS="~amd64"

DEPEND="
	dev-lang/tk[${MULTILIB_USEDEP}]
	jpeg? ( virtual/jpeg[${MULTILIB_USEDEP}] )
	X? ( x11-libs/libX11[${MULTILIB_USEDEP}] )"
RDEPEND="${DEPEND}"

S="${WORKDIR}/${PN}${PV}${MY_V_SUFFIX}"

AUTOTOOLS_AUTORECONF=yes
AUTOTOOLS_NO_AUTO_STATIC_LIBS_CONFARGS=yes
MULTILIB_WRAPPED_HEADERS=( usr/include/bltHash.h )
MULTILIB_PARALLEL_PHASES="src_configure src_compile"

src_prepare() {
	epatch "${FILESDIR}/blt-2.4z-r13-fix-makefile.patch"
	epatch "${FILESDIR}/blt-2.4z-r4-fix-makefile2.patch"
	# From blt-2.4z-6mdk.src.rpm
	epatch "${FILESDIR}"/blt2.4z-64bit.patch

	#epatch "${FILESDIR}"/blt-2.4z-tcl8.5-fix.patch
	epatch "${FILESDIR}"/blt-2.4z-tcl8.5-fixpkgruntime.patch

	epatch "${FILESDIR}"/${P}-ldflags-v2.patch

	# drop RPATH
	sed \
		-e 's:LD_RUN_PATH=.*$:LD_RUN_PATH="":g' \
		-i configure.in || die "sed configure.in failed"

	epatch \
		"${FILESDIR}"/${P}-linking.patch \
		"${FILESDIR}"/${P}-darwin.patch \
		"${FILESDIR}"/${P}-gbsd.patch \
		"${FILESDIR}"/${P}-tk8.6.patch \
		"${FILESDIR}"/${P}-tcl8.6.patch \
		"${FILESDIR}"/${P}-aclocal.patch \
		"${FILESDIR}"/${P}-deprecated-ac-output.patch \
		"${FILESDIR}"/${P}-gentoo-libdir.patch \
		"${FILESDIR}"/${P}-ar-withval.patch \
		"${FILESDIR}"/${P}-parallelmake.patch \
		"${FILESDIR}"/${P}-mkdir-safely.patch \
		"${FILESDIR}"/${P}-TreeViewTextbox_prototype.patch

	append-cflags -fPIC

	rm configure || die
	autotools-multilib_src_prepare
}

src_configure() {
	local RANLIB
	LC_ALL=C \
	autotools-multilib_src_configure \
		--x-includes="${EPREFIX}/usr/include" \
		--x-libraries="${EPREFIX}/usr/@GET_LIBDIR@" \
		--with-blt="${EPREFIX}/usr/@GET_LIBDIR@" \
		--with-tcl="${EPREFIX}/usr/@GET_LIBDIR@" \
		--with-tk="${EPREFIX}/usr/@GET_LIBDIR@" \
		--with-tclincls="${EPREFIX}/usr/include" \
		--with-tkincls="${EPREFIX}/usr/include" \
		--with-tcllibs="${EPREFIX}/usr/@GET_LIBDIR@" \
		--with-tklibs="${EPREFIX}/usr/@GET_LIBDIR@" \
		--with-gentoo-libdir="@GET_LIBDIR@" \
		--with-gnu-ld \
		$(use_enable jpeg) \
		$(use_with X x)
}

ehook autotools-utils-multilib-per-abi-pre_src_configure abi_pre_src_configure
abi_pre_src_configure() {
	myeconfargs+=(
		--with-cc="${CC:-$(tc-getCC)}"
		--with-ar="$(tc-getAR)"
		--with-cflags="${CFLAGS}"
	)
	tc-export RANLIB
	return 0
}

src_install() {
	sed \
		-e "s:\.\./src/bltwish:${EPREFIX}/usr/bin/bltwish:g" \
		-e "s:\.\./bltwish:${EPREFIX}/usr/bin/bltwish:g" \
		-e "s:/usr/local/bin/bltwish:${EPREFIX}/usr/bin/bltwish:g" \
		-e "s:/usr/local/bin/tclsh:${EPREFIX}/usr/bin/tclsh:g" \
		-i demos/{,scripts/}*.tcl || die

	dodir /usr/bin \
		/usr/share/man/mann \
		/usr/include

	autotools-multilib_src_install INSTALL_ROOT="${D}"

	dodoc NEWS PROBLEMS README
	dohtml html/*.html
	for f in `ls "${ED}"usr/share/man/mann` ; do
		mv "${ED}"usr/share/man/mann/${f} "${ED}"usr/share/man/mann/${f/.n/.nblt}
	done

	use static-libs || \
		find "${ED}"usr -name "*.a" -print0 | \
		xargs -r -0 rm -fv
}

ehook autotools-multilib-per-abi-pre_src_install abi_pre_src_install
abi_pre_src_install() {
	dodir /usr/$(get_libdir)/blt2.4/demos/bitmaps
	return 0
}

ehook autotools-multilib-per-abi-post_src_install abi_post_src_install
abi_post_src_install() {
	# fix for linking against shared lib with -lBLT or -lBLTlite
	dosym libBLT24$(get_libname) /usr/$(get_libdir)/libBLT$(get_libname)
	dosym libBLTlite24$(get_libname) /usr/$(get_libdir)/libBLTlite$(get_libname)

	# drop doc files from libdir
	rm -f ${ED}usr/$(get_libdir)/blt2.4/{NEWS,PROBLEMS,README} || die

	# remove demos from libdirs & install as doc
	if multilib_is_native_abi ; then
		docompress -x /usr/share/doc/${PF}/demos
		dodoc -r "${ED}"usr/$(get_libdir)/blt2.4/demos
	fi
	rm -r "${ED}"usr/$(get_libdir)/blt2.4/demos || die
}
