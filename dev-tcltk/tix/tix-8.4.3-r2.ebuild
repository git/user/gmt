# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit eutils multilib-minimal

MY_P="Tix${PV}"
DESCRIPTION="A widget library for Tcl/Tk"
HOMEPAGE="http://tix.sourceforge.net/"
SRC_URI="mirror://sourceforge/tix/${MY_P}-src.tar.gz"

IUSE=""
LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64"

RESTRICT="test"

DEPEND="
	dev-lang/tk[${MULTILIB_USEDEP}]
	x11-libs/libX11[${MULTILIB_USEDEP}]
	x11-libs/libXau[${MULTILIB_USEDEP}]
	x11-libs/libXdmcp[${MULTILIB_USEDEP}]"
RDEPEND="${DEPEND}"

S="${WORKDIR}/${MY_P}"

src_prepare() {
	(use x86-macos || use x64-macos) || epatch "${FILESDIR}"/${P}-link.patch
	sed \
		-e 's:-Os::g' \
		-i configure tclconfig/tcl.m4 || die
	epatch \
		"${FILESDIR}"/${P}-tcl8.5.patch \
		"${FILESDIR}"/${P}-tcl8.6.patch
}

multilib_src_configure() {
	ECONF_SOURCE="${S}" econf \
		--with-tcl="${EPREFIX}/usr/$(get_libdir)" \
		--with-tk="${EPREFIX}/usr/$(get_libdir)"
}

multilib_src_install_all() {
	# Bug 168897
	doheader generic/tix.h
	# Bug 201138
	if use x86-macos || use x64-macos; then
		mv "${ED}"/usr/$(get_libdir)/${MY_P}/libTix{,.}${PV}.dylib
		dosym ${MY_P}/libTix.${PV}.dylib /usr/$(get_libdir)/libTix.${PV}.dylib
	else
		dosym ${MY_P}/lib${MY_P}.so /usr/$(get_libdir)/lib${MY_P}.so
	fi

	dodoc ChangeLog README.txt docs/*.txt
	dohtml -r index.html ABOUT.html docs/
}
