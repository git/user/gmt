# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
PYTHON_COMPAT=( python{2_{6,7},3_{2,3}} pypy2_0 )

inherit distutils-r1

DESCRIPTION="A helper module for working with mimetypes"
HOMEPAGE="https://github.com/FriendCode/mimer.git"
SRC_URI="https://github.com/FriendCode/${PN}/archive/a812e5f6.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

S="${WORKDIR}/mimer-a812e5f631b9b5c969df5a2ea84b635490a96ced"

DEPEND="dev-python/setuptools[${PYTHON_USEDEP}]"
RDEPEND="${DEPEND}"
