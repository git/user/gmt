# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

PYTHON_COMPAT=( python2_7 )

inherit distutils-r1

gitrev="b3d448c83f0ea34cb8dfd5b6d6fb925889e93abd"

DESCRIPTION="A high level pure python git implementation"
HOMEPAGE="https://github.com/FriendCode/gittle"
SRC_URI="https://github.com/FriendCode/${PN}/archive/${gitrev}.tar.gz"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64"
IUSE="ssh"

DEPEND="dev-python/pycrypto[${PYTHON_USEDEP}]
		dev-python/dulwich[${PYTHON_USEDEP}]
		dev-python/funky[${PYTHON_USEDEP}]
		dev-python/mimer[${PYTHON_USEDEP}]
		ssh? ( dev-python/paramiko[${PYTHON_USEDEP}] )
		!dev-python/gittle-gmt"
RDEPEND="${DEPEND}"

S="${WORKDIR}"/gittle-${gitrev}
