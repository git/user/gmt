# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

PYTHON_COMPAT=( python2_7 )

EGIT_REPO_URI="https://github.com/gmt/gittle.git"

inherit distutils-r1 git-r3

DESCRIPTION="A high level pure python git implementation"
HOMEPAGE="https://github.com/FriendCode/gittle"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64"
IUSE="ssh"

DEPEND="dev-python/pycrypto[${PYTHON_USEDEP}]
		dev-python/dulwich[${PYTHON_USEDEP}]
		dev-python/funky[${PYTHON_USEDEP}]
		dev-python/mimer[${PYTHON_USEDEP}]
		ssh? ( dev-python/paramiko[${PYTHON_USEDEP}] )
		!dev-python/gittle"
RDEPEND="${DEPEND}"
