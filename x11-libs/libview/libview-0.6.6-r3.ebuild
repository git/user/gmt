# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit autotools eutils gnome2-multilib

DESCRIPTION="VMware's Incredibly Exciting Widgets"
HOMEPAGE="http://view.sourceforge.net"
SRC_URI="mirror://sourceforge/view/${P}.tar.bz2"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
IUSE="static-libs"

RDEPEND=">=x11-libs/gtk+-2.4.0:2[${MULTILIB_USEDEP}]
		 dev-cpp/gtkmm:2.4[${MULTILIB_USEDEP}]"
DEPEND="${RDEPEND}
	virtual/pkgconfig"

G2CONF=(--enable-deprecated)

src_unpack() {
	gnome2-multilib_src_unpack
}

src_prepare() {
	# Fix the pkgconfig file
	epatch "${FILESDIR}"/${PN}-0.5.6-pcfix.patch
	eautoreconf
	gnome2-multilib_src_prepare
}
src_configure() {
	gnome2-multilib_src_configure \
		--enable-deprecated \
		$(use_enable static-libs static)
}

src_install() {
	gnome2-multilib_src_install
	dodoc AUTHORS ChangeLog NEWS

	find "${ED}" -name '*.la' -delete
}
