# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"
CLUTTER_LA_PUNT="yes"

# Inherit gnome2 after clutter to download sources from gnome.org
inherit gtk-doc virtualx clutter-multilib gnome2-multilib

DESCRIPTION="A library for using 3D graphics hardware to draw pretty pictures"
HOMEPAGE="http://www.clutter-project.org/"

LICENSE="MIT BSD"
SLOT="1.0/20" # subslot = .so version
# doc and profile disable for now due bugs #484750 and #483332
IUSE="examples gles2 gstreamer +introspection +opengl +pango test" # doc profile
KEYWORDS="~amd64"

COMMON_DEPEND="
	>=dev-libs/glib-2.32:2[${MULTILIB_USEDEP}]
	x11-libs/cairo:=[${MULTILIB_USEDEP}]
	>=x11-libs/gdk-pixbuf-2:2[${MULTILIB_USEDEP}]
	x11-libs/libdrm:=[${MULTILIB_USEDEP}]
	x11-libs/libX11[${MULTILIB_USEDEP}]
	>=x11-libs/libXcomposite-0.4[${MULTILIB_USEDEP}]
	x11-libs/libXdamage[${MULTILIB_USEDEP}]
	x11-libs/libXext[${MULTILIB_USEDEP}]
	>=x11-libs/libXfixes-3[${MULTILIB_USEDEP}]
	>=x11-libs/libXrandr-1.2[${MULTILIB_USEDEP}]
	virtual/opengl[${MULTILIB_USEDEP}]
	gles2? ( media-libs/mesa[gles2,${MULTILIB_USEDEP}] )
	gstreamer? (
		media-libs/gstreamer:1.0[${MULTILIB_USEDEP}]
		media-libs/gst-plugins-base:1.0[${MULTILIB_USEDEP}] )

	introspection? ( >=dev-libs/gobject-introspection-1.34.2[${MULTILIB_USEDEP}] )
	pango? ( >=x11-libs/pango-1.20.0[introspection?,${MULTILIB_USEDEP}] )
"
# before clutter-1.7, cogl was part of clutter
RDEPEND="${COMMON_DEPEND}
	!<media-libs/clutter-1.7[${MULTILIB_USEDEP}]"
DEPEND="${COMMON_DEPEND}
	>=dev-util/gtk-doc-am-1.13
	sys-devel/gettext[${MULTILIB_USEDEP}]
	virtual/pkgconfig
	test? (
		app-admin/eselect-opengl
		media-libs/mesa[classic,${MULTILIB_USEDEP}] )
"
# Need classic mesa swrast for tests, llvmpipe causes a test failure
# For some reason GL3 conformance tests all fail again...
RESTRICT="test"

DOCS=(NEWS)
EXAMPLES="examples/{*.c,*.jpg}"

src_prepare() {
	# Do not build examples
	sed -e "s/^\(SUBDIRS +=.*\)examples\(.*\)$/\1\2/" \
		-i Makefile.am Makefile.in || die

	if ! use test ; then
		# For some reason the configure switch will not completely disable
		# tests being built
		sed -e "s/^\(SUBDIRS =.*\)test-fixtures\(.*\)$/\1\2/" \
    		-e "s/^\(SUBDIRS +=.*\)tests\(.*\)$/\1\2/" \
    		-e "s/^\(.*am__append.* \)tests\(.*\)$/\1\2/" \
			-i Makefile.am Makefile.in || die
	fi

	epatch "${FILESDIR}"/${PN}-1.18.0-cogl-h-trouble.patch

	gnome2-multilib_src_prepare
}

src_configure() {
	# TODO: think about kms-egl, quartz, sdl, wayland
	# Prefer gl over gles2 if both are selected
	# Profiling needs uprof, which is not available in portage yet, bug #484750
	# FIXME: Doesn't provide prebuilt docs, but they can neither be rebuilt, bug #483332
	gnome2-multilib_src_configure  \
		--disable-examples-install \
		--disable-maintainer-flags \
		--enable-cairo             \
		--enable-deprecated        \
		--enable-gdk-pixbuf        \
		--enable-glib              \
		--disable-gtk-doc          \
		$(use_enable opengl glx)   \
		$(use_enable opengl gl)    \
		$(use_enable gles2)        \
		$(use_enable gles2 cogl-gles2) \
		$(use_enable gles2 xlib-egl-platform) \
		$(usex gles2 --with-default-driver=$(usex opengl gl gles2)) \
		$(use_enable gstreamer cogl-gst)    \
		$(use_enable introspection) \
		$(use_enable pango cogl-pango) \
		$(use_enable test unit-tests) \
		--disable-profile
#		$(use_enable doc gtk-doc)  \
#		$(use_enable profile)
}

ehook gnome2-multilib-global-pre_src_test maybe_skip_src_test
maybe_skip_src_test() {
	# Use swrast for tests, llvmpipe is incomplete and "test_sub_texture" fails
	# NOTE: recheck if this is needed after every mesa bump
	if [[ "$(eselect opengl show)" != "xorg-x11" ]]; then
		ewarn "Skipping tests because a binary OpenGL library is enabled. To"
		ewarn "run tests for ${PN}, you need to enable the Mesa library:"
		ewarn "# eselect opengl set xorg-x11"
		return 1
	fi
	return 0
}

ehook gnome2-multilib-per-abi-pre_src_test abi_src_test
abi_src_test() {
	# Use swrast for tests, llvmpipe is incomplete and "test_sub_texture" fails
	# NOTE: recheck if this is needed after every mesa bump
	LIBGL_DRIVERS_PATH="${EROOT}/usr/$(get_libdir)/mesa" Xemake check
	return 1
}

src_install() {
	clutter-multilib_src_install
}

ehook clutter-multilib-global-post_src_install global_post_install
global_post_install() {
	# Remove silly examples-data directory
	rm -rvf "${ED}usr/share/cogl/examples-data/" || die
}

ehook clutter-multilib-best-abi-post_src_install install_generated_readme
install_generated_readme() {
	dodoc README
}
