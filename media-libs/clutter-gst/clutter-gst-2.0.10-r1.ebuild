# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"
GCONF_DEBUG="yes"
CLUTTER_LA_PUNT="yes"
PYTHON_COMPAT=( python2_{6,7} )

# inherit clutter-multilib after gnome2-multilib so that defaults aren't overriden
# inherit gnome.org in the end so we use gnome mirrors and get the xz tarball
inherit gnome2-multilib clutter-multilib gnome.org python-any-r1

DESCRIPTION="GStreamer integration library for Clutter"

SLOT="2.0"
KEYWORDS="~amd64"
IUSE="examples +introspection"

# FIXME: Support for gstreamer-basevideo-0.10 (HW decoder support) is automagic
COMMON_DEPEND="
	>=dev-libs/glib-2.20:2[${MULTILIB_USEDEP}]
	>=media-libs/clutter-1.6.0:1.0=[introspection?,${MULTILIB_USEDEP}]
	>=media-libs/cogl-1.10:1.0=[introspection?,${MULTILIB_USEDEP}]
	>=media-libs/gstreamer-1.2.0:1.0[introspection?,${MULTILIB_USEDEP}]
	>=media-libs/gst-plugins-bad-1.2.0:1.0[${MULTILIB_USEDEP}]
	>=media-libs/gst-plugins-base-1.2.0:1.0[introspection?,${MULTILIB_USEDEP}]
	introspection? ( >=dev-libs/gobject-introspection-0.6.8[${MULTILIB_USEDEP}] )
"
# uses goom from gst-plugins-good
RDEPEND="${COMMON_DEPEND}
	media-libs/gst-plugins-good:1.0[${MULTILIB_USEDEP}]
"
DEPEND="${COMMON_DEPEND}
	${PYTHON_DEPS}
	>=dev-util/gtk-doc-am-1.8
	virtual/pkgconfig
"

src_prepare() {
	DOCS="AUTHORS NEWS README"
	EXAMPLES="examples/{*.c,*.png,README}"

	# Make doc parallel installable
	cd "${S}"/doc/reference
	sed -e "s/\(DOC_MODULE.*=\).*/\1${PN}-${SLOT}/" \
		-e "s/\(DOC_MAIN_SGML_FILE.*=\).*/\1${PN}-docs-${SLOT}.sgml/" \
		-i Makefile.am Makefile.in || die
	sed -e "s/\(<book.*name=\"\)clutter-gst/\1${PN}-${SLOT}/" \
		-i html/clutter-gst.devhelp2 || die
	mv clutter-gst-docs{,-${SLOT}}.sgml || die
	mv clutter-gst-overrides{,-${SLOT}}.txt || die
	mv clutter-gst-sections{,-${SLOT}}.txt || die
	mv clutter-gst{,-${SLOT}}.types || die
	mv html/clutter-gst{,-${SLOT}}.devhelp2

	cd "${S}"
	gnome2-multilib_src_prepare
}

src_configure() {
	gnome2-multilib_src_configure \
		--disable-maintainer-flags \
		$(use_enable introspection)
}

ehook gnome2-multilib-per-abi-pre_src_compile my_abi_pre_compile
my_abi_pre_compile() {
	# Clutter tries to access dri without userpriv, upstream bug #661873
	# Massive failure of a hack, see bug 360219, bug 360073, bug 363917
	unset DISPLAY
	default
	return 1
}
