# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit autotools-multilib

DESCRIPTION="Fast and safe graphics and drivers for about any graphics card to the Linux kernel (sometimes)"
HOMEPAGE="http://www.ggi-project.org"
SRC_URI="mirror://sourceforge/ggi/${P}.src.tar.bz2"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
IUSE="X aalib svga fbcon directfb 3dfx debug mmx vis"

RDEPEND=">=media-libs/libgii-1.0.2[${MULTILIB_USEDEP}]
	X? ( x11-libs/libXt[${MULTILIB_USEDEP}]
		x11-libs/libXxf86dga[${MULTILIB_USEDEP}]
		x11-libs/libXxf86vm[${MULTILIB_USEDEP}]
		x11-libs/libXt[${MULTILIB_USEDEP}] )
	svga? ( >=media-libs/svgalib-1.4.2[${MULTILIB_USEDEP}] )
	aalib? ( >=media-libs/aalib-1.2-r1[${MULTILIB_USEDEP}] )"
DEPEND="${RDEPEND}
	X? ( x11-proto/xf86dgaproto[${MULTILIB_USEDEP}]
		x11-proto/xf86vidmodeproto[${MULTILIB_USEDEP}]
		x11-proto/xextproto[${MULTILIB_USEDEP}] )"

src_configure() {
	local myeconfargs=( )

	use svga || myeconfargs+=( --disable-svga --disable-vgagl )

	if use !fbcon && use !directfb; then
		myeconfargs+=( --disable-fbdev --disable-directfb )
	elif use directfb; then
		myeconfargs+=( --enable-fbdev --enable-directfb )
	else
		myeconfargs+=( --enable-fbdev )
	fi

	# in the event that we are building for non-native abi (or, presumably,
	# cross-compiling), configure manages to pick the native gii regardless
	# (and then bomb out when it can't link against it)
	myeconfargs+=( --with-gii=/usr/@GET_LIBDIR@ )

	use X && myeconfargs+=( --x-libraries=/usr/@GET_LIBDIR@ )

	ehook autotools-multilib-per-abi-pre_src_configure set_abi_bits
	autotools-multilib_src_configure $(use_enable 3dfx glide) \
		$(use_enable aalib aa) \
		$(use_enable debug) \
		$(use_enable mmx) \
		$(use_enable vis) \
		$(use_with X x) \
		$(use_enable X x)
}

set_abi_bits() {
	if abi_arch_use amd64 || abi_arch_use ppc64 || abi_arch_use ia64 ; then
		myeconfargs+=( --enable-64bitc )
	else
		myeconfargs+=( --disable-64bitc ) 
	fi
	return 0
}

src_install () {
	autotools-multilib_src_install

	dodoc ChangeLog* FAQ NEWS README
	docinto txt
	dodoc doc/*.txt
}

pkg_postinst() {
	local abis=($(multilib_get_enabled_abis))
	if [[ ${#abis[@]} -gt 1 ]] ; then
		ewarn "Big fat warning: although this can install multiple ABIs, it is"
		ewarn "unclear if they will work.  See https://bugzilla.redhat.com/show_bug.cgi?id=239939"
		ewarn "If this proves to be a real problem then perhaps the sed-patching in the src.rpm Nicolas"
		ewarn "authored in that thread are the way forward."
	fi
}
