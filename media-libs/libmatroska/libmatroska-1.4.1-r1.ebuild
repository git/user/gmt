# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit flag-o-matic eutils multilib toolchain-funcs multilib-minimal

DESCRIPTION="Extensible multimedia container format based on EBML"
HOMEPAGE="http://www.matroska.org/ https://github.com/Matroska-Org/libmatroska/"
SRC_URI="https://github.com/Matroska-Org/libmatroska/archive/release-${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0/6" # subslot = soname major version
KEYWORDS="~amd64"
IUSE="static-libs"
RESTRICT="test"

DEPEND=">=dev-libs/libebml-1.3.0:=[${MULTILIB_USEDEP}]"
RDEPEND="${DEPEND}"

S=${WORKDIR}/${PN}-release-${PV}

src_prepare() {
	cd make/linux
	epatch "${FILESDIR}"/${P}-flags.patch
	cd ../..
	multilib_copy_sources
}

multilib_src_compile() {
	cd make/linux
	local targets="sharedlib"
	use static-libs && targets+=" staticlib"

	#fixes locale for gcc3.4.0 to close bug 52385
	append-flags $(test-flags -finput-charset=ISO8859-15)

	emake PREFIX="${EPREFIX}"/usr \
		LIBEBML_INCLUDE_DIR="${EPREFIX}"/usr/include/ebml \
		LIBEBML_LIB_DIR="${EPREFIX}"/usr/$(get_libdir) \
		CXX="${CXX:-$(tc-getCXX)}" \
		AR="${AR:-$(tc-getAR)}" \
		${targets}
}

multilib_src_install() {
	cd make/linux
	local targets="install_sharedlib install_headers"
	use static-libs && targets+=" install_staticlib"

	emake DESTDIR="${D}" prefix="${EPREFIX}"/usr libdir="${EPREFIX}"/usr/$(get_libdir) ${targets}
}

multilib_src_install_all() {
	dodoc ChangeLog
}
