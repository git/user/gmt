# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v1
# $Header: $

EAPI=5

[[ ${PV} == *9999 ]] && git_eclass="git-r3"
EGIT_REPO_URI=( "git://anongit.kde.org/${PN}" )

MY_PN="phonon-backend-gstreamer"
MY_P=${MY_PN}-${PV}

inherit my-god-its-full-of-quotation-marks cmake-multilib multibuild ${git_eclass}

DESCRIPTION="Phonon GStreamer backend"
HOMEPAGE="https://projects.kde.org/projects/kdesupport/phonon/phonon-gstreamer"
[[ ${PV} == *9999 ]] || SRC_URI="mirror://kde/stable/phonon/${MY_PN}/${PV}/src/${MY_P}.tar.xz"

LICENSE="LGPL-2.1"
if [[ ${PV} == *9999 ]]; then
	KEYWORDS=""
else
	KEYWORDS="~amd64"
fi
SLOT="0"
IUSE="alsa debug +network +qt4 qt5"
REQUIRED_USE="|| ( qt4 qt5 )"

# fixme: qt5 atoms need to be multilibutized.
RDEPEND="
	>=media-libs/gstreamer-0.10.36-r2:0.10[${MULTILIB_USEDEP}]
	>=media-libs/gst-plugins-base-0.10.36-r1:0.10[${MULTILIB_USEDEP}]
	>=media-plugins/gst-plugins-meta-0.10-r9:0.10[alsa?,ogg,vorbis,${MULTILIB_USEDEP}]
	>=media-libs/phonon-4.7.2[qt4?,qt5?,${MULTILIB_USEDEP}]
	qt4? (
		>=dev-qt/qtcore-4.8.5-r2:4[glib,${MULTILIB_USEDEP}]
		>=dev-qt/qtgui-4.8.5-r3:4[glib,${MULTILIB_USEDEP}]
		>=dev-qt/qtopengl-4.8.5-r1:4[${MULTILIB_USEDEP}]
	)
	qt5? (
		dev-qt/qtcore:5[glib,${MULTILIB_USEDEP}]
		dev-qt/qtgui:5[glib,${MULTILIB_USEDEP}]
		dev-qt/qtopengl:5[${MULTILIB_USEDEP}]
		dev-qt/qtwidgets:5[${MULTILIB_USEDEP}]
	)
	>=virtual/opengl-7.0-r1[${MULTILIB_USEDEP}]
	network? (
		>=media-plugins/gst-plugins-soup-0.10.31-r1:0.10[${MULTILIB_USEDEP}]
	)
"
DEPEND="${RDEPEND}
	qt4? ( >=dev-util/automoc-0.9.87 )
	virtual/pkgconfig
"

PATCHES=( "${FILESDIR}/${P}-FindPhonon-multilib.patch" )

[[ ${PV} == 9999 ]] || S=${WORKDIR}/${MY_P}

CMAKE_IN_SOURCE_BUILD=1

setup_qt_variants() {
	MULTIBUILD_VARIANTS=()
	if use qt4; then
		MULTIBUILD_VARIANTS+=(qt4)
	fi
	if use qt5; then
		MULTIBUILD_VARIANTS+=(qt5)
	fi
}

foreach_qt_variant() {
	debug-print-function ${FUNCNAME} "$(mg-qm "${@}")"

	local MULTIBUILD_VARIANTS=()
	setup_qt_variants

	# FIXME: stupidly clever construct
	_localizing_wrapper \
		multibuild_foreach_variant _localizing_wrapper \
			run_in_build_dir "$@"
}

# FIXME: figure out if this is really needed; if so, make
# it magically no longer neccesary
_localizing_wrapper() {
	debug-print-function ${FUNCNAME} "$(mg-qm "${@}")"

	local S="${S}"
	local BUILD_DIR="${BUILD_DIR}"
	local CMAKE_USE_DIR="${BUILD_DIR}"
	local ECONF_SOURCE="${ECONF_SOURCE}"
	"$@"
}

src_prepare() {
	debug-print-function ${FUNCNAME} "$(mg-qm "${@}")"
	debug-print "${FUNCNAME}: BUILD_DIR=\"${BUILD_DIR}\""

	qt_copy_variants() {
		debug-print-function ${FUNCNAME} "$(mg-qm "${@}")"
		debug-print "${FUNCNAME}: BUILD_DIR=\"${BUILD_DIR}\""

		local MULTIBUILD_VARIANTS=()
		setup_qt_variants
		multibuild_copy_sources
		# sigh -- ${BUILD_DIR} is not used, just the copies.
		cd .. || die
		rm -rf ${BUILD_DIR} || die
		mkdir ${BUILD_DIR} || die
	}
	cmake-multilib_src_prepare
	multilib_foreach_abi _localizing_wrapper qt_copy_variants
}

multilib_src_configure() {
	debug-print-function ${FUNCNAME} "$(mg-qm "${@}")"
	debug-print "${FUNCNAME}: BUILD_DIR=\"${BUILD_DIR}\""

	qt_variant_configure() {
		debug-print-function ${FUNCNAME} "$(mg-qm "${@}")"
		debug-print "${FUNCNAME}: BUILD_DIR=\"${BUILD_DIR}\""

		# localize mycmakeargs
		local mycmakeargs=("${mycmakeargs[@]}")

		# find correct multilib Phonon package
		local Phonon_DIR="${EPREFIX}/usr/$(get_libdir)/phonon"
		export Phonon_DIR

		if [[ ${MULTIBUILD_VARIANT} = qt4 ]]; then
			mycmakeargs+=(-DPHONON_BUILD_PHONON4QT5=OFF)
		fi
		if [[ ${MULTIBUILD_VARIANT} = qt5 ]]; then
			mycmakeargs+=(-DPHONON_BUILD_PHONON4QT5=ON)
		fi
		cmake-utils_src_configure
	}

	foreach_qt_variant qt_variant_configure
}

multilib_src_compile() {
	debug-print-function ${FUNCNAME} "$(mg-qm "${@}")"

	foreach_qt_variant cmake-utils_src_compile
}

multilib_src_install() {
	debug-print-function ${FUNCNAME} "$(mg-qm "${@}")"

	foreach_qt_variant cmake-utils_src_install
}

multilib_src_test() {
	debug-print-function ${FUNCNAME} "$(mg-qm "${@}")"

	foreach_qt_variant cmake-utils_src_test
}
