# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4
inherit libtool autotools-multilib

DESCRIPTION="A rendering library for Kate streams using Pango and Cairo"
HOMEPAGE="http://code.google.com/p/libtiger/"
SRC_URI="http://libtiger.googlecode.com/files/${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64"
IUSE="doc"

RDEPEND="x11-libs/pango[${MULTILIB_USEDEP}]
	>=media-libs/libkate-0.2.0[${MULTILIB_USEDEP}]
	x11-libs/cairo[${MULTILIB_USEDEP}]"
DEPEND="${RDEPEND}
	virtual/pkgconfig
	doc? ( app-doc/doxygen )"

DOCS=( THANKS README ChangeLog AUTHORS )

AUTOTOOLS_PRUNE_LIBTOOL_FILES=all
AUTOTOOLS_IN_SOURCE_BUILD=yes

src_configure() {
	autotools-multilib_src_configure \
		$(use_enable doc) --docdir=/usr/share/doc/${PF}
}
