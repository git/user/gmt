# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit multilib-minimal

DESCRIPTION="library for MPEG TS/DVB PSI tables decoding and generation"
HOMEPAGE="http://www.videolan.org/libdvbpsi"
SRC_URI="http://download.videolan.org/pub/${PN}/${PV}/${P}.tar.bz2"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64"
IUSE="doc static-libs"

RDEPEND=""
DEPEND="
	doc? (
		app-doc/doxygen
		>=media-gfx/graphviz-2.26
	)" # Require recent enough graphviz wrt #181147

src_prepare() {
	sed -e '/CFLAGS/s:-O2::' -e '/CFLAGS/s:-O6::' -i configure || die
}

multilib_src_configure() {
	ECONF_SOURCE="${S}" econf \
		$(use_enable static-libs static) \
		--enable-release
	if multilib_is_best_abi && use doc; then
		info cloning doc tree...
		cp -av "${S}"/doc .
	fi
}

multilib_src_compile() {
	emake
	multilib_is_best_abi && use doc && \
		emake doc
}

multilib_src_install() {
	default
	multilib_is_best_abi && use doc && \
		dohtml doc/doxygen/html/*
}

multilib_src_install_all() {
	DOCS=( AUTHORS ChangeLog NEWS README TODO )
	dodoc "${DOCS[@]}"
	rm -f "${ED}"usr/lib*/${PN}.la
}
