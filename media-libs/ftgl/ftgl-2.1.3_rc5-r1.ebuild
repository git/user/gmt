# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit autotools-multilib eutils flag-o-matic autotools

MY_PV=${PV/_/-}
MY_PV2=${PV/_/\~}
MY_P=${PN}-${MY_PV}
MY_P2=${PN}-${MY_PV2}

DESCRIPTION="library to use arbitrary fonts in OpenGL applications"
HOMEPAGE="http://ftgl.wiki.sourceforge.net/"
SRC_URI="mirror://sourceforge/${PN}/${MY_P}.tar.bz2"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
IUSE="static-libs"

DEPEND=">=media-libs/freetype-2.0.9[${MULTILIB_USEDEP}]
	virtual/opengl[${MULTILIB_USEDEP}]
	virtual/glu[${MULTILIB_USEDEP}]
	media-libs/freeglut[${MULTILIB_USEDEP}]"
RDEPEND="${DEPEND}"

S=${WORKDIR}/${MY_P2}

DOCS=( AUTHORS BUGS ChangeLog INSTALL NEWS README TODO docs/projects_using_ftgl.txt )

src_prepare() {
	epatch "${FILESDIR}"/${P}-gentoo.patch \
		"${FILESDIR}"/${P}-underlink.patch
#	AT_M4DIR=m4 eautoreconf
	sed -e "s/AM_CONFIG_HEADER/AC_CONFIG_HEADERS/" -i configure.ac || die
	AUTOTOOLS_AUTORECONF=yes # fixes maintainer-mode badness building OOT
	autotools-multilib_src_prepare
}

src_configure() {
	strip-flags # ftgl is sensitive - bug #112820
	autotools-multilib_src_configure \
		$(use_enable static-libs static)
}

src_install() {
	AUTOTOOLS_PRUNE_LIBTOOL_FILES=all
	autotools-multilib_src_install
	rm -rf "${D}"/usr/share/doc/ftgl
}
