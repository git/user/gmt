# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"
CLUTTER_LA_PUNT="yes"

# Inherit gnome2 after clutter to download sources from gnome.org
# since clutter-project.org doesn't provide .xz tarballs
inherit gtk-doc clutter-multilib gnome2-multilib virtualx

DESCRIPTION="Clutter is a library for creating graphical user interfaces"

LICENSE="LGPL-2.1+ FDL-1.1+"
SLOT="1.0"
IUSE="debug doc gtk +introspection test" # evdev tslib
KEYWORDS="~amd64"

# NOTE: glx flavour uses libdrm + >=mesa-7.3
# XXX: uprof needed for profiling
# >=libX11-1.3.1 needed for X Generic Event support
# XXX: evdev input requires libinput and gudev >= 136
RDEPEND="
	>=dev-libs/glib-2.38.2-r1:2[${MULTILIB_USEDEP}]
	>=dev-libs/atk-2.12.0-r1[introspection?,${MULTILIB_USEDEP}]
	>=dev-libs/json-glib-0.16.2-r1[introspection?,${MULTILIB_USEDEP}]
	>=media-libs/cogl-1.18.0-r1:1.0=[introspection?,pango,${MULTILIB_USEDEP}]
	>=media-libs/fontconfig-2.11.1-r1[${MULTILIB_USEDEP}]
	>=x11-libs/cairo-1.12.16-r2:=[glib,${MULTILIB_USEDEP}]
	>=x11-libs/pango-1.36.3-r2[introspection?,${MULTILIB_USEDEP}]

	>=virtual/opengl-7.0-r1[${MULTILIB_USEDEP}]
	>=x11-libs/libdrm-2.4.52:=[${MULTILIB_USEDEP}]
	>=x11-libs/libX11-1.6.2[${MULTILIB_USEDEP}]
	>=x11-libs/libXext-1.3.2[${MULTILIB_USEDEP}]
	>=x11-libs/libXdamage-1.1.4-r1[${MULTILIB_USEDEP}]
	>=x11-proto/inputproto-2.3[${MULTILIB_USEDEP}]
	>=x11-libs/libXi-1.7.2[${MULTILIB_USEDEP}]
	>=x11-libs/libXcomposite-0.4.4-r1[${MULTILIB_USEDEP}]

	gtk? ( >=x11-libs/gtk+-3.10.8-r1:3[${MULTILIB_USEDEP}] )
	introspection? ( >=dev-libs/gobject-introspection-1.38.0-r1[${MULTILIB_USEDEP}] )
"
DEPEND="${RDEPEND}
	>=dev-util/gtk-doc-am-1.20
	virtual/pkgconfig
	>=sys-devel/gettext-0.18.3.2[${MULTILIB_USEDEP}]
	doc? (
		>=dev-util/gtk-doc-1.20
		>=app-text/docbook-sgml-utils-0.6.14[jadetex]
		dev-libs/libxslt )
	test? ( >=x11-libs/gdk-pixbuf-2.30.7-r1[${MULTILIB_USEDEP}] )"

# Tests fail with both swrast and llvmpipe
# They pass under r600g or i965, so the bug is in mesa
RESTRICT="test"

src_prepare() {
	# We only need conformance tests, the rest are useless for us
	sed -e 's/^\(SUBDIRS =\).*/\1 accessibility conform/g' \
		-i tests/Makefile.am || die "am tests sed failed"
	sed -e 's/^\(SUBDIRS =\)[^\]*/\1  accessibility conform/g' \
		-i tests/Makefile.in || die "in tests sed failed"

	gnome2-multilib_src_prepare
}

src_configure() {
	DOCS="README NEWS ChangeLog*"

	# XXX: Conformance test suite (and clutter itself) does not work under Xvfb
	# (GLX error blabla)
	# XXX: Profiling, coverage disabled for now
	# XXX: What about cex100/egl/osx/wayland/win32 backends?
	# XXX: evdev/tslib input seem to be experimental?
	gnome2-multilib_src_configure \
		--enable-xinput \
		--enable-x11-backend=yes \
		--disable-profile \
		--disable-maintainer-flags \
		--disable-gcov \
		--disable-cex100-backend \
		--disable-egl-backend \
		--disable-quartz-backend \
		--disable-wayland-backend \
		--disable-win32-backend \
		--disable-tslib-input \
		--disable-evdev-input \
		$(usex debug --enable-debug=yes --enable-debug=minimum) \
		$(use_enable doc docs) \
		$(use_enable gtk gdk-backend) \
		$(use_enable introspection) \
		$(use_enable test gdk-pixbuf)
}

src_test() {
	multilib_foreach_abi abi_src_test
}

abi_src_test() {
	Xemake check
	Xemake check -C tests/conform
}

src_install() {
	clutter-multilib_src_install
}
