# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="5"
CLUTTER_LA_PUNT="yes"

# Inherit gnome2 after clutter to download sources from gnome.org
# since clutter-project.org doesn't provide .xz tarballs
inherit gtk-doc clutter-multilib eutils gnome2-multilib virtualx

DESCRIPTION="Clutter is a library for creating graphical user interfaces"

LICENSE="LGPL-2.1+ FDL-1.1+"
SLOT="1.0"
IUSE="debug doc gtk +introspection test" # evdev tslib
KEYWORDS="~amd64"

# NOTE: glx flavour uses libdrm + >=mesa-7.3
# XXX: uprof needed for profiling
# >=libX11-1.3.1 needed for X Generic Event support
RDEPEND="
	>=dev-libs/glib-2.37.3:2[${MULTILIB_USEDEP}]
	>=dev-libs/atk-2.5.3[introspection?,${MULTILIB_USEDEP}]
	>=dev-libs/json-glib-0.12[introspection?,${MULTILIB_USEDEP}]
	>=media-libs/cogl-1.15.9:1.0=[introspection?,pango,${MULTILIB_USEDEP}]
	media-libs/fontconfig[${MULTILIB_USEDEP}]
	>=x11-libs/cairo-1.10:=[glib,${MULTILIB_USEDEP}]
	>=x11-libs/pango-1.30[introspection?,${MULTILIB_USEDEP}]

	virtual/opengl[${MULTILIB_USEDEP}]
	x11-libs/libdrm:=[${MULTILIB_USEDEP}]
	>=x11-libs/libX11-1.3.1[${MULTILIB_USEDEP}]
	x11-libs/libXext[${MULTILIB_USEDEP}]
	x11-libs/libXdamage[${MULTILIB_USEDEP}]
	x11-proto/inputproto[${MULTILIB_USEDEP}]
	>=x11-libs/libXi-1.3[${MULTILIB_USEDEP}]
	>=x11-libs/libXcomposite-0.4[${MULTILIB_USEDEP}]

	gtk? ( >=x11-libs/gtk+-3.3.18:3[${MULTILIB_USEDEP}] )
	introspection? ( >=dev-libs/gobject-introspection-0.9.6[${MULTILIB_USEDEP}] )
"
DEPEND="${RDEPEND}
	>=dev-util/gtk-doc-am-1.15
	virtual/pkgconfig
	>=sys-devel/gettext-0.17[${MULTILIB_USEDEP}]
	doc? (
		>=dev-util/gtk-doc-1.15
		>=app-text/docbook-sgml-utils-0.6.14[jadetex]
		dev-libs/libxslt )
	test? ( x11-libs/gdk-pixbuf[${MULTILIB_USEDEP}] )"

# Tests fail with both swrast and llvmpipe
# They pass under r600g or i965, so the bug is in mesa
RESTRICT="test"

src_prepare() {
	# We only need conformance tests, the rest are useless for us
	sed -e 's/^\(SUBDIRS =\).*/\1 accessibility data conform/g' \
		-i tests/Makefile.am || die "am tests sed failed"
	sed -e 's/^\(SUBDIRS =\)[^\]*/\1  accessibility data conform/g' \
		-i tests/Makefile.in || die "in tests sed failed"

	# Fix buffer_age code path, bug #503560
	epatch "${FILESDIR}/${P}-buffer-age.patch"

	gnome2-multilib_src_prepare
}

src_configure() {
	DOCS="README NEWS ChangeLog*"

	# XXX: Conformance test suite (and clutter itself) does not work under Xvfb
	# (GLX error blabla)
	# XXX: Profiling, coverage disabled for now
	# XXX: What about cex100/egl/osx/wayland/win32 backends?
	# XXX: evdev/tslib input seem to be experimental?
	gnome2-multilib_src_configure \
		--enable-xinput \
		--enable-x11-backend=yes \
		--disable-profile \
		--disable-maintainer-flags \
		--disable-gcov \
		--disable-cex100-backend \
		--disable-egl-backend \
		--disable-quartz-backend \
		--disable-wayland-backend \
		--disable-win32-backend \
		--disable-tslib-input \
		--disable-evdev-input \
		$(usex debug --enable-debug=yes --enable-debug=minimum) \
		$(use_enable doc docs) \
		$(use_enable gtk gdk-backend) \
		$(use_enable introspection) \
		$(use_enable test conformance) \
		$(use_enable test gdk-pixbuf)
}

src_compile() {
	gnome2-multilib_src_compile
	if use test; then
		emake -C tests/conform
	fi
}

src_test() {
	multilib_foreach_abi Xemake check
}

src_install() {
	clutter-multilib_src_install
}
