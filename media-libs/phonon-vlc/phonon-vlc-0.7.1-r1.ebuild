# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

MY_PN="phonon-backend-vlc"
MY_P="${MY_PN}-${PV}"
EGIT_REPO_URI=( "git://anongit.kde.org/${PN}" )
[[ ${PV} == 9999 ]] && git_eclass=git-r3
inherit cmake-multilib multibuild ${git_eclass}
unset git_eclass

DESCRIPTION="Phonon VLC backend"
HOMEPAGE="https://projects.kde.org/projects/kdesupport/phonon/phonon-vlc"
[[ ${PV} == 9999 ]] || SRC_URI="mirror://kde/stable/phonon/${MY_PN}/${PV}/${MY_P}.tar.xz"

LICENSE="LGPL-2.1"

# Don't move KEYWORDS on the previous line or ekeyword won't work # 399061
[[ ${PV} == 9999 ]] || \
KEYWORDS="~amd64"

SLOT="0"
IUSE="debug +qt4 qt5"
REQUIRED_USE="|| ( qt4 qt5 )"

RDEPEND="
	>=media-libs/phonon-4.7.0[qt4=,qt5=,${MULTILIB_USEDEP}]
	>=media-video/vlc-2.0.1:=[dbus,ogg,vorbis,${MULTILIB_USEDEP}]
	qt4? (
		dev-qt/qtcore:4[${MULTILIB_USEDEP}]
		dev-qt/qtdbus:4[${MULTILIB_USEDEP}]
		dev-qt/qtgui:4[${MULTILIB_USEDEP}]
	)
	qt5? (
		dev-qt/qtcore:5[${MULTILIB_USEDEP}]
		dev-qt/qtdbus:5[${MULTILIB_USEDEP}]
		dev-qt/qtgui:5[${MULTILIB_USEDEP}]
	)
"
DEPEND="${RDEPEND}
	app-arch/xz-utils[${MULTILIB_USEDEP}]
	qt4? ( >=dev-util/automoc-0.9.87 )
	virtual/pkgconfig
"

[[ ${PV} == 9999 ]] || S=${WORKDIR}/${MY_P}

DOCS=( AUTHORS )

pkg_setup() {
	MULTIBUILD_VARIANTS=()
	if use qt4; then
		MULTIBUILD_VARIANTS+=(qt4)
	fi
	if use qt5; then
		MULTIBUILD_VARIANTS+=(qt5)
	fi
}

src_prepare() {
	myprepare() {
		cmake-multilib_src_prepare
		# sigh -- we just want to copy from ${S} to ${BUILD_DIR}-${ml-ABI} directories,
		# but as there's no straightforward way to make that happen directly, intermediate
		# ${BUILD_DIR} directories must be created, even though they serve no purpose.
		cd ..
		rm -rf ${BUILD_DIR}
	}
	multibuild_copy_sources
	multibuild_foreach_variant myprepare
}

src_configure() {
myconfigure() {
		local mycmakeargs=(-DPhonon_DIR="${EPREFIX}"/usr/@GET_LIBDIR@/cmake/phonon)
		if [[ ${MULTIBUILD_VARIANT} = qt4 ]]; then
			mycmakeargs+=(-DPHONON_BUILD_PHONON4QT5=OFF)
		fi
		if [[ ${MULTIBUILD_VARIANT} = qt5 ]]; then
			mycmakeargs+=(-DPHONON_BUILD_PHONON4QT5=ON)
		fi
		cmake-multilib_src_configure
	}

	multibuild_foreach_variant myconfigure
}

src_compile() {
	multibuild_foreach_variant cmake-multilib_src_compile
}

src_install() {
	multibuild_foreach_variant cmake-multilib_src_install
}

src_test() {
	multibuild_foreach_variant cmake-multilib_src_test
}

pkg_postinst() {
	elog "For more verbose debug information, export the following variables:"
	elog "PHONON_DEBUG=1"
	elog ""
	elog "To make KDE detect the new backend without reboot, run:"
	elog "kbuildsycoca4 --noincremental"
}
